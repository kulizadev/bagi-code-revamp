const fs = require('fs');
const fetch = require('node-fetch');
const btoa = require('btoa');
const drupalUserName = 'react_api';
const drupalPassword = 'react_api';
const headers = {};
const footerApi =
  'http://54.145.13.134/drupal_backend/api/menu_items/footer?_format=json';
const ContryListApi = 'http://52.45.125.45:8000/api/v1/cordys/country_list/';

const cityListApi = 'http://52.45.125.45:8000/api/v1/cordys/get_cities/';

const cityStateApi = 'http://52.45.125.45:8000/api/v1/cordys/city_state_list/';

const ExistingInsuranceCompanyApi = "http://54.145.13.134/drupal_backend/insurer-names?_format=json"


headers['Content-Type'] = 'application/json';
headers.Accept = 'application/json';

// Python api to get city list
// fetch(cityListApi, {
//   method: 'GET',
//   headers,
// })
//   .then(response => response.json())
//   .then(data => {
//     const cityListStr = `const ResponseData = \n${JSON.stringify(
//       data,
//     )}\n export default ResponseData;`;
//     fs.writeFile(
//       'app/containers/Car/CarRollover/cityList.ts',
//       cityListStr,
//       err => {
//         if (err) throw err;
//         // eslint-disable-next-line no-console
//         console.log('City list api file has been saved!');
//       },
//     );
//   });

// Python City, State List Api Call

  fetch(cityStateApi, {
    method: 'GET',
    headers,
  })
    .then(response => response.json())
    .then(data => {
      const cityStateListStr = `const CityStateList = \n${JSON.stringify(
        data,
      )}\n export default CityStateList;`;
      fs.writeFile(
        'app/constants/CityStateList.ts',
        cityStateListStr,
        err => {
          if (err) throw err;
          // eslint-disable-next-line no-console
          console.log('City state list api file has been saved!');
        },
      );
    });

    

// Python Country List Api Call
fetch(ContryListApi, {
  method: 'GET',
  headers,
})
  .then(response => response.json())
  .then(data => {
    const footerStr = `const ResponseData = \n${JSON.stringify(
      data,
    )}\n export default ResponseData;`;
    fs.writeFile(
      'app/containers/TravelFlow/CountryList.ts',
      footerStr,
      err => {
        if (err) throw err;
        // eslint-disable-next-line no-console
        console.log('Country list api file has been saved!');
      },
    );
  });
// for drupal footer api call........................
headers.Authorization = `Basic ${btoa(`${drupalUserName}:${drupalPassword}`)}`;
fetch(footerApi, {
  method: 'GET',
  headers,
})
  .then(response => response.json())
  .then(data => {
    const footerStr = `const ResponseData = \n${JSON.stringify(
      data,
    )}\n export default ResponseData;`;
    fs.writeFile('app/constants/FooterInfo.ts', footerStr, err => {
      if (err) throw err;
      // eslint-disable-next-line no-console
      console.log('Footer api file has been saved!');
    });
  });


// headers.Authorization = `Basic ${btoa(`${drupalUserName}:${drupalPassword}`)}`;
// fetch(ExistingInsuranceCompanyApi, {
//   method: 'GET',
//   headers,
// })
//   .then(response => response.json())
//   .then(data => {
//     const ExistingInsuranceCompany = `const ExistingInsuranceCompany = \n${JSON.stringify(
//       data,
//     )}\n export default ExistingInsuranceCompany;`;
//     fs.writeFile('app/constants/ExistingInsuranceComapny.ts', ExistingInsuranceCompany, err => {
//       if (err) throw err;
//       // eslint-disable-next-line no-console
//       console.log('existing insurance file has been saved!');
//     });
//   });


  // fetch(ExistingInsuranceCompanyApi, {
  //   method: 'GET',
  //   headers,
  // })
  //   .then(response => response.json())
  //   .then(data => {
  //     const ExistingInsuranceCompany = `const ExistingInsuranceComapny = \n${JSON.stringify(
  //       data,
  //     )}\n export default ExistingInsuranceComapny;`;
  //     fs.writeFile(
  //       'app/constants/ExistingInsuranceComapny.ts',
  //       ExistingInsuranceCompany,
  //       err => {
  //         if (err) throw err;
  //         // eslint-disable-next-line no-console
  //         console.log('existing insurance list api file has been saved!');
  //       },
  //     );
  //   });