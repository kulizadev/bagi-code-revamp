import TravelContactDetails from 'containers/TravelFlow/contact';
import TravelHome from 'containers/TravelFlow/travelHome';
import TravelProposalForm from 'containers/TravelFlow/proposal';
import TravelPremium from 'containers/TravelFlow/premium';
import TravelSummary from 'containers/TravelFlow/summary';
import CarRollOverHome from 'containers/CarFlow/CarRollOverHome';
import NotFoundCarDetails from 'containers/CarFlow/Could_Not_Find_Car';
const RoutingPathConstants = {
    travelInsuranceHome: '/travel-insurance',
    travelInsuranceContact: '/contact-details',
    travelInsuranceProposal: '/proposal',
    travelInsuranceSummary: '/summary',
    travelInsurancePremium: '/premium',
    genericBlogPath: '/blog/:productType/:blogType',
    paymentGateway: '/payment',
    transactionFailed: '/transaction-failed',
    paymentRedirect : '/payment-redirect',
    BillDeskPayment: 'payment/billdesk_payment/',
    BillDeskPaymentMessage: 'payment/billdesk_payment_policy/',
    PayuPaymentMessage: 'payment/payu_payment_policy/',
    maintenancePage: '/bhartiaxa/maintenancepage',
    medicalQuestionnaire: '/product/mdicalquestionnaire',
    notFound: '/under-construction',
    transactionNotSucceded: '/transaction-not-succeeded',
    nstp: '/nstp',
    carRolloverHome: '/car-insurance',
    couldNotFindCarPath: '/could-not-find-car',
};
export const TravelRouting = [
    {path: '/', component: TravelHome, name: 'home'},
    {path: RoutingPathConstants.travelInsuranceContact, component: TravelContactDetails, name: 'contact'},
    {path: RoutingPathConstants.travelInsuranceProposal, component: TravelProposalForm, name: 'proposal'},
    {path: RoutingPathConstants.travelInsuranceSummary, component: TravelSummary, name: 'summary'},
    {path: RoutingPathConstants.travelInsurancePremium, component: TravelPremium, name: 'premium'},
];

export const CarRollOverRouting = [
    {path: '/', component: CarRollOverHome, name: 'Car Rollover home'},
    { path: RoutingPathConstants.couldNotFindCarPath, component: NotFoundCarDetails, name: 'Could Not Find Car' }
];
export default RoutingPathConstants;
