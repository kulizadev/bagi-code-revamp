const GlobalConstants = {
    carInsurance : 'Car Insurance',
    cityOfRegistrationRto: 'City of Registration(RTO)',
    cityOfRegistration: 'City of Registration',
    registrationMonthYear: 'Registration Month and Year',
    carMakeAndModel: 'Car Make & Model',
    carInsuranceQuote: 'Car Insurance Quote',
    reasonsToBuy : 'Reasons To Buy',
    whyUs : 'Why Us?',
    carNameHeading: 'Hyundai Verna Car Insurance Quote',
    home : 'Home',
    motor : 'Motor',
    getQuote: 'Get Quote',
    Continue: 'Continue',
    policyCoverage: 'Policy\'s Coverage',
    Travel: 'Travel Insurance',
    TravelText: 'Travel',
    travelInsuranceQuote: 'Travel Insurace Quote',
    leaveAReview: 'Leave a Review',
    viewMore: 'View more',
    viewLess: 'View Less',
    seeMore: 'See More',
    seeMoreFaqs: 'See more faqs',
    IDV: 'IDV:',
    NCB: 'NCB:',
    Health: 'Health Insurance',
    unableToDecide: 'STILL UNABLE TO DECIDE?',
    scheduleCall: 'Schedule a call with our expert',
    callUs: 'Call us (Toll Free)',
    contactNumber: '1800 000 9000',
    thankYou: 'THANK YOU FOR YOUR REQUEST!',
    executive: 'Our executive will get in touch with you shortly.',
    authorize: 'I authorize Bharti AXA to contact me on the  given details for policy related follow-up.',
    tnc: 'I agree to be contacted by Bharti AXA overriding my NDNC registration.',
    socialMedia: 'Follow us on social media',
    irdaiReg: 'IRDAI Registration N0.139 | CIN-U66030KA2007PLC043362',
    paymentGateway: 'Secure Payment Gateways',
    disclaimer: 'Insurance is the subject matter of the solicitation. The advertisement contains only an indication of cover offered. For more details on risk factors, terms, conditions, and exclusions, please read the sales brocure carefully before concluding the sale. Bharti AXA General Insurance Company Limited.',
    connect: 'Connect With Us',
    travelQuoteLandingHeader:'Give us the below details and get the best quote!',
    travelPreQuoteHeader: 'Give few more details and get the best quote for your travel insurance',
    travelQuoteHeader: 'Choose from the best Quotes below and get your trip insured!',
    ChoosePlan: 'Choose a plan that suits you the best',
};
export const PlaceholderTxt = {
    DateField: 'dd-mmm-yyyy',
    DateFormat: "DD-MMM-YYYY",
    DestinationOfTraveller: 'Country(s) you are travelling to',
    AgeTraveller: 'Age (in yrs)',
    Yes: 'Yes',
    No: 'No',
    RecommendedPlanTxt: 'Recommended for the country(s) selected'
}
export default GlobalConstants;
