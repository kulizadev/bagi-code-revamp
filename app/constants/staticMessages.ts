// Constants file for Error Messages and Tooltips

export const errorMsgs = {
    phoneNumberError: 'Please enter a valid 10-digit mobile number!',
    productCategoryError: 'Please select a category!',
}

export const tooltipMsgs = {}