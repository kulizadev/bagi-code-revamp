// Regex constant file
export const regex = {
    phoneNumberRegex: /^[6789]\d{9}$/,
}
export default regex;
