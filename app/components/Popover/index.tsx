import React, { Component } from 'react';
import AntPopover from 'antd/lib/popover';
import DefaultStyle from './Styles';
class Popover extends Component<any, any> { // need to implement as interface props
   public render() {
    const { width, ...antProps } = this.props;
    return (
            <DefaultStyle>
                <AntPopover {...antProps} overlayStyle={{width: width}}>
                     {this.props.children}
                </AntPopover>
            </DefaultStyle>
        );
    }
}
export default Popover;
