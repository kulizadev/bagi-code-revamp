import styled from 'styled-components';
import vars from 'configs/styleVars';
const cssProps = {
  type1: {
    'border-top': '0px solid',
  },
  type2: {
    'border': '0px solid',
  },
  antCollaspeBorder: {
    'border': 'none',
    'background': `${vars.colors.secondaryBodyBG}`,
  },
  type2Accordian:{
    'padding': '0px',
  },
  type2AccordianInner: {
    'padding': '12px 0px',
    'border-bottom': '2px solid #d9d9d9',
  },
  type2BorderBottom: {
    'border-bottom': 'none',
  },
  type1Border:{
     'border': `1px solid ${vars.colors.palegraythreeColor}`,
     'margin': '4px',
     'border-radius': '10px'
  }
}
const DefaultStyle = styled<any, any>('div')`
.ant-collapse-content{
  ${props => {
    switch (props.accordianType) {
      case 'type1':
        return cssProps[props.accordianType] ;
      case 'type2':
        return cssProps['antCollaspeBorder'];
      default:
        return '';
    }
  }};
}
.ant-collapse{
  ${props => {
    switch (props.accordianType) {
      case 'type1':
        return cssProps['antCollaspeBorder'] ;
      case 'type2':
        return cssProps['antCollaspeBorder'];
      default:
        return '';
    }
  }};
}
.ant-collapse > .ant-collapse-item{
  ${props =>{
    switch (props.accordianType){
      case 'type2':
        return cssProps['type2BorderBottom']
        case 'type1':
        return cssProps['type1Border']
      default:
        return '';

    }
  }}
}
.ant-collapse > .ant-collapse-item > .ant-collapse-header .ant-collapse-extra{
    float: left;
    padding: 0px 13px 0px 5px;
}
.ant-collapse > .ant-collapse-item > .ant-collapse-header{
  // background: ${vars.colors.secondaryBodyBG};
  font-size: ${vars.fonts.fontSizeButton};
  color: ${vars.colors.headingTextColor};
}
.ant-collapse-content > .ant-collapse-content-box{
  ${props =>{
    switch (props.accordianType){
      case 'type2':
        return cssProps['type2Accordian'];
      default:
        return '';

    }
  }}
}
.ant-collapse-icon-position-right > .ant-collapse-item > .ant-collapse-header{
  ${props =>{
    switch (props.accordianType){
      case 'type2':
        return cssProps['type2AccordianInner'];
      default:
        return '';

    }
  }}
}

`;
export default DefaultStyle;