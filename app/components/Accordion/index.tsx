import React, { Component } from 'react';
import DefaultStyle from './Styles';
import Collapse from 'antd/lib/collapse';
const { Panel } = Collapse;
interface AccordionProps{
    accordianType: string;
    onChange?: any;
    contents?: any;
    expandIconPosition?: any;
    extra?: any;
    showArrow?: any;
}
class AccordionComponent extends Component<AccordionProps, any> {
    // type1: with border styling
    // type2: without border styling
    public render() {
    const {extra, showArrow, contents, accordianType, ...antProps } = this.props;
    return (
            <DefaultStyle accordianType={accordianType}>
                 <Collapse {...antProps} accordion >
                     {contents.map((val, ind) => {
                         return(
                            <Panel header={val.header} key={ind} showArrow={showArrow} extra={extra} >
                               <p>{val.body}</p>
                            </Panel>
                         );
                     })}
                </Collapse>
            </DefaultStyle>
        );
    }
}
export default AccordionComponent;
