import React, { Component } from 'react';
import DefaultStyle from './Styles';
import AntCheckbox from 'antd/lib/checkbox';

interface CheakboxProps{
    onChange?: any;
}
class Checkbox extends Component<CheakboxProps, any> {
   public render() {
    const {...antProps } = this.props;
    return (
            <DefaultStyle>
                <AntCheckbox {...antProps}>
                 {this.props.children}
                </AntCheckbox>
            </DefaultStyle>
        );
    }
}
export default Checkbox;
