import styled from 'styled-components';
import vars from 'configs/styleVars';
const DefaultStyle = styled<any, any>('span')`
span.ant-checkbox-inner {
  border-radius: 50%;
  width: 20px;
  height: 20px;
  ::after{
      position: absolute;
      display: table;
      border: 2px solid #cfcfcf;
      border-top: 0;
      border-left: 0;
      left: 25%;
      height: 11.142857px;
      -webkit-transform: rotate(45deg) scale(1) translate(-50%, -50%);
      -ms-transform: rotate(45deg) scale(1) translate(-50%, -50%);
      transform: rotate(45deg) scale(1) translate(-50%, -50%);
      opacity: 1;
      -webkit-transition: all 0.2s cubic-bezier(0.12, 0.4, 0.29, 1.46) 0.1s;
      transition: all 0.2s cubic-bezier(0.12, 0.4, 0.29, 1.46) 0.1s;
      content: ' ';
  }
}
.ant-checkbox-checked span.ant-checkbox-inner {
  background-color: ${vars.colors.checkBoxCheckedColor};
  border-radius: 50%;
  border:  ${vars.colors.checkBoxCheckedColor};
}
.ant-checkbox-wrapper-checked span.ant-checkbox-inner{
  ::after{
      border: 2px solid #fff;
      border-top: 0;
      border-left: 0;
  }
}
.ant-checkbox + span {
  color: ${vars.colors.checkboxTextColor};
  font-size: 12px;
}
`;
export default DefaultStyle;
