/**
 *
 * Rating
 *
 */
import React from 'react';
import Rate from 'antd/lib/rate';

class Rating extends React.PureComponent<any, any> {
   public render() {
    const {...antProps} = this.props;
    return <Rate {...antProps}/>
    }
}
export default Rating;
