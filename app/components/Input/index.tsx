import React, { Component } from 'react';
import DefaultStyle from './Styles';
import AntInput from 'antd/lib/input';

interface InputProps{
    inputType: string;
    placeholder: string;
    prefix?: any;
    suffix?: any;
    placeholderPosition?: any;
    onChange?: any;
    maxLength?: number;
    minLength?: number;
    onFocus?: any;
    onBlur?: any;
    className?: string;
}
class InputComponent extends Component<InputProps, any> {
   public render() {
       // for position of placeholder pass placeholderPosition props
    const { inputType, placeholderPosition, ...antProps } = this.props;
    return (
            <DefaultStyle inputType={inputType} placeholderPosition={placeholderPosition}>
                <AntInput {...antProps} />
            </DefaultStyle>
        );
    }
}
export default InputComponent;
