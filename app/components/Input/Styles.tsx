import styled from 'styled-components';
import vars, { sizes, colors } from 'configs/styleVars';
const cssStyle = (props, inputType) =>{
const cssProps = {
  type1: {
    'box-shadow': '0px 2px 0.8px 0.2px rgba(0, 0, 0, 0.16)',
    'border': 'solid 1px #cccccc',
    'background-color': colors.secondaryBodyBG,
    'border-radius': sizes.borderRadiusS,
    'min-height': `${vars.sizes.formFieldMinHeight}`,
    'text-align': `${props.placeholderPosition}`,
    'color': 'rgba(119, 119, 119, 0.7);',
    'width': '100%',
    'object-fit' : 'contain',
    'margin-bottom': '10px',
  },
}
return cssProps[inputType];
}
const DefaultStyle = styled<any, any>('div')`
.ant-input{
    ${props => {
      return cssStyle(props, props.inputType)
    }
   }
}

`;
export default DefaultStyle;