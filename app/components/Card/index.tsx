import React, { Component } from 'react';
import DefaultStyle from './Styles';
class Card extends Component<any, any> { // implement proptype with interface
    // cardType:-
    // layout1 : card without any background image 
    // layout2 : card without any background image with full Width
    // layout3 : card with background image
    // layout4 : transparent card
    // layout5 : small card
    // layout7 : card with Floating left or right background image;
    public render() { // TODO: need to paas SideImg props as object
        const {backgroundPosition, sideImgTop, sideImgRight, sideImg, cardType, backgroundImg, onClick } = this.props;
        return (
            <DefaultStyle cardType={cardType} backgroundImg={backgroundImg}
                sideImgRight={sideImgRight} sideImgTop={sideImgTop} onClick={onClick}
                backgroundPosition={backgroundPosition}>
                {sideImg && <img src={sideImg} alt="Side Image" className="SideImg" />}
                {this.props.children}
            </DefaultStyle>
        );
    }
}
export default Card;
