import styled from 'styled-components';
import { sizes, colors } from 'configs/styleVars';
const cssStyle = (props, cardType) => { 
  const cssProps = {
    layout1: {
      'background' : colors.secondaryBodyBG,
      'border-radius' : sizes.borderRadiusL,
      'margin' : 'auto',
      'background-size': 'cover',
    },
    layout2: {
      'background' : colors.secondaryBodyBG,
      'margin' : 'auto',
      'background-size': 'cover',
    },
    layout3: {
      // 'background' : colors.primaryBodyBG,
      'border-radius' : sizes.borderRadiusL,
      'background-image':  `url(${props.backgroundImg})`,
      'margin' : 'auto',
      'background-size': 'cover',
      'background-position': 'left',
      'min-height': '400px', // need to remove later
    },
    layout4: {
      'height': '350px',
      'background' : colors.primaryBodyBG,
      'border-radius' : sizes.borderRadiusL,
      'margin' : 'auto',
      'background-size': 'cover',
    },
    layout5: {
      'background' : colors.secondaryBodyBG,
      'border-radius' : sizes.borderRadiusS,
      'margin' : 'auto',
      'box-shadow': '0px 3px 7.4px 0.6px rgba(17, 49, 132, 0.18)',
      'border': 'solid 1px #c2cadf',
      'background-size': 'cover',
    },
    layout7: {
      'background' : colors.secondaryBodyBG,
      'border-radius' : sizes.borderRadiusL,
      'background-image':  `url(${props.backgroundImg})`,
      'margin' : 'auto',
      'background-size': 'contain',
      'background-position': `${props.backgroundPosition}`,
      'background-repeat': 'no-repeat',
      'min-height': '300px',
    },
  };
  return cssProps[cardType];
}

const DefaultStyle = styled<any, any>('div')`
  .SideImg{
    position: absolute;
    right:${props => props.sideImgRight ? props.sideImgRight : '8rem'};
    top: ${props => props.sideImgTop ? props.sideImgTop : '0rem'};
    width: 37rem;
  }
    ${props => {
      return cssStyle(props, props.cardType);
    }
   }`;
export default DefaultStyle;
