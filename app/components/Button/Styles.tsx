import styled from 'styled-components';
import { sizes, colors } from 'configs/styleVars';
const cssStyle = {
  bordered: {
      'border': '2px solid #113184',
      'border-radius': sizes.borderRadiusL,
      'background' : colors.secondaryBodyBG,
      'color': colors.enabledButtonBG,
  },
  filled: {
      'border-radius': sizes.borderRadiusM,
      'background' : colors.enabledButtonBG,
      'color': colors.secondaryBodyBG,
  },
  shadowed: {
      'border-radius': sizes.borderRadiusS,
      'box-shadow' : '0px 3px 12px 3px rgba(0, 0, 0, 0.15)',
      'color':colors.enabledButtonBG,
  },
  disabled: {
    'border-radius': sizes.borderRadiusM,
    'background' : 'rgb(175, 175, 175)',
    'color': colors.secondaryBodyBG,
    'cursor': 'not-allowed'
  }
}
const DefaultStyle = styled<any, any>('div')`
   .ant-btn{
    ${props => {
        if(props.disabled){
            return {...cssStyle['disabled']}
        } else{
            return cssStyle[props.buttonType];
        }
    }
   }
}
`;
export default DefaultStyle;