import React, { Component } from 'react';
import DefaultStyle from './Styles';
import Button from 'antd/lib/button';
interface ButtonProps{
    buttonType: string;
    label: string;
    onClick?: any;
    htmlType?: any;
    disabled?: boolean;
}
class ButtonComponent extends Component<ButtonProps, any> {
   public render() {
    const { disabled, buttonType, label, ...antProps } = this.props; // buttonType: bordered, shadowed, filled
    return (
            <DefaultStyle buttonType={buttonType} disabled={disabled}>
                <Button {...antProps}>
                      {label}
                </Button>
            </DefaultStyle>
        )
    }
}
export default ButtonComponent;