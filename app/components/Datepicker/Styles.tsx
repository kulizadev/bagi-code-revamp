import styled from 'styled-components';
import vars from 'configs/styleVars';
const cssStyle = (props, cardType, padding) =>{ 
const cssProps = {
  type1: {
    'box-shadow': '0px 2px 0.8px 0.2px rgba(0, 0, 0, 0.16)',
    'border': 'solid 1px #cccccc',
    'background-color': `${vars.colors.secondaryBodyBG}`,
    'border-radius':  `${vars.sizes.borderRadiusS}`,
    'min-height': `${vars.sizes.formFieldMinHeight}`,
    'text-align': `${props.placeholderposition}`,
    'color': `${vars.fonts.fontColorInputBox}`,
    'font-size': `${vars.fonts.fontSizeText}`,
    'font-weight': `${vars.fonts.fontWeightHeadingOne}`,
    'width': '100%',
    'padding-left': `${padding}`,
  },
}
return cssProps[cardType];
}
const DefaultStyle = styled<any, any>('div')`
.ant-input{
    ${props => {
      return cssStyle(props, props.inputType, props.prefixImg ? '5rem' : '1rem')
    }
   }
}
img{
  width: 2rem;
  height: 2rem;
  object-fit: contain;
  &.prefix-img{
    position: absolute;
    z-index: 1;
    top: 50%;
    transform: translateY(-50%);
    left: 1.5rem;
  }
}
.ant-calendar-picker{
  width: 100%;
}

`;
export default DefaultStyle;