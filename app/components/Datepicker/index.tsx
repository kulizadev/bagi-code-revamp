import React, { Component } from 'react';
import DefaultStyle from './Styles';
import DatePicker  from 'antd/lib/date-picker';
import calendar from 'images/calendar-icon.png';
import moment from 'moment';
interface DateProps{
    input_type: string;
    placeholder: string;
    suffixIcon?: any;
    prefixImg?: any;
    placeholder_position?: any;
    onChange?: any,
    mode?: any,
    allowClear?:boolean,
    format?: string,
    showToday?: boolean,
    pickerType?: string,
    disableType?: string;
    defaultValue?: any;
    age?: number;
}
class DatepickerComponent extends Component<DateProps, any> {
    componentWillMount(){
    console.log('date picker props componentWillMount',this.props);

    }
    componentDidMount(){
    console.log('date picker props componentDidMount',this.props);

    }
    public disabledDate(current) {
        console.log('date picker props',this.props);
        const {disableType, age} = this.props;
        // let future = false, past = false, dob=false;
        // if(disableType && disableType == 'future'){
        //     future = current && current > moment().endOf('day');
        // }
        // if(disableType && disableType == 'past'){
        //     past = current && current < moment().startOf('days');
        // }
        // if(disableType && disableType == 'dob' && age){
        //     dob =  current && (current.year() >= moment().subtract(age - 1, 'years').year() || current.year() < moment().subtract(age + 1, 'years').year());
        // }
        // return future || past || dob;
        switch (disableType) {
            case 'future':
                return current && current > moment().endOf('day');
            case 'past':
                return current && current < moment().startOf('days'); 
            case 'dob':
                return age ? current && (current.year() >= moment().subtract(age - 1, 'years').year() || current.year() < moment().subtract(age + 1, 'years').year()) : false; 
            default: 
                return false;
          }
    }
    public render() {
        console.log('date picker props',this.props);
        // for position of placeholder pass placeholderposition props
        const { input_type, pickerType, defaultValue, placeholder_position, prefixImg, ...antProps } = this.props;
        const {  MonthPicker } = DatePicker;
        if(pickerType !== 'date'){
            return (
            <DefaultStyle inputType={input_type} placeholderposition={placeholder_position} prefixImg={prefixImg ? true : false}>
                {prefixImg ? <img src={prefixImg} className="prefix-img" /> : ''}
                <DatePicker {...antProps} suffixIcon={<img src={calendar}/>} allowClear={false} showToday={false} disabledDate={this.disabledDate.bind(this)} defaultPickerValue={defaultValue}/>
            </DefaultStyle>);
        }else {
            return (
            <DefaultStyle inputType={input_type} placeholderposition={placeholder_position}>
                {prefixImg ? <img src={prefixImg} className="prefix-img" /> : ''}
                <MonthPicker {...antProps} suffixIcon={<img src={calendar}/>} allowClear={false} disabledDate={this.disabledDate.bind(this)} defaultPickerValue={defaultValue}/>
            </DefaultStyle>);
        }
    }
}
export default DatepickerComponent;
