import styled from 'styled-components';
import vars from 'configs/styleVars';
const DefaultStyle = styled<any, any>('div')`
  .center-loader{
    display: block;
    font-size: 40px;
    color: ${vars.colors.loaderColor};
    height: 100px;
  }
`;
export default DefaultStyle;