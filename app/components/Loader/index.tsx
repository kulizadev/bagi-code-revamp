import React, { Component } from 'react';
import Icon from 'antd/lib/icon'
import DefaultStyle from './Styles';
class Loader extends Component<any, any> { // need to implement as interface props
   public render() {
    return (
            <DefaultStyle>
                 <Icon type="loading" className="center-loader" />
            </DefaultStyle>
        );
    }
}
export default Loader;
