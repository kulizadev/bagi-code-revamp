import React, { Component } from 'react';
import DefaultStyle from './Styles';
import Select  from 'antd/lib/select';
import uuid from 'uuid';
interface SelectProps{
    placeholder: string;
    prefix?: any;
    suffix?: any;
    placeholder_position?: any;
    onChange?: any,
    mode?: string,
    showArrow?: boolean,
    showSearch?: boolean,
    options: any;
}
const Option = Select.Option;

class SelectComponent extends Component<SelectProps, any> {
   public render() {
       // for position of placeholder pass placeholderposition props
    const { options, placeholder_position, ...antProps } = this.props;
    return (
            <DefaultStyle placeholderposition={placeholder_position}>
                <Select {...antProps} notFoundContent="No destination(s) found">
                {
                    (options || []).map(option => <Option key={uuid()} value={option.value || option.id}>{option.label}</Option>)                // TODO Destructure the options object
                }
                </Select>
            </DefaultStyle>
        );
    }
}
export default SelectComponent;
