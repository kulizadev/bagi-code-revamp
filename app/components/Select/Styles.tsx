import styled from 'styled-components';
import vars from 'configs/styleVars';
const cssStyle = (props, cardType) => { 
const cssProps = {
  type1: {
    'border': 'none',
    'min-height': `${vars.sizes.formFieldMinHeight}`,
    'text-align': `${props.placeholderposition}`,
    'color': `${vars.fonts.fontColorInputBox}`,
    'font-size': `${vars.fonts.fontSizeText}`,
    'font-weight': `${vars.fonts.fontWeightHeadingOne}`,
    'object-fit': 'contain',
    'width': '100%',
  },
  type2: {
    'box-shadow': `${vars.borderBoxStyle.boxShadow}`,
    'border': `${vars.borderBoxStyle.border}`,
    'background-color': `${vars.colors.secondaryBodyBG}`,
    'border-radius': `${vars.sizes.borderRadiusS}`,
    'min-height': `${vars.sizes.formFieldMinHeight}`,
    'text-align': `${props.placeholderposition}`,
    'color': `${vars.colors.inputFieldClr}`,
    'padding': `${vars.sizes.paddingS}`,
    'object-fit': 'contain',
    'width': '100%',
  },
}
return cssProps[cardType];
}
const DefaultStyle = styled<any, any>('div')`
.ant-select{
    ${props => {
      return cssStyle(props, "type1")
    }
   }
}
.ant-select-selection{
  ${props => {
    return cssStyle(props, 'type2')
  }
 }
}
.ant-select-selection--multiple .ant-select-selection__clear, .ant-select-selection--multiple .ant-select-arrow{
  top: 50%;
}
.ant-select-selection--multiple .ant-select-selection__choice{
  width: 103px;
}
`;
export default DefaultStyle;