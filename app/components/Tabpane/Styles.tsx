import styled from 'styled-components';
import vars from 'configs/styleVars';
const cssProps = {
  common: {
   'font-size': '16px',
  },
  type1: {
    'background-color': `${vars.colors.enabledButtonBG}`,
    'height': '3px',
  },
  type2: {
    'background-color': 'transparent',
  },
  type3: {
    'background-color': 'transparent',
  },
  type2BorderMarginPadd: {
    'border-radius': '1rem',
    'margin': '0 12px 0 0',
    'padding': '8px 16px',
    'box-shadow': '0px 3px 12px 3px rgba(0, 0, 0, 0.15)',
  },
  type3BorderMarginPadd: {
    'margin': '0 0px 0 0',
    'padding': '3px 14px',
  },
  type1background:{
    'color': `${vars.colors.enabledButtonBG}`,
  },
  type2background:{
    'background': `${vars.colors.buttonActiveTabpane}`,
    'color': 'white',
  },
  type3background:{
    'background': `${vars.colors.buttonActiveTabpane}`,
    'color': 'white',
    'border-radius' : `${vars.sizes.borderRadiusM}`
  },
  type2nonActivebackground:{
    'background':`${vars.colors.secondaryBodyBG}`,
    'color': `${vars.colors.enabledButtonBG}`,
  },
  type3nonActivebackground:{
    'background': `${vars.colors.toggle}`,
    'color': 'rgba(17, 49, 132, 0.5)',
  },
  tabScrollBarBg:{
    'background':  `${vars.colors.toggle}`,
    'border-radius': `${vars.sizes.borderRadiusM}`,
    'display': 'table',
  },
  antTabBarBorderBttom:{
    'border-bottom' : '0px solid',
  },
  tabScrollBarpadding:{
    'padding': '11px 3px 15px 11px',
  }
  
}
const DefaultStyle = styled<any, any>('div')`
.ant-tabs-nav .ant-tabs-tab:hover{
  color: unset;
}

.ant-tabs {
  ${props => props.headingSubMenuTabsWidth ? {'max-width': props.headingSubMenuTabsWidth} : ''
  };
}
.ant-tabs-tab{
  ${props => {
    switch (props.tabType) {
      case 'type1':
        return '';
      case 'type2':
        return cssProps['type2nonActivebackground'] ;
      case 'type3':
        return cssProps['type3nonActivebackground'] ;
      default:
        return '';
    }
  }};
}
.ant-tabs-tab.ant-tabs-tab-active{
  ${props => {
    switch (props.tabType) {
      case 'type1':
        return cssProps['type1background'] ;
      case 'type2':
        return cssProps['type2background'] ;
      case 'type3':
        return cssProps['type3background'] ;
      default:
        return '';
    }
  }};
}
.ant-tabs-ink-bar{
    ${props => {
        switch (props.tabType) {
          case 'type1':
            return cssProps[props.tabType] ;
          case 'type2':
            return cssProps[props.tabType] ;
          case 'type3':
            return cssProps[props.tabType] ;
          default:
            return '';
        }
      }};
}
.ant-tabs-nav .ant-tabs-tab{
    ${props => {
        switch (props.tabType) {
          case 'type1':
            return '' ;
          case 'type2':
            return  cssProps['type2BorderMarginPadd'];
          case 'type3':
            return  cssProps['type3BorderMarginPadd'];
          default:
            return '';
        }
      }};
}
.ant-tabs-nav-scroll{
  ${props => {
    switch (props.tabType) {
     case 'type2':
        return  cssProps['tabScrollBarpadding'];
      case 'type3':
        return  cssProps['tabScrollBarBg'];
      default:
        return '';
    }
  }};
}
.ant-tabs-bar{
  ${props => {
    switch (props.tabType) {
      case 'type1':
        return  cssProps['antTabBarBorderBttom'];
      case 'type2':
        return  cssProps['antTabBarBorderBttom'];
      case 'type3':
        return  cssProps['antTabBarBorderBttom'];
      default:
        return '';
    }
  }};
}

`;
export default DefaultStyle;
