import React, { Component } from 'react';
import DefaultStyle from './Styles';
import AntTabs from 'antd/lib/tabs';
const AntTabPane = AntTabs.TabPane;
interface InputProps{
    tabType: string;
    tabs: any;
    tabBarStyle?: any;
    headingSubMenuTabsWidth?: any;
}
class Tabs extends Component<InputProps, any> {
   public render() {
    const { tabs, tabType, headingSubMenuTabsWidth,  ...antProps } = this.props;
    return (
            <DefaultStyle tabType={tabType} headingSubMenuTabsWidth={headingSubMenuTabsWidth} >
               <AntTabs {...antProps}>
                {
                    tabs.map((tab) => {
                    return <AntTabPane tab={tab.heading} key={tab.key}>{tab.children}</AntTabPane>
                    })
                }
                </AntTabs>
            </DefaultStyle>
        );
    }
}
export default Tabs;
