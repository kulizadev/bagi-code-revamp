import styled from 'styled-components';
import vars from 'configs/styleVars';
const cssProps = {
    sliderRail:{
        'background-color': `${vars.colors.slider}`,
        'height': '2px',
    },
    sliderTrack:{
        'background-color': `${vars.colors.slider}`,
        'height': '2px',
    },
    sliderDot:{
        'position': 'absolute',
        'top': '-5px',
        'height': '12px',
        'margin-left': '-2px',
        'background-color': `${vars.colors.secondaryBodyBG}`,
        'border': '1px solid #5580e9',
        'cursor': 'pointer',
        'width': '0px',
    },
    sliderHandler:{
        'position': 'absolute',
        'width': '20px',
        'height': '20px',
        'margin-top': '-10px',
        'margin-left': '-10px',
        'background-color': `${vars.colors.secondaryBodyBG}`,
        'border': 'solid 1px #5580e9',
        'border-radius': '50%',
    }
}
const DefaultStyle = styled<any, any>('div')`
.ant-slider-rail{
  ${props => {
    switch (props.sliderType) {
      case 'type1':
        return cssProps['sliderRail'];
      case 'type2':
        return cssProps['sliderRail'] ;
      default:
        return '';
    }
  }};
}
.ant-slider-track{
    ${props => {
        switch (props.sliderType) {
          case 'type1':
            return cssProps['sliderTrack'];
          case 'type2':
            return cssProps['sliderTrack'] ;
          default:
            return '';
        }
      }};
}
.ant-slider-dot{
    ${props => {
        switch (props.sliderType) {
          case 'type1':
            return cssProps['sliderDot'];
          case 'type2':
            return cssProps['sliderDot'];
          default:
            return '';
        }
      }};
}
.ant-slider-handle{
    ${props => {
        switch (props.sliderType) {
          case 'type1':
            return cssProps['sliderHandler'];
          case 'type2':
            return cssProps['sliderHandler'];
          default:
            return '';
        }
      }};
}
`;
export default DefaultStyle;
