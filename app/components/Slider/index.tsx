import React, { Component } from 'react';
import DefaultStyle from './Styles';
import AntSlider from 'antd/lib/slider';
class Slider extends Component<any, any> {
   public render() {
    const { sliderType,  ...antProps } = this.props;
    return (
            <DefaultStyle sliderType={sliderType}>
               <AntSlider  {...antProps} />
            </DefaultStyle>
        );
    }
}
export default Slider;
