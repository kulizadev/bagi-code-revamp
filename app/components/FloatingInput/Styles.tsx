import styled from 'styled-components';
import vars, { sizes, colors } from 'configs/styleVars';
const cssStyle = (props, inputType) =>{ 
const cssProps = {
  type1: {
    'box-shadow': '0px 2px 0.8px 0.2px rgba(0, 0, 0, 0.16)',
    'border': 'solid 1px #cccccc',
    'background-color': colors.secondaryBodyBG,
    'border-radius': sizes.borderRadiusS,
    'min-height': `${vars.sizes.formFieldMinHeight}`,
    'text-align': `${props.placeholderPosition}`,
    'color': 'rgba(119, 119, 119, 0.7);',
    'width': '100%',
    'object-fit' : 'contain',
    'margin-bottom': '10px',
  },
}
return cssProps[inputType];
}
const DefaultStyle = styled<any, any>('div')`
.ant-input{
    ${props => {
      return cssStyle(props, props.inputType)
    }
   }
}
&{
    position: relative;
    overflow: hidden;
    margin-bottom: 0px;
}
label.floating-label{
      position: absolute;
      transition: background 0.2s, color 0.2s, top 0.2s, bottom 0.2s, right 0.2s, left 0.2s;
      z-index: -1;
      .anticon.anticon-close{
        position: absolute;
        right: 4px;
        font-size: 10px;
        top: 5px;
      }
  }
input:focus + label.floating-label, input:not([value=""]) + label.floating-label{
      border-top-left-radius: 10px;
      background: #e2e6f0;
      border-top-right-radius: 10px;
      left: 0px;
      z-index: 2;
      width: 100%;
     
      max-height: 23px;
      color: #847c7c;
      font-size: 13px;
  }
input[value=""].ant-input.floating-label-input{
  position: relative;
  top: 0;
  left: 0;
  z-index: 1;
  font-size: 13px;
  background-color: #eeeeee;
  margin-bottom: 10px;
  box-shadow: 0px 2px 0px 0px #dedede;
}
input[value=""].floating-label-input.ant-input:focus, input:not([value=""]).floating-label-input.ant-input {
  padding: 5px;
  padding-top: 25px;
  background: white;
}
input[value=""].floating-label-input.ant-input:focus, input[value=""].floating-label-input.ant-input:hover, input:not([value=""]).floating-label-input.ant-input{
  border-color: #40a9ff;
  font-weight: bold;
}
label.floating-label{
    .close-btn{
        position: absolute;
        right: 4px;
        top: 7px;
        font-size: 10px;
        cursor: pointer;
      }
      span{
        position: relative;
        left: 9px;
        font-size: 12px;
      }
}
.close-btn-input{
    position: absolute;
    top: 5px;
    right: 5px;
    z-index: 2;
    font-size: 12px;
    color: grey;
}
.floating-label-input::-webkit-input-placeholder { color:grey; text-align: center; font-size: 16px;}
.floating-label-input::-moz-placeholder { color:grey; text-align: center; font-size: 16px;} /* FF 4-18 */
.floating-label-input::-moz-placeholder { color:grey; text-align: center; font-size: 16px;} /* FF 19+ */
.floating-label-input::-ms-input-placeholder { color:grey; text-align: center; font-size: 16px;} /* IE 10+ */
`;
export default DefaultStyle;