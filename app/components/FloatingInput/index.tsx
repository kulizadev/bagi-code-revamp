import React, { Component } from 'react';
import DefaultStyle from './Styles';
import Input from 'antd/lib/input';
import Icon from 'antd/lib/icon';
interface InputProps{
    inputType: string;
    placeholder: string;
    onFocusPlaceholder?: string;
    prefix?: any;
    suffix?: any;
    placeholderPosition?: any;
    onChange?: any,
    maxLength?:number,
    minLength? :number,
    onFocus?: any,
    onBlur?: any,
    className?: string,
}
class InputFloatComponent extends Component<InputProps, any> {
   public render() {
    const { inputType, placeholderPosition, onFocusPlaceholder, placeholder, ...antProps } = this.props;
    return (
            <DefaultStyle inputType={inputType} placeholderPosition={placeholderPosition}>
                <Icon
                    type="close"
                    // tslint:disable-next-line: jsx-no-lambda
                    //   onClick={() => this.clearInput(val.type)}
                    className='close-btn-input'
                />
                <Input {...antProps} onFocus={e =>(e.target.placeholder = onFocusPlaceholder ? onFocusPlaceholder : '') } onBlur={e => (e.target.placeholder = placeholder)} placeholder={placeholder} className="floating-label-input"/>
                <label className="floating-label">
                    <span>{placeholder}</span>
                    <Icon
                    type="close"
                    /></label>
            </DefaultStyle>
        );
    }
}
export default InputFloatComponent;
