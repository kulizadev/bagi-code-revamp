import styled from 'styled-components';
import vars from 'configs/styleVars';
const DefaultStyle = styled.span`
    background-color: ${vars.colors.toggle};
    border-radius: ${vars.sizes.borderRadiusXL};
    padding: 8px 0px 7px 0px;
    color:${vars.colors.primaryBodyBG};
    input[type="radio"].toggle {
        display: none;
    }
    input[type="radio"].toggle + label {
        font-size: 11px;
        color: rgba(17, 49, 132, 0.5);
    }
    .toggle-label {
        display: inline-block;
        padding: ${vars.sizes.paddingS};
        position: relative;
        text-align: center;
        padding: 3px 11px 4px 8px;
        transition: background-color 0.1s ease;
    }

    input[type="radio"].toggle:checked + label {
        border-radius: 50px;
        color:${vars.colors.secondaryBodyBG};
        background-color:${vars.colors.enabledButtonBG};
        padding: 5px 13px 7px 13px;
    }

    input[type="radio"].toggle.toggle-right + label {
    }
    @media (max-width: 760px) {
        &.comprehensive-thirdparty-switch {
            display: inline;
        }
        &.responsive-switch input[type="radio"].toggle + label{
            min-width: 50px;
        }
    }
`;

export default DefaultStyle;
