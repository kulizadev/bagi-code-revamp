import React from 'react';

import DefaultStyle from './Styles';
interface CustomToggleProps {
    onClick(event: any): void;
    rightTitle: string;
    leftTitle: string;
    className?: string;
    name: string;
    id1?: string;
    id2?: string;
    defaultChecked ?: boolean;
}
const ToggleComponent = (props: CustomToggleProps) => {
    return (
        <DefaultStyle className={props.className}>
            <input
                id={props.id1}
                onClick={props.onClick}
                className="toggle toggle-left"
                name={props.name}
                type="radio"
                checked={props.defaultChecked}/>
            <label htmlFor={props.id1} className="toggle-label">{props.leftTitle}</label>
            <input
                id={props.id2}
                onClick={props.onClick}
                className="toggle toggle-right"
                name={props.name}
                type="radio"
                checked={!props.defaultChecked}/>
            <label htmlFor={props.id2} className="toggle-label">{props.rightTitle}</label>
        </DefaultStyle>
    );
};
export default ToggleComponent;


