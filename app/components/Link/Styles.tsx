import styled from 'styled-components';
import vars from 'configs/styleVars'
import { Link } from 'react-router-dom';

const DefaultStyle = styled(Link)`
    font-size: 12px;
    font-weight: 500;
    margin-bottom: 10px;

    &.link-text {
        color: ${vars.colors.linkText};
    }&.link-text:focus {
        text-decoration: underline;
    }
`;

export default DefaultStyle;
