
import React from 'react';
import DefaultStyle from './Styles';
interface LinkComponentProps {
    text: string;
    textStyle: 'link-text';
    path: string;
}
const LinkComponent = (props: LinkComponentProps) => {
    return(
            <DefaultStyle to={props.path} className={props.textStyle}>{props.text}</DefaultStyle>
    );
};
export default LinkComponent;
