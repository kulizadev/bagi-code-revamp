
import React, { Component } from 'react'
import popoverIcon from 'images/popoverIcon.png';
class PopoverIcon extends Component {
   public render() {
        return (
            <div>
                <img src={popoverIcon} alt="popover icon"/>
            </div>
        );
    }
}
export default PopoverIcon;