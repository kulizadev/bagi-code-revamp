import React, { Component } from 'react';
import DefaultStyle from './Styles';
import Breadcrumb from 'antd/lib/breadcrumb';
class BreadcrumbComponent extends Component<any, any> {
   public render() {
    const { ...antProps } = this.props;
    return (
               <DefaultStyle>
                    <Breadcrumb {...antProps}>
                    {this.props.breadCrumbValues.map((item, i) => {
                         return <Breadcrumb.Item key={i} href={item.href}>{item.title}</Breadcrumb.Item>;
                       })}
                    </Breadcrumb>
                </DefaultStyle>
        );
    }
}
export default BreadcrumbComponent;
