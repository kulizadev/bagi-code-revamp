import styled from 'styled-components';
import { colors } from 'configs/styleVars';
const DefaultStyle = styled<any, any>('div')`
a.ant-breadcrumb-link,  a.ant-breadcrumb-link:hover {
    text-decoration: none;
    color: ${colors.enabledButtonBG};
}
span:last-child a.ant-breadcrumb-link{
    color: ${colors.breadCrumbLink};
}
`;
export default DefaultStyle;