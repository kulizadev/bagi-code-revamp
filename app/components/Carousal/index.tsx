import React, { Component } from 'react';
import DefaultStyle from './Styles';
import rightArrowC from 'images/rightArrowC.png';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import AntCarousel from 'antd/lib/carousel';
class Carousal extends Component<any, any> {
   public render() {
    let slider;
    const nextSlider = () => {
      slider.next();
    };
    const prevSlider = e => {
      slider.prev();
    };
    const { showArrow, ...antProps } = this.props;
    return (
            <DefaultStyle>
                <Row>
                    <Col lg={showArrow ? 2 : 0} className="left-arrow">
                    <img src={rightArrowC} alt="Right Arrow"  onClick={nextSlider} className="left-arrow-img"/>
                    </Col>
                    <Col lg={showArrow ? 20 : 24} className="carousal-content">
                            <AntCarousel { ...antProps}  ref={node => (slider = node)} >
                                {this.props.children}
                            </AntCarousel>
                    </Col>
                   <Col lg={showArrow ? 2 : 0} className="right-arrow">
                    <img src={rightArrowC} alt="Right Arrow"
                     onClick={prevSlider} className="right-arrow-img"/>
                    </Col>
                </Row>
            </DefaultStyle>
        );
    }
}
export default Carousal;
