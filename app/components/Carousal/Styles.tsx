import styled from 'styled-components';
import vars from 'configs/styleVars';
const DefaultStyle = styled<any, any>('div')`
  position: relative;
  margin-bottom: 2rem;
  .anticon{
      position: absolute;
      top: 60px;
    }
  .ant-carousel .slick-dots li button{
    width: 8px;
    height: 9px;
    border-radius: 50%;
    background: ${vars.colors.carousalDots};
}
// .ant-carousel .slick-dots-bottom{
//   bottom: -13px;
// }
 .left-arrow-img{
    transform: rotate(180deg);
    cursor: pointer;
 }
 .right-arrow-img{
    cursor: pointer;
 }
  .ant-carousel .slick-dots-bottom{
    bottom: -13px;
  }
  .left-arrow{
    position: absolute;
    top: 50%;
    left: 8rem;
  }
  .right-arrow{
      position: absolute;
      top: 50%;
      right: 0%;
  }
  .carousal-content{
      float: none;
      margin: auto;
  }
}`;
export default DefaultStyle;
