/*
 *
 * Heading
 *
 */
import React from 'react';
import DefaultStyle from './Styles';
import Typography from 'antd/lib/typography';

const { Title } = Typography;

class Heading extends React.PureComponent<any, any> {
   public render() {
    const { headingType, label, ...antProps } = this.props;
    return (
            <DefaultStyle headingType={headingType}>
                <Title {...antProps}>
                      {label}
                </Title>
            </DefaultStyle>
        );
    }
}
export default Heading;
