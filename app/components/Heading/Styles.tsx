import styled from 'styled-components';
import { fonts, colors } from 'configs/styleVars';

const cssStyle = {
  headingOne: {
    'font': `${fonts.fontSizeHeadingOne}  ${fonts.fontFamilyPrimary}`,
    'font-weight': fonts.fontWeightHeadingOne,
    'line-height': fonts.lineHeightH1,
    'color': colors.headingTextColor,
  },
  headingTwo: {
    'font': `${fonts.fontSizeHeadingTwo}  ${fonts.fontFamilyPrimary}`,
    'font-weight': fonts.fontWeightPrimary,
    'line-height': fonts.lineHeightH2,
    'color': colors.headingTextColor,
  },
  headingThree: {
    'font': `${fonts.fontSizeHeadingThree}  ${fonts.fontFamilyPrimary}`,
    'font-weight': fonts.fontWeightPrimary,
    'color': colors.headingTextColor,
  },
  headingFour: {
    'font': `${fonts.fontSizeButton}  ${fonts.fontFamilyPrimary}`,
    'font-weight': fonts.fontWeightHeadingOne,
    'color': colors.headingTextColor,
  },
}

const DefaultStyle = styled<any, any>('div')`
    h1.ant-typography, .ant-typography h1{
    ${props => {
      return cssStyle[props.headingType];
    }
   }
}
`;
export default DefaultStyle;
