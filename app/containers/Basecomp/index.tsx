// File to test base components
import React, { Component } from 'react';
import Card from 'components/Card';
import SelectComponent from 'components/Select';
import DatepickerComponent from 'components/Datepicker';
import InputFloatComponent from 'components/FloatingInput';
import ToggleComponent from 'components/Toggle';
import GlobalConstants from 'constants/globalConstants';
// import Option from 'antd/lib/select';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import moment from 'moment';
import { request } from 'configs/networklayer';
import BaseUrls from 'constants/apiConstants';
import ButtonComponent from 'components/Button';
// import Header from 'constants/headerConstants';
export default class Basecomp extends Component {
    public componentDidMount() {
        const postData = { countries: ["SCH", "USA"], travellers: [{ "type": "self", "age": 24 }, { "type": "spouse", "age": 22 }], start_date: "2019-04-24", end_date: "2019-05-05", mobile_no: 8828328988, email_id: 'adsds@dddd.ddd' };
        const postData1 = null;
        const url = BaseUrls.PythonUrl + BaseUrls.getQouteTravel;
        const url1 = BaseUrls.DrupalUrl + BaseUrls.NavbarHeaderUrl;
        const authorization = {};
        const authorization1 = 'Basic ' + btoa(BaseUrls.DrupalCredential.username + ':' + BaseUrls.DrupalCredential.password);
        const sendingMethod = 'post'
        const sendingMethod1 = 'get'
        // travelGetQoutePostApi(postData).then(val => console.log('sdadasd',val));
        request(url, authorization, sendingMethod, postData).then(val => {
            //   this.props.setTravelData(val);
            console.log('response from network layer', val);
        });
        //Header section api
        request(url1, authorization1, sendingMethod1, postData1).then(val => {
            //   this.props.setTravelData(val);
            console.log('response from header', val);
        });
    }
    public render() {
        const { travelInsuranceQuote } = GlobalConstants;
        let children = new Array();
        for (let i = 10; i < 36; i++) {
            children.push({ value: i, id: i, label: i + 1 });
        }
        return ( // make every props as small caps
            <div>
                <Card card_type="layout1">
                    <Row>
                        <Col md={12} lg={12}>
                            <SelectComponent placeholder={travelInsuranceQuote} showArrow={true} showSearch={true} options={children} />
                            <SelectComponent placeholder={travelInsuranceQuote} mode='multiple' showArrow={true} showSearch={true} options={children} />
                            <DatepickerComponent input_type="type1" placeholder="dd-mmm-yyyy" format="DD-MMM-YYYY" pickerType='asa' disableType='dob' defaultValue={moment('12-dec-1999')} age={20} />
                            <DatepickerComponent input_type="type1" placeholder="dd-mmm-yyyy" format="DD-MMM-YYYY" pickerType='date' disableType='past' />
                            <DatepickerComponent input_type="type1" placeholder="dd-mmm-yyyy" format="DD-MMM-YYYY" pickerType='date' disableType='future' />
                            <InputFloatComponent inputType='type1' placeholder='please select an input' />
                            <ToggleComponent rightTitle='No' leftTitle='Yes' id1='emig1' id2='emig2' name='emigrant' onClick={() => console.log('kk')} />
                            <ButtonComponent
                                htmlType="submit"
                                label='Get Quote'
                                buttonType='bordered'
                            />
                        </Col>
                    </Row>
                </Card>
            </div>
        )
    }
}
