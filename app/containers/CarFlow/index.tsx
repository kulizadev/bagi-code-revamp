import React, { Component } from 'react';
import { Switch, Route, Router } from 'react-router-dom';
import history from 'utils/history';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { CarRollOverRouting } from 'constants/routingPathConstants';
export class CarRolloverFlow extends Component<any, any> {
    public state = {
        loading: true,
    }
    public render() {
        const { match } = this.props;
        console.log('this.props', this.props);
        return (
            <div>
                <Router history={history}>
                    <React.Fragment>
                        <Switch>
                            {CarRollOverRouting.map((val, ind) => {
                                return (
                                    <Route exact path={match.path + val.path} render={() => <val.component  {...this.props} />} key={ind} />
                                );
                            })
                            }
                            <Route component={NotFoundPage} />
                        </Switch>
                    </React.Fragment>
                </Router>
            </div>
        );
    }
}