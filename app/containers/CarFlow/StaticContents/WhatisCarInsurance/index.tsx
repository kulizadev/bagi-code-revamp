import React from 'react';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import renderHTML from 'react-render-html';
import { DefaultStyle } from './Styles';
import Card from 'components/Card';
import Heading from 'components/Heading';

const WhatIsCarInsurance = props => {
    return (
        <DefaultStyle>
            <Row type="flex" justify="center">
                <Col md={22} xs={24}  lg={22} sm={24}>
                    <Heading headingType="headingTwo" 
                        label={props.heading}
                        className="heading"
                    />
                </Col>
            </Row>
             <Row type="flex" justify="center">
                <Col md={22} xs={24}  lg={22} sm={24}>
                    <Card cardType="layout1">
                        <div className="para-and-header">
                        <Row>
                            <Col>
                            <Heading headingType="headingFour" 
                                label="What is Car Insurance?"
                                className="car-sub-heading"
                            />
                            </Col>
                        </Row>
                            {renderHTML(props.paragraph)}
                        </div>
                    </Card>
                </Col>
             </Row>
        </DefaultStyle>
    );
};

export default WhatIsCarInsurance;
