import styled from 'styled-components';
// import vars from 'configs/styleVars';
export const DefaultStyle = styled.div`{
    .biggerBLogImg{
        width: 95%;
        height: 403px;
        border-radius: 14px;
    }
    .heading{
        text-align: center;
    }
    .smallBlogImg{
        width: 200px;
        height: 120px;
        padding-right: 10px;
        border-radius: 14px;
        margin-bottom: 15px;
    }
    .guide-text {
        position: relative;
        bottom: 67px;
        padding-left: 14px;
        color: white;
        background: rgba(1, 1, 1, 0.34);
        width: 95%;
        border-bottom-left-radius: 15px;
        border-bottom-right-radius: 15px;
    }
    .guide-to-buy {
        color: white;
    }
    .travel-and-date {
        display: flex;
    }
    .travelblog {
        background: #f28a8f;
        margin: 0px;
        padding: 0px 10px;
        display: table;
        color: white;
        margin-right: 10px;
    }
    .travelblogheader {
        font-size: 16px;
    }
    .see-more{
        text-align:center;
    }
}`;

