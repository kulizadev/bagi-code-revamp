import React from "react";
import { Row, Col } from "antd";
import { DefaultStyle } from "./Styles";
import GlobalConstants from 'constants/globalConstants';
// import BaseUrls from 'constants/apiConfig';
import Card from 'components/Card';
import Heading from 'components/Heading';
const BlogContent = props => {
  // console.log('travel blogs.......', props);
  // console.log('travel blogs.......', props);
     const { seeMore} = GlobalConstants;
    //  const {travelBlog} = BaseUrls;
     return (
    <DefaultStyle>
      <Row type="flex" justify="center">
        <Col md={22} xs={24} lg={22} sm={24}>
          <Heading headingType="headingThree" 
                label={props.blogHeading}
                className="heading"
            />
          <Card cardType="layout4">
            <Row>
              <Col md={15} lg={15}>
                {props.contentArr.length > 0 &&
                  props.contentArr
                    .filter(val => val.sticky === "True")
                    .map((val, ind) => {
                      return (
                        <div key={ind}>
                          <img
                            className="biggerBLogImg"
                            src={val.uri}
                            alt="biggerblogs"
                          />
                          <div className="guide-text">
                            <h3 className="guide-to-buy">{val.title}</h3>
                            <div className="travel-and-date">
                              <p className="travelblog">
                                {" "}
                                {val.field_blog_tags}
                              </p>
                              <p>{val.field_date}</p>
                            </div>
                          </div>
                        </div>
                      );
                    })}
              </Col>
              <Col md={9} lg={9}>
                {props.contentArr.length > 0 &&
                  props.contentArr
                    .filter(val => val.sticky === "False")
                    .map((val, ind) => {
                      return (
                        <Row key={ind}>
                          <Col md={12} lg={12} xs={10}>
                            <img
                              className="smallBlogImg"
                              src={val.uri}
                              alt="blogs"
                            />
                          </Col>
                          <Col xs={1} />
                          <Col md={12} lg={12} xs={13}>
                            <div>
                              <p className="travelblog">
                                #{val.field_blog_tags}
                              </p>
                              <h2 className="travelblogheader">{val.title}</h2>
                              <p>{val.field_date}</p>
                            </div>
                          </Col>
                        </Row>
                      );
                    })}
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <Row>
      <Row type="flex" justify="center">
          <Col md={22} xs={24} lg={22} sm={24} className="see-more">
           <a href='#' target="_blank" className="see-more-faq">{seeMore}</a>
        </Col>
          </Row>
      </Row>
    </DefaultStyle>
  );
};
export default BlogContent;
