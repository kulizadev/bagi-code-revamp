import React from 'react';
import DefaultStyle from './Styles';
import {Row, Col} from 'antd';
interface AddOnsContentProps {
    keyValue: number;
    imageSrc: string;
    header: string;
    content: string;
}
const AddonsContentComponent = (props: AddOnsContentProps) => {
    const { keyValue, imageSrc, header, content} = props;
    return (
        <DefaultStyle>
           <Col xs={24} md={12} lg={12} className="add-ons-content"  key={keyValue}>
                <Row>
                    <Col xs={4} sm={2} md={8} lg={6} className="addOnsBackgroundImage">
                        <img src={imageSrc} alt="" className="circle-image addons-image"/>
                    </Col>
                    <Col xs={12} sm={12} md={14} lg={16} className="content">
                        <Row className="sub-header"><h3>{header}</h3></Row>
                        <Row className="web-content">{content}</Row>
                    </Col>
                </Row>
                <Row className="mobile-content">{content}</Row>
            </Col>
        </DefaultStyle>
    );
};
export default AddonsContentComponent;


