import styled from 'styled-components';
// import Spacing from 'styles/StylingConstants';
// import Colors from 'styles/colors';
import ellipseImage from 'images/global_images/ellipse.png';
import ellipseGreyImage from 'images/global_images/ellipse-grey.png';
const DefaultStyle = styled.div`
    & .addOnsBackgroundImage {
        background-image: url(${ellipseImage});
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        height: 100px;
        width: 140px;
        position: relative;
    }

    & .content{
        margin-left:10px;
    }
    & .add-ons-content {
        margin-top: 20px;
    }

    & .mobile-content {
        display: none;
    }
    .circle-image{
        left: 39px;
        position: absolute;
        top: 19px;
        height: 63px;
        width: 63px;
        background-size: contain;
    }
    .sub-header{
        font-size:16px;
    }
    .web-content {
        font-size:12px;
    }
    .heading{
        margin-top:60px;
    }
    .show-more-show-less{
        text-align: center;
    }

    @media (max-width: 760px) {
        & .content {
            margin-left: 10px;
        }
        & .mobile-content {
            display: block;
        }

        & .web-content {
            display: none;
        }
        & .addOnsBackgroundImage {
            background-image: url(${ellipseGreyImage});
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            height: 100px;
            width: 100px;
            position: relative;
        }
    }
`;

export default DefaultStyle;


