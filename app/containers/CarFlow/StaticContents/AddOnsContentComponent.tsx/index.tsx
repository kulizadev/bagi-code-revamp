import React, { Component } from "react";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
// import renderHTML from 'react-render-html';
import DefaultStyle from './Styles';
import Card from 'components/Card';
import Heading from 'components/Heading';
import AddonsContentComponent from './AddonsContent';
import dottie from 'dottie';

export default class AddOnsContentComponent extends Component<any, any> {
    public state= {
      itemsToShow: 4,
      expanded: false,
    }
    public showMore=()=> {
      this.state.itemsToShow === 4 ? (
        this.setState({ itemsToShow: this.props.carContent.length, expanded: true })
      ) : (
        this.setState({ itemsToShow: 4, expanded: false })
      )
    }
    public render() {
    const { title, carContent } = this.props;
    return (
      <DefaultStyle>
        <Row type="flex" justify="center">
          <Heading headingType="headingTwo" label={title} className="heading" />
        </Row>
        <Row type="flex" justify="center">
          <Col md={22} xs={24} lg={22} sm={24}>
            <Card cardType="layout4">
              <Row>
                {carContent.slice(0,this.state.itemsToShow).map((data, index) => {
                  return (
                    <AddonsContentComponent
                      keyValue={index}
                      imageSrc={dottie.get(data,'field_paragraph_image.0.url','')}
                      header={dottie.get(data,'field_paragraph_heading.0.value', '')}
                      content={dottie.get(data, 'field_paragraph_body.0.value', '')}
                    />
                  );
                })}
              </Row>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="show-more-show-less">
                <a onClick={this.showMore}>
                  {this.state.expanded ? (
                    <span>View less</span>
                  ) : (
                    <span>View more</span>
                  )}
                </a>
            </div>
          </Col>
        </Row>
      </DefaultStyle>
    );
  }
}
