import styled from 'styled-components';
import vars from 'configs/styleVars';
export const DefaultStyle = styled.div`{
    .content{
        padding: 44px 0px 27px 51px;
    }
    .car-content-card-header>p:first-child {
        font-size:20px;
    }
    .car-content-card-header{
        font-size: 12px;
    }

    .car-content-card-header th{
        background-color: ${vars.colors.primaryBodyBG};
    }

    .car-content-card-header th, .car-content-card-header td{
        border: 1px solid #ddd;
        padding: 8px;
    }
    .content-heading {
        text-align: center;
        margin-top:60px;
        font-size: 14px;
    }
    .car-collasion-img{
        height: 300px;
        width: 386px;
        padding: 23px;
        margin: 67px 0px 0px 126px;
    }

}`;

