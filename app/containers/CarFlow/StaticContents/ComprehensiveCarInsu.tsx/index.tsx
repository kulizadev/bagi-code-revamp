import React from 'react';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import sideImg from 'images/global_images/comp-car-icon.png';
import renderHTML from 'react-render-html';
import { DefaultStyle } from './Styles';
import Card from 'components/Card';
import dottie from 'dottie';

const ComprehensiveCarInsurance = props => {
    return (
        <DefaultStyle>
            <Row type="flex" justify="center">
                <Col md={22} xs={24} lg={22} sm={24}>
                    <Row className="content-heading">
                        <Col md={22} xs={24} lg={22} sm={24}>
                            {renderHTML(dottie.get(props.carContentData.node, 'body.0.value', ''))}
                        </Col>
                    </Row>
                    <Card cardType="layout7" backgroundImg={sideImg} backgroundPosition="right">
                        <Row>
                            <Col md={14} xs={24} lg={14} sm={24} className="content">
                                <Row>
                                    <Col className="car-content-card-header">
                                        {renderHTML(dottie.get(props.carContentData.node, 'field_section_description_one.0.value', ''))}
                                    </Col>
                                </Row>
                            </Col>

                            <Col md={10} xs={24} lg={10} sm={24}>
                                <img src={dottie.get(props.carContentData.node,'field_section_image_one.0.url', '')} alt="" className= "car-collasion-img"/>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
        </DefaultStyle>
    );
};

export default ComprehensiveCarInsurance;
