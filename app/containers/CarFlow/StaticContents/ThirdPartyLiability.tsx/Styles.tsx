import styled from 'styled-components';
import vars from 'configs/styleVars';
export const DefaultStyle = styled.div`{
    .content{
        padding: 44px 1px 27px 11px;
    }
    .car-content-card-header>p:first-child {
        font-size:20px;
    }
    .car-content-card-header{
        font-size:12px;
    }

    .car-content-card-header th{
        background-color: ${vars.colors.primaryBodyBG};
    }

    .car-content-card-header th, .car-content-card-header td{
        border: 1px solid #ddd;
        padding: 8px;
    }
    .content-heading {
        text-align: center;
    }
    .car-collasion-img{
        height: 300px;
        width: 300px;
        padding: 23px;
        margin: 20px 0px 0px 30px;
    }
    .thirdparty{
        margin-top:61px;
    }

}`;

