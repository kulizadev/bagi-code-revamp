import React from 'react';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import sideImg from 'images/ellipse-1678-copy-3.png';
import renderHTML from 'react-render-html';
import { DefaultStyle } from './Styles';
import Card from 'components/Card';
import dottie from 'dottie';

const ThirdPartyLiability = props => {
    console.log("kdhfkdshfk", props)
    return (
        <DefaultStyle>
            <Row type="flex" justify="center">
                <Col md={22} xs={24} lg={22} sm={24} className="thirdparty">
                    <Card cardType="layout7" backgroundImg={sideImg} backgroundPosition="left">
                        <Row>
                            <Col md={8} xs={24} lg={8} sm={24}>
                                <img src={dottie.get(props.carContentData.node, 'field_section_image_two.0.url', '')} alt="" className="car-collasion-img" />
                            </Col>
                            <Col md={15} xs={24} lg={15} sm={24} className="content">
                                <Row>
                                    <Col className="car-content-card-header">
                                        {renderHTML(dottie.get(props.carContentData.node, 'field_section_description_two.0.value', ''))}
                                    </Col>
                                </Row>
                            </Col>


                        </Row>
                    </Card>
                </Col>
            </Row>
        </DefaultStyle>
    );
};

export default ThirdPartyLiability;
