import React from 'react';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import dottie from 'dottie';
import renderHTML from 'react-render-html';
import Carousal from 'components/Carousal';
import quoteStart from 'images/corousal.jpg';
import quoteEnd from 'images/crousal-upward.jpg';
import defaultImg from 'images/Homepage-assets/defaultDp.png';
import DefaultStyle from './Styles';
import Button from 'components/Button';
import Heading from 'components/Heading';
 
class CarCustomersCarousel extends React.Component<any,any> {
    render() {
        console.log("carousal data", this.props)
        const data = dottie.get(this.props, 'data');
        if(!data) return null;
        return (
            <DefaultStyle>
                <Row type="flex" justify="center">
                    <Col lg={22} md={22}>
                    <Heading headingType="headingThree" 
                            label={this.props.customerReviewSec}
                            className="heading"
                        />
                    </Col>
                </Row>
                <Row>
                    <div style={{display: 'flex', justifyContent: 'center'}}>
                        <div className="avg-rating-head">
                            <span className="rated-text">Rated </span>
                            <span className="rated-head">{this.props.overallrating}</span>
                        </div>
                        <div className="avg-rating-txt">
                            <strong>{this.props.averageRatingTxt}</strong>
                        </div>
                    </div>
              </Row>
                <Carousal showArrow={true}>
                    {data ? data.map((val,index) => {
                        return (
                            <div key={index}>
                                <Row>
                                    <Col className="carousel-avatar" md={4} lg={4} sm={4} xs={4}>
                                        <img src={val.uri?val.uri:defaultImg} alt="profile-img" />
                                    </Col>
                                    <Col className="carousel-content" md={18} sm={18} xs={18} lg={18}>
                                        <img className="carousel-image" src={quoteStart} alt="quote Image"/>
                                        {renderHTML(val.comment_body)}
                                        <img className="carousel-image-opp" src={quoteEnd} alt="quote Image"/>
                                        <div className="content-heading-rating">
                                            <div  className="rating-wrapper">
                                                <div className="person-name">
                                                    <span>{val.field_customer_name} </span>
                                                </div>
                                                <div className="avg-rating">
                                                    Rated
                                                </div>
                                                <div className="rated"> {val.field_rating}</div>
                                            </div>
                                            <div className="designation">{val.field_designation}</div>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        );
                    })
                    :''}
                </Carousal>
                <div className="review-btn">
                        <Button buttonType="filled" label="Leave a review"/>
                </div>
            </DefaultStyle>
        );
    }
}
 
export default CarCustomersCarousel;