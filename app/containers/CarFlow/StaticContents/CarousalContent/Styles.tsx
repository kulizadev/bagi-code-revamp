import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';
const DefaultStyle = styled.div`
    .review-btn{
        text-align: center;
        padding: 30px 0;
    }
    .rated-head{
        background: rgb(105, 151, 220);
        border-radius: 10px;
        color: ${colors.buttonTextColor};
        padding: 3px 9px 3px 9px;;
        font-size: 18px;
    }
    .avg-rating-txt{
        padding: 3px 0px 0px 16px;
        font-size:14px;
    }
    .rated-text{
        font-size:12px;
    }
    .avg-rating-head{
        border-right: 1px solid;
        padding-right: 20px;
    }
    .heading{
        text-align: center;
        margin-top:60px;
    }
    .carousel-avatar{
        position: relative;
        top: 35px;
        z-index: 1;
        img{
            width: 11em;
            border-radius: 50%;
            position: absolute;
            left: 50%;
            top:50%;
            box-shadow: 1px 1px 20px 2px;
        }
    }
    .carousel-content{
        padding: 30px;
        border-radius: 20px;
        box-shadow: 0px 1px 12.8px 2.3px rgba(0, 0, 0, 0.08);
        margin-top: 10px;
        background: white;
        p{
            padding: 18px 18px 18px 90px;
            text-align: justify;
            font-size: ${fonts.fontSizeButton};
            color: ${colors.headingTextColor};
            margin-bottom: 0;
        }
        .content-heading-rating{
            display: table;
            padding-left: 90px;
        }
        .rating-wrapper{
            display: flex;
            margin-bottom: 5px;
            .person-name{
                margin-right: 12px;
                padding-top: 5px;
                color: ${colors.headingTextColor};
            }
            .avg-rating{
                padding: 5px 0 0 12px;
                border-left: 1px solid;
                color: ${colors.headingTextColor};
            }
            .rated{
                padding-top: 2px;
                background: rgb(105, 151, 220);
                border-radius: 10px;
                margin-left: 2px;
                height: 30px;
                width: 55px;
                color: ${colors.buttonTextColor};
                font-size: ${fonts.fontSizeHeadingThree};
                text-align: center;
            }
        }
        .designation {
            color: ${colors.headingTextColor};
        }
        .carousel-image{
            display: inline;
            padding-left: 70px;
        }
        .carousel-image-opp{
            float: right;
            margin-top: -21px 24px 0 0;
        }
`;
export default DefaultStyle;