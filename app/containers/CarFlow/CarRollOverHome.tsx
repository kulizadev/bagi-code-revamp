import React, { Component } from "react";
import BaseUrls from "constants/apiConstants";
import { request } from "configs/networklayer";
import WhatIsCarInsurance from "containers/CarFlow/StaticContents/WhatisCarInsurance";
import ComprehensiveCarInsurance from "containers/CarFlow/StaticContents/ComprehensiveCarInsu.tsx";
import ThirdPartyLiability from "containers/CarFlow/StaticContents/ThirdPartyLiability.tsx";
import PolicyCoverage from "containers/TravelFlow/StaticContents/Policy-coverage";
import AddOnsContentComponent from "containers/CarFlow/StaticContents/AddOnsContentComponent.tsx";
import KeyBenefitsofCarInsurance from "containers/CarFlow/StaticContents/KeyBenefitsofCarInsurance.tsx";
import FrequentlyAskedQues from "containers/TravelFlow/StaticContents/Frequenty-asked-question";
import Loader from "components/Loader";
import BlogContent from "containers/CarFlow/StaticContents/BlogSection";
import CarCustomersCarousel from 'containers/CarFlow/StaticContents/CarousalContent';
import CallMeBack from 'containers/Home/Call-me-back';
import dottie from "dottie";
import Form from 'antd/lib/form';
import CarHomePageForm from './CarHomePageForm';
const CallMeBackForm = Form.create({ name: 'callMeBack' })(CallMeBack);
export default class CarRollOverHome extends Component {
  public state = {
    isLoading: true,
    isLoadingFaq: true,
    isLoadingCrous: true,
    isLoadingblog: true
  };
  public carContent = undefined;
  public subCompoData = [];
  public componentDidMount() {
    const { DrupalUrl, carRegContentApi, DrupalCredential } = BaseUrls;
    const url = DrupalUrl + carRegContentApi; //Header API
    const authorization =
      "Basic " +
      btoa(DrupalCredential.username + ":" + DrupalCredential.password);
    request(url, authorization, "get", null).then(drupalResponse => {
      this.carContent = drupalResponse;
      console.log("what is car", this.carContent);
      this.setState({
        isLoading: false
      });
      dottie
        .get(drupalResponse, "node.field_additional_api_links", [])
        .forEach((val, ind) => {
          request(DrupalUrl + val.value, authorization, "get", null).then(
            res => {
              console.log("inner api data car", res);
              this.subCompoData[ind] = res;
              if (ind === 0) {
                this.setState({ isLoadingFaq: false });
              } else if (ind === 1) {
                this.setState({ isLoadingblog: false });
              } else if (ind === 2) {
                this.setState({ isLoadingCrous: false });
              }
            }
          );
        });
      return drupalResponse;
    });
  }
  public render() {
    return (
      <div>
        <CarHomePageForm />
        <WhatIsCarInsurance
          paragraph={dottie.get(
            this,
            "carContent.node.field_general_description.0.value",
            ""
          )}
          heading={dottie.get(this, 'carContent.node.title.0.value', '')}
        />

        <ComprehensiveCarInsurance
          carContentData={dottie.get(this, "carContent", "")}
        />
        <ThirdPartyLiability
          carContentData={dottie.get(this, "carContent", "")}
        />
        <PolicyCoverage
          contentArr={dottie.get(
            this.carContent,
            "paragraphs.field_section_paragraph_three",
            []
          )}
          sectitle={dottie.get(
            this.carContent,
            "node.field_section_title_three.0.value",
            ""
          )}
        />
        {this.carContent && (
          <AddOnsContentComponent
            carContent={dottie.get(
              this.carContent,
              "paragraphs.field_section_pararaph_four",
              []
            )}
            title={dottie.get(
              this.carContent,
              "node.field_section_title_four.0.value",
              ""
            )}
          />
        )}
        <KeyBenefitsofCarInsurance
          value={dottie.get(
            this,
            "carContent.node.field_paragraph_five_title.0.value",
            ""
          )}
          paragraph={dottie.get(
            this.carContent,
            "node.field_paragraph_five_description.0.value",
            ""
          )}
        />

        {this.state.isLoadingFaq ? (
          <Loader />
        ) : (
          <FrequentlyAskedQues
            contentArr={dottie.get(this, "subCompoData.0.rows", "")}
            heading={dottie.get(this.carContent, 'node.field_faq_title.0.value', '')}
          />
        )}

        {this.state.isLoadingblog ? (
          <Loader />
        ) : (
          <BlogContent contentArr={this.subCompoData[1]} id="why-us"
           blogHeading={dottie.get(this.carContent, 'node.field_blog_title.0.value', '')} />
        )}
         <CarCustomersCarousel
                data={this.subCompoData[2]}
                overallrating={ dottie.get(this.carContent, 'node.field_review_heading.1.value', '')}
                customerReviewSec={dottie.get(this.carContent, 'node.field_review_heading.0.value', '')} 
                averageRatingTxt={dottie.get(this.carContent, 'node.field_review_heading.2.value', '')}
                />
        <CallMeBackForm />

      </div>
    );
  }
}
