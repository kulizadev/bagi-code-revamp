import React, { Component } from 'react';
// import Constants from '../../../constants';
import Card from 'components/Card';
import { Row, Col, Form  } from 'antd';
import backgroundImage from 'images/sideImg.jpg';
// import RightSideImg from 'components/CardRightSideImage';
import carlogo from 'images/car_flow_images/car.png';
// import DivComponent from 'components/Div';
// import SpanComponent from 'components/Span';
import CouldNotFindCarFormComponent from './couldNotFindCarForm';
const CouldNotFindCarForm = Form.create({ name: 'couldNotFindCar' })(CouldNotFindCarFormComponent);

export default class CouldNotFindCardComponent extends Component {
    constructor(props) {
        super(props);
    }

    public render() {
        // const { weWereUnableToFindCar, soProvideUsWithBelowDetails} = Constants;
        return (
            <Row type="flex" justify="center">
            <Col lg={22} md={22}>
            <Card cardType="layout3" backgroundImg={backgroundImage} sideImg={carlogo}>
                {/* <RightSideImg imgSrc={carlogo} width="300px" bottom="10px" right="210px"/> */}
                <Row>
                    <Col md={14} xs={24} lg={16}>
                        {/* <DivComponent
                            value={weWereUnableToFindCar}
                            className="" />
                        <SpanComponent
                            textStyle="details-text"
                            text={soProvideUsWithBelowDetails}
                        /> */}
                        <CouldNotFindCarForm />
                    </Col>
                </Row>
            </Card>
            </Col>
            </Row>
        );
    }
}
