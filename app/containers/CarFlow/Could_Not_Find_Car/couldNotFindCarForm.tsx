
import React from 'react';
import {
	Form, Row, Col
} from 'antd';
import ButtonComponent from 'components/Button';
// import GlobalConstants from '../../../../constants/globalConstants';
// import apiConstants from 'constants/apiConstants';
// import Constants from '../../constants';
import SelectComponent from 'components/Select';
// import SpanComponent from 'components/Span';
import DatepickerComponent from 'components/Datepicker';
// import history from 'utils/history';
import moment from 'moment';
// import RoutingPathConstants from 'constants/routingPathConstants';

function hasErrors(fieldsError) {
	return Object.keys(fieldsError).some(field => fieldsError[field]);
}

export default class CouldNotFindCarFormComponent extends React.Component<any, any> {

	constructor(props) {
		super(props);
		this.state = {
		};

	}

	public componentDidMount() {

	}

	public getTodaysDate(date) {
		const currentDate = new Date(date);
		const day = currentDate.getDate();
		const month = currentDate.getMonth() + 1;
		const year = currentDate.getFullYear();
		return `${year}-${month}-${day}`;
	}

	public disabledDate(current) {
		// tslint:disable-next-line:max-line-length
		return (
			current > moment().subtract(10, 'months') ||
			current > moment().endOf('day') ||
			current < moment().subtract(15, 'years')
		);
	}

	public render() {

		const {
			getFieldDecorator,
			getFieldsError,
			isFieldTouched,
			getFieldError,
		} = this.props.form;


		return (
			<Row type="flex" justify="center">
				<Form
					layout="horizontal"
					onSubmit={() => { }}
					hideRequiredMark={true}
				>
					<Row>
						<Col md={10} xs={24} lg={14}>
							<Form.Item
								className="custom-form-item-label"
								label={'City Of Registration'}
							//   validateStatus={carCityRegError ? 'error' : ''}
							//   help={carCityRegError || ''}
							>
								{getFieldDecorator('cityOfRegistration', {
									rules: [
										{ required: true, message: 'Required' },
									],
								})(
									<SelectComponent
										placeholder={'City of Registration'}
										mode='multiple'
										showArrow={true}
										showSearch={true}
										options={[]}
									/>,
								)}
							</Form.Item>
						</Col>
						<Col md={6} xs={24} lg={10}>
							<Form.Item
								className="custom-form-item-label"
								label={'Registration Month and Year'}
							//   validateStatus={carRegMonthAndYearError ? 'error' : ''}
							//   help={carRegMonthAndYearError || ''}
							>
								{getFieldDecorator('regMonthAndYear', {
									rules: [
										{ required: true, message: 'Please select the field!' },
									],
								})(
									<DatepickerComponent
										input_type="type1"
										placeholder={'Reg Month and Year'}
										format={'MMM-YYYY'}
										pickerType='date'
										disableType='past'
									/>,
								)}
							</Form.Item>
						</Col>
						<Col md={10} xs={24} lg={16}>
							<Form.Item
								className="custom-form-item-label"
								label={'Car Model'}
							//   validateStatus={carCityRegError ? 'error' : ''}
							//   help={carCityRegError || ''}
							>
								{getFieldDecorator('carModel', {
									rules: [
										{ required: true, message: 'Required' },
									],
								})(
									<SelectComponent
										placeholder={'Car Model'}
										mode='multiple'
										showArrow={true}
										showSearch={true}
										options={[]}
									/>,
								)}
							</Form.Item>
						</Col>
					</Row>

					<Row>
						{/* <SpanComponent
                        textStyle="info-text"
                        text={'This will help us get you the best quote'}
                    /> */}
					</Row>
					<Row>
						<Col md={8} xs={24} lg={6}>
							<Form.Item>
								<ButtonComponent
									htmlType="submit"
									label={'Continue'}
									disabled={hasErrors(getFieldsError())}
									buttonType="filled"
								/>
							</Form.Item>
						</Col>
					</Row>
				</Form>
			</Row>
		);
	}
}
