import React, { Component } from "react";
import Col from "antd/lib/col";
import Row from "antd/lib/row";
import DefaultStyle from "./Styles";
import Card from "components/Card";
import Form from "antd/lib/form";
import Heading from "components/Heading";
import background from "images/sideImg.jpg";
import Planelogo from "images/car_flow_images/car.png";
import InputComponent from 'components/Input';
import polygonImage from 'images/car_flow_images/polygon.png';
import LinkComponent from 'components/Link';
import Constants from 'containers/CarFlow/Constants';
import Button from 'components/Button';
import Checkbox from 'components/Checkbox';

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
  }
class CarHomePageForm extends Component<any, any> {
  public componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
    }
  public handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
          }
        });
      };
  public onlyNumber(e) {
        let value = e.target.value;
        value = value.replace(/\D/g, '');
        return value;
    }
  public render() {
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

    const carRegNumberError = isFieldTouched('carRegisteration') && getFieldError('carRegisteration');
    const mobileNumberError = isFieldTouched('mobileNumber') && getFieldError('mobileNumber');
    const emailAddressError = isFieldTouched('emailAddress') && getFieldError('emailAddress');

    const { emailAddressPlaceholder, emailAddress, mobileNumberPlaceholder, mobileNumber, carRegPlaceholder, carRegErrorMessage, mobileNumberErrorMessage, emailAddressErrorMessage, carRegisterationNumber, dontKnowYourCarNumber, thisWillBePrimaryModeContact, iWishToGetPolicyDetailsOnWhatsapp, byClickingOnGetQuoteIagree,iAlreadyHaveBhartiAxaPolicy, smartDecisionNowFilltheseDetails} = Constants;

    return (
      <DefaultStyle>
        <Row type="flex" justify="center">
          <Col lg={22} md={22}>
            <Card
              cardType="layout3"
              backgroundImg={background}
              sideImg={Planelogo}
              sideImgTop="13rem"
            >
              <Row>
                <Col lg={24} md={24} xs={24} sm={24} className="content">
                  <Row>
                    <Col lg={24} md={24} xs={24} sm={24}>
                      <Heading
                        headingType="headingThree"
                        label={smartDecisionNowFilltheseDetails}
                        className="heading"
                      />
                    </Col>
                  </Row>
                  <Row>
                        <Col>
                           <LinkComponent
                                textStyle="link-text"
                                text={iAlreadyHaveBhartiAxaPolicy}
                                path="#"
                            />
                        </Col>
                    </Row>
                  <Form hideRequiredMark={true}>
                    <Row>
                      <Col lg={5} md={5} xs={24} sm={24} className="car-registration">
                        <Form.Item validateStatus={carRegNumberError ? 'error' : ''}
                         help={carRegNumberError || ''} label={carRegisterationNumber}>
                            {getFieldDecorator('carRegisteration', {
                                rules: 
                                [{ required: true, message:carRegErrorMessage,
                                   pattern: /^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$/
                                }],
                            })(
                                <InputComponent inputType="type1" placeholder={carRegPlaceholder} placeholderPosition="center"
                                prefix={<img src={polygonImage}/>}
                                suffix={<img src={polygonImage}/>}
                                maxLength={13}
                                />,
                            )}
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row>
                        <Col lg={10} md={10} xs={24} sm={24}>
                           <LinkComponent
                                textStyle="link-text"
                                text={dontKnowYourCarNumber}
                                path="#"
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={5} md={5} xs={24} sm={24}>
                            <Form.Item validateStatus={mobileNumberError ? 'error' : ''} 
                            help={mobileNumberError || ''} label={mobileNumber}>
                            {getFieldDecorator('mobileNumber', {
                                    rules: [{ required: true, message:mobileNumberErrorMessage, 
                                      pattern: /^[6789]\d{9}$/,
                                    }],
                                    getValueFromEvent: this.onlyNumber,
                                })(
                                    <InputComponent
                                    inputType="type1"
                                    placeholder={mobileNumberPlaceholder}
                                    maxLength={10}
                                    />,
                                )}
                            </Form.Item>
                        </Col>
                        <Col lg={5} md={5} xs={24} sm={24} className="emailfield">
                            <Form.Item validateStatus={emailAddressError ? 'error' : ''} 
                            help={emailAddressError || ''} label={emailAddress}>
                            {getFieldDecorator('emailAddress', {
                                    rules: [{ required: true, message:emailAddressErrorMessage,
                                      pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                     }],
                                })(
                                    <InputComponent
                                    inputType="type1"
                                    placeholder={emailAddressPlaceholder}
                                    maxLength={50} minLength={5}
                                    />,
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                      <Col lg={10} md={10} xs={24} sm={24}>
                        <span className="primarymode"> {thisWillBePrimaryModeContact}</span>
                      </Col>
                    </Row>
                    <Row>
                      <Col  lg={12} md={12} xs={24} sm={24} className="wishtoget">
                         <Checkbox>
                           {iWishToGetPolicyDetailsOnWhatsapp}
                         </Checkbox>
                      </Col>
                    </Row>
                    <Row>
                        <Col lg={10} md={10} xs={24} sm={24}>
                          <Form.Item>
                            <Button buttonType="filled" label="Get Quote" 
                             htmlType="submit" disabled={hasErrors(getFieldsError())}/>
                          </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                      <Col lg={11} md={11} xs={24} sm={24} className="getquote">
                        <span className="bycliking"> {byClickingOnGetQuoteIagree}</span>
                      </Col>
                    </Row>
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </DefaultStyle>
    );
  }
}
const CarHome = Form.create({ name: 'TravelFlow' })(
    CarHomePageForm,
);
export default CarHome;