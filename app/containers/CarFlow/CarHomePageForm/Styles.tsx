import styled from 'styled-components';
import vars from 'configs/styleVars';
const DefaultStyle = styled.div`
  .content{
    padding: 41px 0px 0px 64px;
  }
  .ant-form-item{
    margin-bottom: 0px;
  }
  .emailfield{
      margin-left:9px
  }
  .primarymode, .bycliking{
    font-size:12px;
    color: ${vars.colors.blackThrityColor}
  }
  .wishtoget{
    margin: 20px 0px 30px 0px;
  }
  .car-registration{
    margin-top: 15px;
  }
  .getquote{
    margin-bottom: 10px;
    line-height: normal;
  }
`;
export default DefaultStyle;
