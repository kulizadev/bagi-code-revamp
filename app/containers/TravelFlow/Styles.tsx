import styled from 'styled-components';
import { globalColors } from 'configs/styleVars';
export const DefaultStyle = styled.div`{
    margin: 4rem 0;
    form{
        margin: 3.9rem 5.9rem;
        .date-separator{
            display: block;
            position: relative;
            margin: auto;
            top: 4.9rem;
            right: 0.5rem;
        }
        .custom-label{
            line-height: 3.99999rem;
            color: ${globalColors.labelColor};
            font-size: 1.3rem;
            font-weight: bold;
        }
        .img-icon{
            width: 1.4rem;
            margin-right: 1rem;
        }
    }
    .ant-form-item{
        margin-right: 0.9rem;
    }
    .center-loader{
        display: block;
        font-size: 40px;
        color: #01529c;
   }
}`;