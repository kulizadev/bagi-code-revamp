/*
 *
 * TravelFlow constants
 *
 */
import ResponseData from './CountryList';
enum ActionTypes {
  DEFAULT_ACTION = "app/TravelFlow/DEFAULT_ACTION"
}
export function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}
export const travellersConstant = [
  'Self',
  'Spouse',
  'Child1',
  'Child2',
  // 'Child3',
  'Mother',
  'Father',
  'Sister',
  'Brother',
  'Mother-in-law',
  'Father-in-law',
]
export const initialTravelerAge = [
  {
      type: 'Self',
      age: '',
    },
    {
      type: 'Spouse',
      age: '',
    },
    {
      type: 'Child1',
      age: '',
    },
]
let children = new Array();
ResponseData.map(val => {
    children.push({value: val.display, label: val.display, id: val.display});
})
export const errrMsg ={
  DestinationErr: 'Please select one or more destinations',
  TravelMoreThan18: 'traveller age should be greater or equal to 18',
  ValidAge: 'please enter valid age',
  ChildAge: "Child's age can't be more than 23 years",

}
export const labelMsg = {
  DestinationLabel: 'Where are you travelling?',
  TravellersLabel: 'Travellers',
  AddTraveller: 'Add Travellers',
  LandingDisclaimer: 'By clicking Continue, I declare that all the traveller(s) will be travelling from India',
  EmigrantVisaMsg: 'Do any of the traveller(s) have an immigrant visa? ',
  TripStartDate: 'Trip Start Date',
  TripEndDate: 'Trip End  Date',
  WhatsAppTxt: ' I wish to get my policy details on my whatsapp number ',
  SpamTxt: 'This will be our primary mode of Contact we promise not to spam you'

}
export const TooltipTxt = {
  DestinationMsg: 'Please select your destination country or region. You can select more than one.',
  TravellerMsg: 'Infants 3 months and older require to be entered. Please enter their age as 1 Year'
}
export const ButtonTxt = {
  Continue: 'Continue',
  GetQuote: 'Get Quote'
}
export {children};
export default ActionTypes;
