import ActionTypes from 'constants/actionTypes';

export const initialState = {
    travelContent : '',
    travelLandingPageInfo: '',
    travelPlanInfo: '',
    travelProposalInfo: '',
};

function TravelFlowReducer(state = initialState, action) {
    switch (action.type) {
    case ActionTypes.SET_TRAVEL_REG_CONTENT:
        return {
            ...state,
            travelContent :  action.payload.data,
        };
    case ActionTypes.SET_TRAVEL_FAQ_CONTENT:
        return {
            ...state,
            travelFaqContent :  action.payload.data,
        };
    case ActionTypes.SET_TRAVEL_BLOG_CONTENT:
        return {
            ...state,
            travelBlogContent :  action.payload.data,
        };
    case ActionTypes.SET_TRAVEL_COMMENT_LIST_CONTENT:
        return {
            ...state,
            travelCommentListContent :  action.payload.data,
        };
    case ActionTypes.SET_LANDING_PAGE_INFORMATION:
        return {
            ...state,
            travelLandingPageInfo: action.payload.data,
        };
    case ActionTypes.SET_TRAVEL_QUOTE_PAGE_INFORMATION:
        return {
            ...state,
            travelLandingPageInfo: action.payload.data,
        };
    case ActionTypes.GET_TRAVEL_QUOTE_PAGE_INFORMATION:
        return {
            ...state,
            travelLandingPageInfo: action.payload.data,
        };
    case ActionTypes.SET_TRAVEL_PLAN_INFORMATION:
        return {
            ...state,
            travelPlanInfo: action.payload.data,
        };
    case ActionTypes.GET_TRAVEL_PLAN_INFORMATION:
        return {
            ...state,
            travelPlanInfo: action.payload.data,
        };
    case ActionTypes.SET_TRAVEL_PROPOSAL_INFORMATION:
        return {
            ...state,
            travelProposalInfo: action.payload.data,
        };
    case ActionTypes.GET_TRAVEL_PROPOSAL_INFORMATION:
        return {
            ...state,
            travelProposalInfo: action.payload.data,
        };
    default:
        return state;
    }
}

export default TravelFlowReducer;
