import React, { Component } from 'react';
import { request } from 'configs/networklayer';
import BaseUrls from 'constants/apiConstants';
import WhatIsTravelinsurance from 'containers/TravelFlow/StaticContents/What-is-travel-insurance';
import Card from 'components/Card';
import DatepickerComponent from 'components/Datepicker';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import Form from 'antd/lib/form';
import Icon from 'antd/lib/icon';
import background from 'images/card-bg.png';
import Planelogo from 'images/plane-11.jpg';
import SelectComponent from 'components/Select';
import { PlaceholderTxt } from 'constants/globalConstants';
import infoImage from 'images/global_images/info.jpg';
import InputFloatComponent from 'components/FloatingInput';
import ButtonComponent from 'components/Button';
import ToggleComponent from 'components/Toggle';
import { DefaultStyle } from './Styles';
import DateSep from 'images/trip-start-and-end.png';
import depFlgt from 'images/departure.png';
import arrFlgt from 'images/arrival.png';
import _get from 'lodash/get';
import TravInsuByDestination from './StaticContents/Travel-insurance-by-destination';
import BenefitsOfTravelInsurance from './StaticContents/Benefits-of-schangan-travel.tsx';
import FrequentlyAskedQues from './StaticContents/Frequenty-asked-question';
import KeyAdvAndFeatureOfTravel from './StaticContents/Key-advantage-and-feature-of-travel';
import PolicyCoverage from './StaticContents/Policy-coverage';
import CallMeBack from 'containers/Home/Call-me-back';
import Loader from 'components/Loader';
const CallMeBackForm = Form.create({ name: 'callMeBack' })(
    CallMeBack,
);
import { hasErrors, travellersConstant, children, initialTravelerAge, errrMsg, labelMsg, ButtonTxt } from './constants';
class TravelHomeForm extends Component<any, any>{
    constructor(props) {
        super(props);
        this.state = {
            selectedTraveler: initialTravelerAge,
            travelName: travellersConstant,
            loading: true,
            isLoading: true,
            isLoadingFaq: true,
            isLoadingCrous: true,
            isLoadingblog: true,
        }
    }
    public contentData;
    public subCompoData;
    private handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };
    public componentDidMount() {
        this.subCompoData = new Array();
        const { DrupalUrl, TravelContentDataUrl, DrupalCredential } = BaseUrls;
        const url = DrupalUrl + TravelContentDataUrl;    //Header API
        const authorization = 'Basic ' + btoa(DrupalCredential.username + ':' + DrupalCredential.password);
        request(url, authorization, 'get', null)
            .then(drupalResponse => {
                this.contentData = drupalResponse;
                console.log("what is travel", this.contentData)
                this.setState({
                    isLoading: false,
                })
                _get(drupalResponse, 'node.field_additional_api_links', []).forEach((val, ind) => {
                    request(DrupalUrl + val.value, authorization, 'get', null)
                        .then(res => {
                            console.log("inner api data travel", res)
                            this.subCompoData[ind] = res;
                            if (ind === 0) {
                                this.setState({ isLoadingFaq: false });
                            } else if (ind === 1) {
                                this.setState({ isLoadingblog: false });
                            } else if (ind === 2) {
                                this.setState({ isLoadingCrous: false });
                            }
                        });
                });
                return drupalResponse;
            });
    }
    public render() {
        const {
            getFieldDecorator,
            getFieldsError,
        } = this.props.form;
        const { DestinationOfTraveller, DateField, AgeTraveller, DateFormat, Yes, No } = PlaceholderTxt;
        const labelWithIcon = (txt, imgIcon) => <span>{txt} <img src={imgIcon} className="img-icon" /></span>;
        const label = (txt) => <span>{txt}</span>;

        return (
            <DefaultStyle>
                <Row type="flex" justify="center">
                    <Col lg={22} md={22}>
                        <Card cardType="layout3" backgroundImg={background} sideImg={Planelogo}>
                            <Row>
                                <Form onSubmit={this.handleSubmit} hideRequiredMark={true}>
                                    <Row>
                                        <Col md={7} xs={24} lg={7} sm={24}>
                                            <Form.Item label={labelWithIcon(labelMsg.DestinationLabel, infoImage)}>
                                                {getFieldDecorator('multiSelect', {
                                                    rules: [
                                                        {
                                                            required: true,
                                                            message: errrMsg.DestinationErr,
                                                            type: 'array',
                                                        },
                                                    ],
                                                })(
                                                    <SelectComponent placeholder={DestinationOfTraveller} showArrow={true} showSearch={true} options={children} mode='multiple' />)}
                                            </Form.Item>
                                        </Col>
                                        <Col md={12} xs={24} lg={12} sm={24}>
                                            <Col md={24} lg={24} sm={24} xs={24} className="custom-label">
                                                {labelWithIcon(labelMsg.TravellersLabel, infoImage)}
                                            </Col>
                                            {this.state.selectedTraveler.map((val, ind) => {
                                                return (
                                                    <Col md={5} xs={8} lg={5} key={ind}>
                                                        {ind !== 0 && ind !== 1 && ind !== 2 && (
                                                            <Icon
                                                                type="close"
                                                            // tslint:disable-next-line: jsx-no-lambda
                                                            //   onClick={() => this.clearInput(val.type)}
                                                            />
                                                        )}
                                                        <Form.Item>
                                                            {getFieldDecorator(val.type, {
                                                                //   rules: [{ validator: this.validateAge }],
                                                                //   getValueFromEvent: this.onlyNumber,
                                                                validateTrigger: "onBlur",
                                                            })(
                                                                <InputFloatComponent
                                                                    inputType='type1'
                                                                    placeholder={val.type}
                                                                    onFocusPlaceholder={AgeTraveller}
                                                                    minLength={1}
                                                                    maxLength={2}
                                                                />,
                                                            )}
                                                        </Form.Item>
                                                    </Col>
                                                );
                                            })}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={5} xs={24} lg={5} sm={24} >
                                            <Form.Item label={label(labelMsg.TripStartDate)}>
                                                {getFieldDecorator('tripStartDate', {
                                                    //   rules: [{ validator: this.validateAge }],
                                                    rules: [{
                                                        required: true,
                                                    }],
                                                    //   getValueFromEvent: this.onlyNumber,
                                                    validateTrigger: 'onBlur',
                                                })(
                                                    <DatepickerComponent input_type="type1" placeholder={DateField} format={DateFormat} pickerType='date' disableType='past' prefixImg={depFlgt} />,
                                                )}
                                            </Form.Item>
                                        </Col>
                                        <Col md={2} xs={24} lg={2} sm={24} >
                                            <img src={DateSep} className='date-separator' />
                                        </Col>
                                        <Col md={5} xs={24} lg={5} sm={24} >
                                            <Form.Item label={label(labelMsg.TripEndDate)}>
                                                {getFieldDecorator('tripEndDate', {
                                                    //   rules: [{ validator: this.validateAge }],
                                                    rules: [{
                                                        required: true,
                                                    }],
                                                    //   getValueFromEvent: this.onlyNumber,
                                                    validateTrigger: 'onBlur',
                                                })(
                                                    <DatepickerComponent input_type="type1" placeholder={DateField} format={DateFormat} pickerType='date' disableType='past' prefixImg={arrFlgt} />,
                                                )}
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={24} lg={24} sm={24} xs={24} className='custom-label'>
                                            {labelWithIcon(labelMsg.EmigrantVisaMsg, infoImage)}
                                            <ToggleComponent rightTitle={No} leftTitle={Yes} id1='emig1' id2='emig2' name='emigrant' onClick={() => console.log('kk')} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Form.Item>
                                            <ButtonComponent
                                                htmlType="submit"
                                                label={ButtonTxt.Continue}
                                                disabled={hasErrors(getFieldsError()) || this.state.btndisabled}
                                                buttonType="filled"
                                            />
                                        </Form.Item>
                                    </Row>
                                    <Row>
                                        {label(labelMsg.LandingDisclaimer)}
                                    </Row>
                                </Form>
                            </Row>
                        </Card>
                    </Col>
                </Row>


                {this.state.isLoading ? (
                    <Loader />
                ) : (
                        _get(this.contentData, 'node.body.length') > 0 && (
                            <WhatIsTravelinsurance
                                paragraph={
                                    this.contentData.node.body.length > 0
                                        ? this.contentData.node.body[0].value
                                        : ''
                                }
                                titleHeading={
                                    this.contentData.node.title.length > 0
                                        ? this.contentData.node.title[0].value
                                        : ''
                                }
                            />
                        )
                    )}

                <Row id="reasonsToBuyId">
                    {this.state.isLoading ? (
                        <Loader />
                    ) : (
                            _get(this.contentData, 'node.field_paragraph_one_title.length') > 0 && (
                                <TravInsuByDestination
                                    contentArr={
                                        this.contentData.paragraphs.field_paragraph_one_data
                                    }
                                    heading={
                                        this.contentData.node.field_general_description.length > 0
                                            ? this.contentData.node.field_general_description[0].value
                                            : ''
                                    }
                                    sectitle={
                                        this.contentData.node.field_paragraph_one_title.length > 0
                                            ? this.contentData.node.field_paragraph_one_title[0].value
                                            : ''
                                    }
                                />
                            )
                        )}

                    {this.state.isLoading ? (
                         <Loader />
                    ) : (
                            _get(this.contentData, 'node.field_paragraph_two_title.length') > 0 && (
                                <TravInsuByDestination
                                    contentArr={this.contentData.paragraphs.field_paragraph_two_data}
                                    heading={''}
                                    sectitle={
                                        this.contentData.node.field_paragraph_two_title.length > 0
                                            ? this.contentData.node.field_paragraph_two_title[0].value
                                            : ''
                                    }
                                />
                            )
                        )}

                    <Row id="whyUsId">
                        {this.state.isLoading ? (
                             <Loader />
                        ) : (
                                this.contentData.node.field_paragraph_three_title.length > 0 && (
                                    <KeyAdvAndFeatureOfTravel
                                        contentArr={
                                            this.contentData.paragraphs.field_paragraph_three_data
                                        }
                                        heading={''}
                                        sectitle={
                                            this.contentData.node.field_paragraph_three_title.length > 0
                                                ? this.contentData.node.field_paragraph_three_title[0].value
                                                : ''
                                        }
                                    />
                                )
                            )}
                    </Row>
                    {this.state.isLoading ? (
                       <Loader />
                    ) : (
                            this.contentData.node.field_paragraph_four_title.length > 0 && (
                                <BenefitsOfTravelInsurance
                                    contentArr={this.contentData.paragraphs.field_paragraph_four_data}
                                    heading={''}
                                    sectitle={
                                        this.contentData.node.field_paragraph_four_title.length > 0
                                            ? this.contentData.node.field_paragraph_four_title[0].value
                                            : ''
                                    }
                                />
                            )
                        )}

                    {this.state.isLoading ? (
                        <Loader />
                    ) : (
                            this.contentData.node.field_paragraph_five_title.length > 0 && (
                                <BenefitsOfTravelInsurance
                                    contentArr={this.contentData.paragraphs.field_paragraph_five_data}
                                    heading={''}
                                    sectitle={
                                        this.contentData.node.field_paragraph_five_title.length > 0
                                            ? this.contentData.node.field_paragraph_five_title[0].value
                                            : ''
                                    }
                                />
                            )
                        )}
                    {this.state.isLoading ? (
                        <Loader />
                    ) : (
                            this.contentData.paragraphs.field_paragraph_six_data && (
                                <PolicyCoverage
                                    contentArr={this.contentData.paragraphs.field_paragraph_six_data}
                                    sectitle={
                                        this.contentData.node.field_paragraph_six_title.length > 0
                                            ? this.contentData.node.field_paragraph_six_title[0].value
                                            : ''
                                    }
                                />
                            )
                        )}

                    {this.state.isLoadingFaq ? (
                        <Loader />
                    ) : (
                            <FrequentlyAskedQues contentArr={this.subCompoData[0].rows}
                            heading={this.contentData.node.field_faq_title_travel.length > 0 ? this.contentData.node.field_faq_title_travel[0].value:''} />
                        )}

                </Row>
            </DefaultStyle>
        )
    }
}
const TravelHome = Form.create({ name: 'TravelFlow' })(
    TravelHomeForm,
);
export default TravelHome;
