/*
 *
 * TravelFlow
 *
 */

import React, {Component} from "react";
import { connect } from "react-redux";
// import { FormattedMessage } from "react-intl";
import { compose, Dispatch } from "redux";
// import { useInjectSaga } from "utils/injectSaga";
// import { useInjectReducer } from "utils/injectReducer";
import { createStructuredSelector } from "reselect";
// import { RootState } from "./types";
// import saga from "./saga";
// import messages from "./messages";
import history from 'utils/history';
import {TravelRouting} from 'constants/routingPathConstants';
import { Switch, Route, Router } from 'react-router-dom';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { setTravelLandingInfoAction, setTravelProposalAction, setTravelPlanAction } from './actions';
import { getTravelInfo, getTravelPlanInfo  } from './selectors';
import {request} from 'configs/networklayer';
import BaseUrls from 'constants/apiConstants';
import Spin from 'antd/lib/spin';
// interface OwnProps {}

// interface StateProps {}

// interface DispatchProps {
//   dispatch: Dispatch;
// }

// type Props = StateProps & DispatchProps & OwnProps;
// fetchTravelDetai(props);
export class TravelFlow extends Component<any, any> {
  public state = {
    loading: true,
  }
  private fetchTravelDetai = () => {
    const postData1 =  null;
    const url1 = BaseUrls.DrupalUrl+ BaseUrls.NavbarHeaderUrl;
    const authorization1 = 'Basic ' + btoa(BaseUrls.DrupalCredential.username + ':' + BaseUrls.DrupalCredential.password);
    const sendingMethod1 = 'get'
    request(url1, authorization1, sendingMethod1, postData1).then(val => {
          this.props.setTravelData(val);
          console.log('response from header', val);
          this.setState({loading: false});
      });
  }
  componentDidMount(){
    this.fetchTravelDetai();

    // const constants = {
    //   'INIT': 'initaliser',
    //   'SUCCESS': 'successor',
    //   'ERROR': 'error',
    //   'URL': BaseUrls.DrupalUrl+ BaseUrls.NavbarHeaderUrl
    // }
    // new Promise((resolve,reject) => 
    //   this.props.dispatch({type:'GET',payload: {constants, resolve, reject, sendingMethod: 'GET'}})
    // ).then(response=>{
      
    // }).catch(err=>{
    // })
  }
  render(){
  // useInjectReducer({ key: "travelFlow", reducer: TravelFlowReducer });
  // useInjectSaga({ key: "travelFlow", saga: saga });
  const {match} = this.props;
  console.log('this.props', this.props);
  return (
    <Spin tip="Loading..." spinning={this.state.loading}>
      <div>
            <Router history={history}>
                  <React.Fragment>
                  <Switch>
                      {TravelRouting.map((val, ind) => {
                              return(
                                  <Route exact path={match.path + val.path} render={() => <val.component  {...this.props}/>} key={ind}/>
                              )
                          })
                      }
                      <Route component={NotFoundPage} />
                  </Switch>
                  </React.Fragment>
          </Router>
          </div>
    </Spin>
  );
}
}

// Map RootState to your StateProps
const mapStateToProps = createStructuredSelector({
  travelInfo: getTravelInfo(),
  travelPlanInfo: getTravelPlanInfo(),
});

// Map Disptach to your DispatchProps
function mapDispatchToProps(
  dispatch: Dispatch,
  // ownProps: OwnProps
){
  return {
    setTravelData: data => dispatch(setTravelLandingInfoAction(data)),
    setTravelProposalData: data => dispatch(setTravelProposalAction(data)),
    setTravelPlanData: data => dispatch(setTravelPlanAction(data)),
    dispatch
  };
}
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(withConnect)(TravelFlow);
