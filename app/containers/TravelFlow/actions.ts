/*
 *
 * Travel actions
 *
 */

import { action } from 'typesafe-actions';
import ActionTypes from 'constants/actionTypes';


export const setTravelLandingInfoAction = (data) => {
    return action(ActionTypes.SET_LANDING_PAGE_INFORMATION, {data: data});
};
export const getTravelLandingInfoAction = () => {
    return action(ActionTypes.GET_LANDING_PAGE_INFORMATION);
};
export const setTravelQuotePageAction = (data) => {
    return action(ActionTypes.SET_TRAVEL_QUOTE_PAGE_INFORMATION, {data: data});
};
export const getTravelQuotePageAction = () => {
    return action(ActionTypes.GET_TRAVEL_QUOTE_PAGE_INFORMATION);
};
export const getTravelPlanAction = () => {
    return action(ActionTypes.GET_TRAVEL_PLAN_INFORMATION);
};
export const setTravelPlanAction = (data) => {
    return action(ActionTypes.SET_TRAVEL_PLAN_INFORMATION, {data: data});
};
export const getTravelProposalAction = () => {
    return action(ActionTypes.GET_TRAVEL_PROPOSAL_INFORMATION);
};
export const setTravelProposalAction = (data) => {
    return action(ActionTypes.SET_TRAVEL_PROPOSAL_INFORMATION, {data: data});
};


