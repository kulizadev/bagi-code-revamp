import React, { Component } from 'react';
import { request } from 'configs/networklayer';
import BaseUrls from 'constants/apiConstants';
import Card from 'components/Card';
import Icon from 'antd/lib/icon';
import PlanCompareContent from 'containers/TravelFlow/StaticContents/Premium-static-content';
export default class TravelPremium extends Component<any, any> {
    public contentData;
    public featureData;
    public coverageData;
    public state = {
        isLoading: true,
    };

    public componentDidMount() {
        this.coverageData = [];
        this.featureData = [];
        const { DrupalUrl, ProductFeature, DrupalCredential } = BaseUrls;
        let url = DrupalUrl + ProductFeature + '/sch/?_format=json';    //Header API
        const authorization = 'Basic ' + btoa(DrupalCredential.username + ':' + DrupalCredential.password);
        request(url, authorization, 'get', null).then(data => {
            this.contentData = data.rows;
            console.log("plans comparision", this.contentData)
            Object.keys(this.contentData).forEach(key => {
                this.contentData[key].forEach(val => {
                    if (val.field_category.toLowerCase() === 'coverage') {
                        this.coverageData.push({ body: val.body, title: val.nothing });
                    } else {
                        this.featureData.push({ body: val.body, title: val.nothing });
                    }
                });
            });
            this.setState({ isLoading: false });
            return data;
        });
    }
    public render() {
        console.log('Premium Details');
        return (
            <React.Fragment>
                <h1>permium details</h1>
                {this.state.isLoading ? (
                    <Card cardType="layout6">
                        <Icon type="loading" className="center-loader" />
                    </Card>
                ) : (
                        <PlanCompareContent
                            contentArr={this.contentData}
                            coverageData={this.coverageData}
                            featureData={this.featureData}
                        />
                    )}
            </React.Fragment>
        );
    }
}