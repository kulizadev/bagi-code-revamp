/*
 * TravelFlow Messages
 *
 * This contains all the text for the TravelFlow container.
 */

import { defineMessages } from "react-intl";

export const scope = "app.containers.TravelFlow";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the TravelFlow container!"
  }
});
