import { ActionType } from "typesafe-actions";
import * as actions from "./actions";
import { ApplicationRootState } from "types";

/* --- STATE --- */
interface TravelFlowState {
  readonly travelContent: [];
  readonly travelLandingPageInfo: [];
  readonly travelPlanInfo: {};
  readonly travelProposalInfo: {};
}

/* --- ACTIONS --- */
type TravelFlowActions = ActionType<typeof actions>;

/* --- EXPORTS --- */

type RootState = ApplicationRootState;
type ContainerState = TravelFlowState;
type ContainerActions = TravelFlowActions;

export { RootState, ContainerState, ContainerActions };
