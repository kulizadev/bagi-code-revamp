import { createSelector } from 'reselect';
import { ApplicationRootState } from 'types';

const getStateValue = (state: ApplicationRootState) => {
    return state.travelFlow;
};
// tslint:disable-next-line:max-line-length
const getTravelInfo = () => createSelector(getStateValue, stateValue  => {
    return stateValue.travelLandingPageInfo; },
);
const getTravelPlanInfo = () => createSelector(getStateValue, stateValue  => {
    return stateValue.travelPlanInfo; },
);
const getTravelProposalInfo = () => createSelector(getStateValue, stateValue  => {
    return stateValue.travelProposalInfo; },
);
export { getStateValue, getTravelInfo, getTravelPlanInfo, getTravelProposalInfo };
