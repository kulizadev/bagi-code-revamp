import styled from 'styled-components';
import leftTickImage from 'images/policy_cov_check.png';
import rightCrossImage from 'images/policy_cov_cross.png';
import HealthArrow from 'images/HealthAssets/HealthArrow.png'

interface PolicyStyleProps {
    backgroundImage?: string;
}
const PolicyCoverageWrapper = styled.div`
    & .policyBackgroundImage {
        background-image: url(${(props: PolicyStyleProps) => props.backgroundImage});
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        height: 136px;
        width: 140px;
        position: relative;
    }
    & ul {
        list-style-image: url('${leftTickImage}');
    }
    &.cross-img>div>div>ul {
        list-style-image: url('${rightCrossImage}');
    }
    &.ArrowIconHealth>div>div>ul {
        list-style-image: url('${HealthArrow}');
    }
    & ul>li {
        font-size:16px;
        font-weight:600;
    }
    & ul>li>ul>li {
        font-size:14px;
        font-weight:normal;
        margin-bottom: 20px;
        list-style: none;
    }

    & ul>li>ul {
        list-style-image: none;
        padding: 0px;
    }

    & .policy-sub-header {
        font-size:18px;
    }
    .covered{
        position: absolute;
        top: 26px;
        height: 80px;
        width: 90px;
        left: 25px;
        background-size: contain;
    }

    @media (max-width: 760px) {

    & .policy-content-style {
        border-bottom: 1px solid #aeaeae;
        padding:20px;
    }
`;

export default PolicyCoverageWrapper;
