import styled from 'styled-components';
import Col from 'antd/lib/col';
interface CustomCardStyleProps {
    backgroundImage: string;
    imagePosition: string;
}
export const CustomCardStyleCoverageTravel = styled.div`
    width: 90%;
    background: white;
    margin: auto;
    margin-bottom: 20px;
    clear:both;
    position: relative;
    top: 20px;
    border-radius: 62px;
    height: auto;
    background-image: url(${(props: CustomCardStyleProps) => props.backgroundImage});
    background-size: 50% 100%;
    background-repeat: no-repeat;
    background-position: ${(props: CustomCardStyleProps) => props.imagePosition};
    padding: 20px;

    &.mob-view{
        display: none;
    }
    &.web-view{
        display: block;
    }
    &.content-heading{
        display: table;
        margin: auto;
        text-align: center;
        padding-top: 30px;
        margin-bottom:17px;
    }
    @media (max-width: 760px) {data-styled-version
        background: white;
        width: 100%;
        padding: 20px;
        border-radius:0px;
        padding-bottom: 50px;
        margin-bottom: 0px;
        border-bottom: 1px solid gray;
        &.mob-view{
            display: block;
            margin-bottom: 20px;
        }
        &.web-view{
            display: none;
        }
    }

`;
export const HeaderStyle = styled(Col)`
    .content-heading{
        display: table;
        margin: auto;
        text-align: center;
        padding-top: 30px;
    }

`;