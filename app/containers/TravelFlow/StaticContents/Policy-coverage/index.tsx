import React from 'react';
import PolicyCoverageContentComponent from './PolicyCoverageContent';
import backgroundImage from 'images/global_images/ellipse-grey1.png';
import Row from 'antd/lib/row';
import { CustomCardStyleCoverageTravel, HeaderStyle } from './Styles';
import Heading from 'components/Heading';
import ellipseImage from 'images/global_images/ellipse-grey.png';
const PolicyCoverage = props => {
  console.log('car policy props', props);
  return (
    <Row>
      <HeaderStyle md={22} xs={24} lg={22} sm={24}>

          <Heading headingType="headingTwo"
                        label={props.sectitle ? props.sectitle : ''}
                        className="content-heading"
                    />
      </HeaderStyle>
      <CustomCardStyleCoverageTravel
        backgroundImage={backgroundImage}
        imagePosition="left"
        className="web-view"
      >
        <Row>
          {Array.isArray(props.contentArr) && props.contentArr.map((val, ind) => {
            return (
              <PolicyCoverageContentComponent
                imageSrc={
                  val.field_paragraph_image.length > 0
                    ? val.field_paragraph_image[0].url
                    : ' '
                }
                subHeader={
                  val.field_subtitle.length > 0
                    ? val.field_subtitle[0].value
                    : ''
                }
                listContent={
                  val.field_paragraph_bullets.length > 0
                    ? val.field_paragraph_bullets[0].processed
                    : ''
                }
                backgroundImage={ellipseImage}
                className={ind === 1 ? 'cross-img' : ''}
                key={ind}
              />
            );
          })}
        </Row>
      </CustomCardStyleCoverageTravel>
    </Row>
  );
};
export default PolicyCoverage;
