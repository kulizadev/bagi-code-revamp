import React from 'react';
import PolicyCoverageWrapper from './PolicyCoverageWrapper';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import renderHTML from 'react-render-html';
interface PolicyCoverageContentProps {
    subHeader: string;
    imageSrc: string;
    listContent: object;
    backgroundImage?: string;
    className?: string;
}
const PolicyCoverageContentComponent = (props: PolicyCoverageContentProps) => {
    const {imageSrc, subHeader, listContent, backgroundImage, className} = props;
    return (
        <PolicyCoverageWrapper backgroundImage={backgroundImage} className={className}> 
            <Col md={12} lg={12}>
                <Row>
                    <Col xs={{span: 6, offset: 7}}
                        md={{span: 12, offset: 0}}
                        lg={{span: 22, offset: 0}}
                        className="policyBackgroundImage">
                       <img src={imageSrc} alt="side img" className="covered"/>
                    </Col>
                </Row>
                <Row>
                    <Col
                        md={{span: 16, offset: 0}}
                        lg={{span: 20, offset: 0}}
                        xs="24"
                        className="policy-sub-header"
                    >
                        <h3>{subHeader}</h3>
                    </Col>
                </Row>
                <Row>
                {renderHTML(listContent)} 
                </Row>
            </Col>
        </PolicyCoverageWrapper>
    );
};
export default PolicyCoverageContentComponent;


