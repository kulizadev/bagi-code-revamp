import styled from 'styled-components';
export const DefaultStyle = styled.div`{
   .content-heading{
      display: table;
      margin-bottom: 38px;
   }
   .content-Topheading{
      text-align: center;
      margin: 60px 0px 17px 0px;
      p:first-child {
        font-size:18px;
        margin: 0px;
        color: black;
      }
      P{
        font-size:14px;
        margin-bottom: 0px;
        color:#666666
      }
   }
   .Trvl-insu{
     width:90%;
     margin:auto;
     background:white;
     border-radius:3rem;
     padding: 38px 44px 78px 47px
   }
   .content-mobile-header{
     font-size:18px;
   }
}`;

export const RoundIconStyle = styled.div`
  & .sehengen-travel {
    position: absolute;
    top: 15px;
    left: 15px;
    width: 50px;
    height: 50px;
  }
`;
interface RoundedIconProps {
    backgroundImg: any;
  }
export const RoundedIcon = styled.div`
  background-image: url(${(props: RoundedIconProps) => props.backgroundImg});
  height: 82px;
  position: relative
  background-size: contain;
  `;
