import React from 'react';
import roundedIconimg from 'images/ellipse-593.jpg';
import renderHTML from 'react-render-html';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import {DefaultStyle, RoundIconStyle, RoundedIcon } from './Styles';
import Heading from 'components/Heading';
const TravInsuByDestination = (props) => {
    return (
      <DefaultStyle>
        <Row type="flex" justify="center">
          <Col md={22} xs={24}  lg={22} sm={24}>
            <div className="content-Topheading">
              {renderHTML(props.heading)}
            </div>
          </Col> 
        </Row>
      <div className="Trvl-insu">
          <Heading headingType="headingTwo" 
                  label={props.sectitle}
                  className="content-heading"
          />
        <Row>
          {props.contentArr.map((val, index) => {
              return(
                <Col md={11} lg={11} xs={24} key={index} className="content-sec-padd">
                    <Col md={5} xs={24} lg={5}>
                    <RoundedIcon backgroundImg={roundedIconimg}>
                      <RoundIconStyle>
                      <img
                      src={val.field_paragraph_image.length > 0 ? val.field_paragraph_image[0].url : '#'}
                      alt="img" className="sehengen-travel" />
                      </RoundIconStyle>
                    </RoundedIcon>
                  </Col>
                  <Col md={19} xs={24} lg={19}>
                    <h3  className="content-mobile-header">{val.field_paragraph_heading[0].value}</h3>
                      {renderHTML(val.field_paragraph_body[0].value)}
                  </Col>
              </Col>
              );
          })}
          </Row>
      </div>
      </DefaultStyle>
    );
};

export default TravInsuByDestination;
