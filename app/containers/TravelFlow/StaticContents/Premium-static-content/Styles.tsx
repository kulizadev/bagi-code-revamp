import styled from 'styles/styled-components';

export const DefaultStyle = styled.div`
    .benTrvlTblSty{
        overflow-x: scroll;
    }
    .benTrvlTblSty::-webkit-scrollbar {
        width: 0px;
        height: 5px;
    }
    .benTrvlTblSty::-webkit-scrollbar-thumb {
    background-color: darkgrey;
    outline: 1px solid slategrey;
    }
    .benTrvlTblSty table{
        min-width: 100% ;
        border:0px solid transparent ;
    }
    .benTrvlTblSty th, td {
        padding: 15px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }
    .benTrvlTblSty  tr td:not(:first-child) {
        text-align: center;
    }
    .benTrvlTblSty tr:last-child td{
        border-bottom: none;
    }
    .content-heading{
        text-align: center;
        margin-top: 29px;
    }
    .rounded-tab .ant-tabs-bar.ant-tabs-top-bar{
        border: none;
        display: table;
        position: absolute;
        right: 213px;
        z-index: 11;
        margin: 0px;
        top: 40px;
    }
    .rounded-tab-policy-mob .ant-tabs-bar.ant-tabs-top-bar{
        border: none;
        display: table;
        position: relative;
        margin: 0px;
        margin-bottom: 10px;
    }
    .rounded-tab .ant-tabs-nav.ant-tabs-nav-animated, .rounded-tab-policy-mob .ant-tabs-nav.ant-tabs-nav-animated{
        background: #e7eaf2;
        border-radius: 25px;
        margin: 0px;
    }
    .rounded-tab .ant-tabs-nav .ant-tabs-tab, .rounded-tab-policy-mob .ant-tabs-nav .ant-tabs-tab{
        margin: 0px;
        padding: 5px 10px;
        font-size: 0.90em;
        min-height: 30px;
        min-width: 5.5em;
    }
    .rounded-tab .ant-tabs-ink-bar.ant-tabs-ink-bar-animated, .rounded-tab-policy-mob .ant-tabs-ink-bar.ant-tabs-ink-bar-animated{
        max-width: 0px;
    }
    .rounded-tab .ant-tabs-nav .ant-tabs-tab-active, .rounded-tab-policy-mob .ant-tabs-nav .ant-tabs-tab-active{
        background: #667bb0;
        color: white;
        margin: 0px;
        border-radius: 25px;
    }
    .rounded-tab{
        padding: 26px 172px 30px 172px;;
    }

    @media (max-width: 760px) {
        .rounded-tab{
            padding:0px;
        }
        .rounded-tab .ant-tabs-bar.ant-tabs-top-bar{
            right: 43px;
            top: 15px;
        }
    }
 
`;
