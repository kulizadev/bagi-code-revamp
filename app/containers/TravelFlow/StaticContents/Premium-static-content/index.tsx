import React from 'react';
import Tabs from 'antd/lib/tabs';
import { DefaultStyle } from './Styles';
import renderHTML from 'react-render-html';
import Card from 'components/Card';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Heading from 'components/Heading';
import AccordionComponent from 'components/Accordion';
const TabPane = Tabs.TabPane;
const PlanCompareContent = props => {
    console.log("coverage data", props.coverageData)
    // const genExtra = bluetick1x => <img className="tickImage" src={bluetick1x} />;
    return (
        <DefaultStyle>
            <Row type="flex" justify="center" className="benTrvlTblSty">
                <Col md={22} xs={24} lg={22} sm={24}>
                    <Card cardType="layout1">
                        <Row type="flex" justify="center">
                            <Col md={22} xs={24} lg={22} sm={24}>
                                <Heading headingType="headingTwo"
                                    label="Let us help to choose the best plan by comparing them"
                                    className="content-heading"
                                />
                            </Col>
                        </Row>
                        <Tabs defaultActiveKey="1" className="rounded-tab">
                            <TabPane tab="Features" key="1">
                                <AccordionComponent contents={props.featureData.map((val, ind) => {
                                    return ({
                                        header: val.title,
                                        body: renderHTML(val.body)
                                    })
                                })}
                                    accordianType="type1" expandIconPosition="right" showArrow={true} />
                            </TabPane>
                            <TabPane tab="Coverage" key="2">
                                <AccordionComponent contents={props.coverageData.map((val, ind) => {
                                    return ({
                                        header: val.title,
                                        body: renderHTML(val.body)
                                    })
                                })}
                                    accordianType="type1" expandIconPosition="right" showArrow={true} />
                            </TabPane>
                        </Tabs>
                    </Card>
                </Col>
            </Row>

        </DefaultStyle>
    );
};
export default PlanCompareContent;
