import React from 'react';
import renderHTML from 'react-render-html';
import AccordionComponent from 'components/Accordion';
export default function FaqContent(props) {
    return (
        <div className="faq-content">
            <AccordionComponent contents={props.content.map((val, ind) => {
                return ({
                    header: val.title,
                    body: renderHTML(val.body)
                })
            })}
                accordianType="type1" expandIconPosition="right" showArrow={true} />
        </div>
    );
}
