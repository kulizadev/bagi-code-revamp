import styled from 'styled-components';
export const DefaultStyle = styled.div`
    width:90%;
    margin:auto;
  & .ant-tabs-nav {
    display: flex;
    justify-content: space-around;
  }
  & .ant-tabs-bar {
    margin-top: 10px;
    margin-bottom: 0px;
    border: none;
  }
  & .ant-tabs.ant-tabs-card .ant-tabs-card-bar .ant-tabs-tab {
    border-radius: 10px;
    margin-right: 15px;
    color: #01529c;
  }
  & .ant-tabs.ant-tabs-card .ant-tabs-card-bar .ant-tabs-tab-active {
    color: #f8fafb;
    background: #a7a3cf;
  }
  .faq-content .ant-collapse {
    padding: 31px 106px 29px 108px;
    border-radius: 30px;
  }
  .view-faq-button{
    text-align: center;
    margin-top: 20px;
    margin-bottom: 50px;
  }
  .content-heading{
    text-align: center;
    margin-top: 59px;
  }
  @media (max-width: 760px){
    .ant-tabs-nav.ant-tabs-nav-animated>div {
      width: 100%;
    }
    & .ant-tabs-bar {
      margin-left: 45px;
    }
  }
`;