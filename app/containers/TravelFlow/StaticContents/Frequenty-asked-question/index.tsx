import React from 'react';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import FaqContent from './FaqContent';
import GlobalConstants from 'constants/globalConstants';
import { Link } from 'react-router-dom';
import { DefaultStyle } from './Styles';
import Tabs from 'components/Tabpane';
import Heading from 'components/Heading';
import renderHTML from 'react-render-html';
const FrequentlyAskedQues = props => {
  const { seeMoreFaqs } = GlobalConstants;
  return (
    <DefaultStyle>
      <Row type="flex" justify="center">
        <Col>
            <Heading headingType="headingThree" 
                label={renderHTML(props.heading && props.heading)}
                className="content-heading"
            />
        </Col>
      </Row>
      <Tabs tabs={Object.keys(props.contentArr).map((val, ind) => {
        return (
          {
            heading: val,
            key: (ind + 1).toString(),
            children: <FaqContent content={props.contentArr[val]} />
          }
        );
      })} tabType="type2" />
      <Row type="flex" justify="center">
        <Col lg={22} md={22} className="view-faq-button">
          <Link
            to="#"
          >
            {seeMoreFaqs}
          </Link>
        </Col>
      </Row>
    </DefaultStyle>
  );
};
export default FrequentlyAskedQues;
