import styled from 'styled-components';
export const DefaultStyle = styled.div`{
    .content-heading{
        display: table;
        margin: auto;
        text-align: center;
        padding-top: 30px;
        margin-bottom:17px;
      }
      .content-padding{
        padding: 70px 0px 69px 45px;
      }
      &{
        img.key-adv-icon{
            position :relative;
            top: 4px;
        }
        .para{
            font-size: 14px;
        }
    }
}`;
