import React from 'react';
import logo from 'images/left_side_arrow.png';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import { DefaultStyle } from './Styles';
import Heading from 'components/Heading';
import Card from 'components/Card';
export default function KeyAdvAndFeatureOfTravel(props) {
    return (
        <DefaultStyle>
            <Row type="flex" justify="center">
                <Col md={22} xs={24} lg={22} sm={24}>
                    <Heading headingType="headingTwo"
                        label={props.sectitle}
                        className="content-heading"
                    />
                </Col>
            </Row>
            <Row type="flex" justify="center">
                <Col md={22} xs={24} lg={22} sm={24}>
                    <Card cardType="layout1">
                        <Row style={{padding: '70px 0px 69px 45px'}} className="content-padding">
                            {props.contentArr.map((val, ind) => {
                                return (
                                    <Col md={12} lg={12} sm={24} xs={24} key={ind}>
                                        <Col md={1} xs={2} lg={1} sm={2}>
                                            <img src={logo} className="key-adv-icon" />
                                        </Col>
                                        <Col md={20} xs={20} lg={20} sm={20}>
                                            <Heading headingType="headingThree"
                                                label={val.field_paragraph_heading.length > 0 ? val.field_paragraph_heading[0].value : ''}
                                            />
                                            <p className="para">
                                                {val.field_paragraph_body.length > 0 ? val.field_paragraph_body[0].value : ''}
                                            </p>
                                        </Col>
                                    </Col>
                                );
                            })}
                        </Row>
                    </Card>
                </Col>
            </Row>

        </DefaultStyle>
    );
}
