import styled from 'styled-components';
const DefaultStyle = styled.div`{
    .content-heading {
        text-align: center;
    }
    .para-and-header{
        padding: 38px 57px 46px 47px;
        p:first-child {
            font-size:18px;
          }
          P{
            font-size:14px;
          }
    }
}`;

export default DefaultStyle;
