import React, { Component } from 'react'
import DefaultStyle from './Styles';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import Card from 'components/Card';
import renderHTML from 'react-render-html';
import Heading from 'components/Heading';
export default class WhatIsTravelinsurance extends Component<any, any> {
   public render() {
        return (
         <DefaultStyle>
             <Row type="flex" justify="center">
                <Col md={22} xs={24}  lg={22} sm={24}>
                <Heading headingType="headingTwo" 
                        label={this.props.titleHeading}
                        className="content-heading"
                    />
                </Col>
                <Col md={22} xs={24}  lg={22} sm={24}>
                    <Card cardType="layout1">
                        <div className="para-and-header">
                            {renderHTML(this.props.paragraph)}
                        </div>
                    </Card>
                </Col>
             </Row>
        </DefaultStyle>
        )
    }
}
