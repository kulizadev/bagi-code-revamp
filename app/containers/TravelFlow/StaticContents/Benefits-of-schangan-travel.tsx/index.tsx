import React from 'react';
import Row from 'antd/lib/row';
// import bluetick1x from 'images/medical-benefits.jpg';
// import planeIcon from 'images/plane-icon.png';
// import thumbIcon from 'images/thumb-icon.png';
import renderHTML from 'react-render-html';
import Heading from 'components/Heading';
import {DefaultStyle} from './Styles';
import AccordionComponent from 'components/Accordion';

const BenefitsOfTravelInsurance = props => {
  // const genExtra = bluetick1x => <img className="tickImage" src={bluetick1x} />;
  return (
      <DefaultStyle>
    <Row className="benTrvlTblSty">
      <Heading headingType="headingThree" 
                        label={props.sectitle}
                        className="content-heading"
                    />
      <div className="content-padding">
        <AccordionComponent contents={props.contentArr.map((val, ind)=>{
            return({
                header: val.field_benefits_heading[0].value,
                    body: renderHTML(val.field_benefits_body[0].value)
                   });
              })}
        accordianType="type1" expandIconPosition="right" showArrow={true}/>
      </div>
       </Row>
    </DefaultStyle>
  );
};

export default BenefitsOfTravelInsurance;
