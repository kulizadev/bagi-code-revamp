import styled from 'styled-components';
export const DefaultStyle = styled.div`{
    width:90%;
    margin:auto;
   .content-heading{
      display: table;
      margin: auto;
      text-align: center;
      padding-top: 30px;
      margin-bottom:17px;
    }
    .content-padding{
        height: auto;
        background: #fff;
        border-radius: 3rem;
        margin: auto;
        background-size: cover;
        padding: 53px 62px 52px 62px;
    }
    .benTrvlTblSty{
        overflow-x: scroll;
    }
    .benTrvlTblSty::-webkit-scrollbar {
        width: 0px;
        height: 5px;
    }
    .benTrvlTblSty::-webkit-scrollbar-thumb {
      background-color: darkgrey;
      outline: 1px solid slategrey;
    }
    .benTrvlTblSty table{
        min-width: 100% ;
        border:0px solid transparent ;
    }
    .benTrvlTblSty th, td {
        padding: 15px;
        text-align: left;
        border-bottom: 1px solid #ddd;
      }
      .benTrvlTblSty  tr td:not(:first-child) {
         text-align: center;
      }
      .benTrvlTblSty tr:last-child td{
          border-bottom: none;
      }
}`;

