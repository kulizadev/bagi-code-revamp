/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import * as React from 'react';
import { Switch, Route } from 'react-router-dom';

// import Test from 'containers/Test';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Card from 'components/Card';
import Landing from 'containers/Landing';
import Basecomp from 'containers/Basecomp';
import GlobalStyle from 'global-styles';
import TravelFlow from 'containers/TravelFlow';
import {CarRolloverFlow} from 'containers/CarFlow';
import RoutingPathConstants from 'constants/routingPathConstants'
import Home from 'containers/Home';
export default function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Home} />
        {/* <Route exact path="/" component={Test} /> */}
        <Route exact path="/card" component={Card} />
        <Route exact path="/landing" component={Landing} />
        <Route exact path="/basecomp" component={Basecomp} />
        <Route path={RoutingPathConstants.travelInsuranceHome} component={TravelFlow} />
        <Route path={RoutingPathConstants.carRolloverHome} component={CarRolloverFlow} />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </div>
  );
}
