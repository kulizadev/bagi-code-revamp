// File to test base components
import React, { Component } from 'react';
import backgroundImg from 'images/card-bg.png';
import placeholderPre from 'images/polygon-1.png';
import ellipsec from 'images/ellipsec-725.png'

import Card from 'components/Card';
import Button from 'components/Button';
// import PopoverIcon from 'components/PopoverIcon';
import popoverImg from 'images/popoverIcon.png';
import Popover from 'components/Popover';
import InputComponent from 'components/Input';
import Icon from 'antd/lib/icon';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import Heading from 'components/Heading';
import Checkbox from 'components/Checkbox';
import Carousal from 'components/Carousal';
import BreadcrumbComponent from 'components/Breadcrumb';
import Tabs from 'components/Tabpane';
import AccordionComponent from 'components/Accordion';
import ToggleComponent from 'components/Toggle';
import Slider from 'components/Slider';


// const text = <span>Title</span>;
const content = (
  <div>
    <p>Content ContentContent Content ContentContent Contentvv ContentContent Content
         Content ContentContentContentContent Content ContentContentContent ContentContentContent</p>
  </div>
);

export default class Landing extends Component {
    state = {
        showAddOnsPage: true,
        value: 2.5, // default rating value
      };
    public call = () => {
     console.log('callsd')
    }
    public handleChange = value => {
        this.setState({
            value: value
        });
        console.log(value);
    }
    public toggleContent = () => {
        this.setState({
            showAddOnsPage: !this.state.showAddOnsPage,
        } , ()=> console.log("toggle changed", this.state.showAddOnsPage))
    }
    public genExtra = () => (
        <Icon
        type="setting"
        />
      );
    public render() {
        const breadCrumbValues = [{
            href: '#',
            title: 'home',
        },
        {
            href: '#',
            title: 'motor',
        },
        {
            href: '#',
            title: 'carInsurance',
        }];

        const tabValues = [{
            heading : 'All',
            key: '1',
            children: 'this is children this is children this is children',
        },
        {
            heading : 'this is heading 2',
            key: '2',
            children: 'this is children 2',
        },
        {
            heading : 'this is heading 3',
            key: '3',
            children: 'this is children 3',
        },

      ]
        const AccordianValues = [{
            header : 'All',
            body: <h1>hello</h1>,
            },
            {
                header : 'this is heading 2',
                body: 'this is children 2',
            },
            {
                header : 'this is heading 3',
                body: 'this is children 3',
            },

        ];
        const marks = {
            100: '10%',
            500:'50%',
            1000: '100%',
        };
        return ( // make every props as small caps
            <div>
                 <Row>
                    <Col lg={24}>
                    <Card cardType="layout3" backgroundImg={backgroundImg}>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                    <h1>test</h1>
                </Card>
                    </Col>
                    </Row>
                <Button label="Call me back" buttonType="filled" />
                <Row>
                    <Col lg={6}>
                    </Col>
                    <Col lg={6}>
                        <Popover placement="top" content={content} trigger="hover">
                        <img src={popoverImg} alt=""/>
                        </Popover>
                    </Col>
                </Row>
                   
                    <Row>
                        <Col lg={6}>
        <InputComponent inputType="type1" placeholder="Full Name" prefix={<img src={placeholderPre}/>}
        suffix={<img src={placeholderPre}/>} placeholderPosition="center" onChange={this.call}/>
                        </Col>
                    </Row>
                    <Popover placement="top" content={content} trigger="hover">
                    <img src={popoverImg} alt=""/>
                    </Popover>
                    <Heading label={"welcome"} headingType={'headingOne'} />
                    <Checkbox onChange={this.call}>
                        Cheakbox
                    </Checkbox>
                        <Carousal showArrow={true}>
                            <div>
                                <Row>
                                    <Col  lg={16} style={{background:'red'}}>
                                    <Card card_type="layout5" backgroundImg={backgroundImg}>
                                            <h1 style={{textAlign:'center'}}>test</h1>
                                    </Card>
                                    </Col>
                                </Row>
                            </div>
                            <div>
                               <Row>
                                   <Col lg={20} style={{background:'green'}}>
                                       <Card card_type="layout5" backgroundImg={backgroundImg}>
                                           <h1>test</h1>
                                       </Card>
                                   </Col>
                               </Row>
                            </div>
                            <div>
                            <Row>
                                   <Col lg={22} style={{background:'blue'}}>
                                       <Card card_type="layout5" backgroundImg={backgroundImg}>
                                           <h1>test</h1>
                                       </Card>
                                   </Col>
                               </Row>
                            </div>
                            <div>
                            <Row>
                                   <Col lg={24} style={{background:'orange'}}>
                                       <Card card_type="layout5" backgroundImg={backgroundImg}>
                                           <h1>test</h1>
                                       </Card>
                                   </Col>
                               </Row>
                            </div>
                    </Carousal>
                    <BreadcrumbComponent breadCrumbValues={breadCrumbValues} separator="|"/>
                    <Row>
                        <Col lg={6}>
                        </Col>
                        <Col lg={10}>
                           <Tabs tabs={tabValues} tabType="type3"/>
                        </Col>
                    </Row>

                    <Row style={{background:'white'}}>
                        <Col lg={6}>
                        </Col>
                        <Col lg={5}>
                           <AccordionComponent contents={AccordianValues} 
                           accordianType="type2" expandIconPosition="right" showArrow={true} extra={this.genExtra()}/>
                        </Col>
                    </Row>
                    <ToggleComponent
                        onClick={this.toggleContent}
                        rightTitle="Add ons"
                        leftTitle="others"
                        id1="policy-web-toggle-on"
                        id2="policy-web-toggle-off"
                        className="comprehensive-thirdparty-switch"
                        name="policyWebToggle"
                        defaultChecked={this.state.showAddOnsPage}/>

                    <Row style={{background: 'white'}}>
                        <Col lg={6}>
                        </Col>
                        <Col lg={5}>
                           <Slider sliderType="type2" min={100}  max={1000} marks={marks} tooltipVisible={false}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={20}>
                        <Card cardType="layout3" backgroundImg={backgroundImg} sideImg={ellipsec} sideImgRight="9rem"
                        sideImgTop="9rem">
                            <Row>
                                <Col lg={16}>
                                  <h1>tes</h1>
                                </Col>
                            </Row>
                        </Card>
                        </Col>
                    </Row>
            </div>
        )
    }
}
