import React from 'react';
import DefaultStyle from './Styles';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import GlobalConstants from 'constants/globalConstants';
import images from './image-array';
import irdai from 'images/Homepage-assets/footer/irdai.png';
import ResponseData from 'constants/FooterInfo';

export default class Footer extends React.Component{
    render(){
        if(!ResponseData) return null;
        const { irdaiReg, paymentGateway, disclaimer, connect } = GlobalConstants;
        const { payment, socialMedia } = images;
        return (
            <DefaultStyle>
                {/* IRDAI Section */}
                <Row>
                    <Col offset={3} lg={8} md={8}>
                        <img src={irdai} alt="" className="irdai-img"/>
                        <span className="irdai-text common-text">
                            {irdaiReg}
                        </span>
                    </Col>
                    <Col lg={11} md={11} className="payment-gateway-wrapper">
                        <span className="common-text">
                            {paymentGateway}
                        </span>
                        {payment.map((content,key) => {
                            return <img src={content} alt="" key={key} className="ml18"/>
                        })}
                    </Col>
                </Row>
                <Row type="flex" justify="center">
                    <Col lg={19} md={19}>
                        <p className="disclaimer common-text">
                            {disclaimer}
                        </p>
                    </Col>
                </Row>
                {/* Blue Page Footer Section */}
                <Row className="footer-wrapper">
                    {ResponseData.map((content,key) => {
                        if (key == 0) {
                            return (
                                <React.Fragment key={key}>
                                    <h3 className="heading">{content.title}</h3>
                                    <div className="inline-options-wrapper">
                                        {content.below.map((content,key)=>{
                                            return (
                                                <a href={content.uri} className="inline-options" key={key}>{content.title}</a>
                                            );
                                        })}
                                    </div>
                                </React.Fragment>
                            )
                        }else{
                            return (
                                <React.Fragment key={key}>
                                    <Col md={8} lg={8} className="sections">
                                        <h3 className="heading">{content.title}</h3>
                                        <div className="">
                                            {content.below.map((content,key)=>{
                                                return (
                                                    <a href={content.uri} className="options" key={key}>{content.title}</a>
                                                );
                                            })}
                                        </div>
                                    </Col>
                                </React.Fragment>
                            )
                        }
                    })}
                </Row>
                <div className="connect-wrapper">
                    <p>{connect}</p>
                    <div>
                    {socialMedia.map((val,key)=> {
                        return (
                            <a href={val.url} className="imgs" target="_blank" key={key}>
                                <img src={val.image} alt="social-media-icon" />
                            </a>
                        )
                    })}
                    </div>
                </div>
            </DefaultStyle>
        );
    }
}