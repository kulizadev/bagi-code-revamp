import dmca from 'images/Homepage-assets/footer/dmca.png';
import rapid from 'images/Homepage-assets/footer/rapid.png';
import visa from 'images/Homepage-assets/footer/visa.png';
import masterCard from 'images/Homepage-assets/footer/master-card.png';
import amex from 'images/Homepage-assets/footer/amex.png';
import netBanking from 'images/Homepage-assets/footer/net-banking.png';
import twitter from 'images/Homepage-assets/footer/twitter.png';
import fb from 'images/Homepage-assets/footer/fb.png';
import linkedIn from 'images/Homepage-assets/footer/linkedIn.png';
// import googlePlus from 'images/Homepage-assets/footer/googlePlus.png';
import youtube from 'images/Homepage-assets/footer/youtube.png';

const payment = [
    dmca, rapid, visa, masterCard, amex, netBanking
];
const socialMedia = [
    {
        'image': twitter,
        'url': 'https://twitter.com/BhartiAXAGI'
    },
    {
        'image': fb,
        'url': 'https://www.facebook.com/BhartiAXAGeneralInsuranceCompany'
    },
    {
        'image': linkedIn,
        'url': 'https://www.linkedin.com/company-beta/289098/'
    },
    // {
    //     'image': googlePlus,
    //     'url': '#'
    // },
    {
        'image': youtube,
        'url': 'https://www.youtube.com/user/BhartiAXAGI'
    },
];
const images = {
    payment, socialMedia
}
export default images;
