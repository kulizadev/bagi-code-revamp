import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    padding: 20px 0 0;
    background-color: ${colors.irdaiFooterBG};
    .common-text{
        font-size: ${fonts.fontSizeFooterText};
        color: ${colors.headingTextColor};
    }
    .irdai-text{
        padding-left: 20px;
    }
    .payment-gateway-wrapper{
        padding-top: 10px;
    }
    .ml18{
        margin-left: 18px;
    }
    .disclaimer{
        text-align: center;
        margin: 30px 0 21px;
        line-height: 1.64;   
    }
    .footer-wrapper{
        padding: 21px 0 41px;
        background-color: ${colors.footerLightBG};
        text-align: center;
        .heading{
            font-size: ${fonts.fontSizeText};
            font-weight: ${fonts.fontWeightHeadingOne};
            color: ${colors.buttonTextColor};
            padding-right: 45px;
        }
        .inline-options-wrapper{
            width: 90%;
            margin: 21px auto 32px;
            border-top: 1px solid #fff;
            border-bottom: 1px solid #fff;
            padding: 15px 0;
            .inline-options:last-child{
                border-right: none;
            }
            .inline-options{
                color: ${colors.buttonTextColor};
                font-size: ${fonts.fontSizeOverlineLink};
                padding: 0 15px;
                border-right: 1px solid #fff;
            }
        }
        .options{
            display: inline-block;
            text-align: left;
            color: ${colors.buttonTextColor};
            font-size: ${fonts.fontSizeOverlineLink};
            float: right;
            width: 63%;
        }
        .sections:last-child{
            margin: 18px 0;
            .heading{
                padding-right: 75px;
            }
        }
    }
    .connect-wrapper{
        text-align: center;
        background-color: ${colors.enabledButtonBG};
        color: ${colors.buttonTextColor};
        font-size: ${fonts.fontSizeText};
        padding: 18px 0;
        .imgs{
            margin-right: 30px;
            cursor: pointer;
        }
        .imgs:last-child{
            margin-right: 0;
        }
    }
}`

export default DefaultStyle;
