import styled from 'styled-components';
import { colors, fonts, sizes } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    .heading{
        margin: 40px 0 0;
        text-align: center;
        padding: 20px 0;
    }
    .tab-data-wrapper{
        padding: 0 80px;
    }
    .ant-tabs-nav-scroll{
        margin: 0 auto;
        text-align: center;
    }
    .tab-data-wrapper{
        .bigBlogImg{
            width: 95%;
            height: 401px;
            border-radius: 14px;
        }
        .guide-text{
            position: relative;
            bottom: 67px;
            padding-left: 14px;
            font-size: ${fonts.fontSizeOverlineLink};
            color: ${colors.buttonTextColor};
            background: rgba(1, 1, 1, 0.34);
            width: 95%;
            border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
            .guide-to-buy{
                color: ${colors.buttonTextColor};
                margin-bottom: ${sizes.marginS};
                font-size: ${fonts.fontSizeHeadingThree};
            }
            .travel-and-date{
                display: flex;
            }
        }
        .smallBlogWrapper{
            font-size: ${fonts.fontSizeOverlineLink};
            .travelBlogHeader{
                font-size: ${fonts.fontSizeButton};
            }
        }
        .travelBlog{
            background:#f28a8f;
            margin: 0px;
            padding: 0px ${sizes.paddingS};
            display: table;
            color: ${colors.buttonTextColor};
            margin-right: ${sizes.marginS};
        }
        .smallBlogImg{
            width: 200px;
            height: 120px;
            padding-right: ${sizes.paddingS};
            border-radius: 14px;
            margin-bottom: 15px;
        }
    }
    .see-all{
        text-align: center;
        padding-bottom: 40px;
        font-size: ${fonts.fontSizeOverlineLink};
    }
}`

export default DefaultStyle;
