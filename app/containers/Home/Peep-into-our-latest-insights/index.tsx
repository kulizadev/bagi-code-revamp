import React from 'react';
import Card from 'components/Card';
import Heading from 'components/Heading';
import DefaultStyle from './Styles';
import Row  from 'antd/lib/row';
import Col  from 'antd/lib/col';
import dottie from 'dottie';
// import renderHtml from 'react-render-html';
import Tabs from 'components/Tabpane';
//Imported network layer
import { request } from 'configs/networklayer';
import BaseUrls from 'constants/apiConstants';

interface StateConfig {
    internalApiStateData: any
}
export default class Peep extends React.Component<any,StateConfig>{
    public finalTabsData = new Array();
    componentWillReceiveProps(nextProps){
        const paragraphs = dottie.get(nextProps.productData, 'paragraphs');
        const { DrupalUrl, DrupalCredential } = BaseUrls;
        const internalApiData = new Array();
        const tabHeadingData = new Array();
        paragraphs.field_latest_insights_link ?
        // Will have to do map later as the drupal response is not generic
        // response keys are different as well
        paragraphs.field_latest_insights_link.map((content,key) => {
            // mapping the internal apis to fetch the data
            const url = DrupalUrl + content.field_link[0].value;
            tabHeadingData.push(content.field_url_title[0].value);
            const authorization = 'Basic ' + btoa(DrupalCredential.username + ':' + DrupalCredential.password);
            // Videos & infographics on hold for now so added blog check only
            // might have to do 3 api calls or handle using promise in future
            if(url.includes('blog-list')){
                request(url, authorization, 'get', null).then(drupalResponse => {
                    internalApiData.push(drupalResponse);
                    this.setState({
                        internalApiStateData: internalApiData
                    })
                });
            }
        })
        : null;
        // Adding heading and key - final data for tab-pane base component
        tabHeadingData.map((content,index) => {
            this.finalTabsData[index] = {};
            this.finalTabsData[index]['heading'] = content;
            this.finalTabsData[index]['key'] = String(index);
        });
    }
    render(){
        if(!this.state) return null;
        const node = dottie.get(this.props.productData, 'node');
        const { internalApiStateData } = this.state;
        // Adding children as react node - final data for tab-pane base component
        internalApiStateData ?
        internalApiStateData.map((content,key) => {
            this.finalTabsData[key]['children'] =
                <div key={key+1}>
                    <Heading headingType="headingThree"
                        label={
                            node.field_latest_insights_heading ?
                            node.field_latest_insights_heading[key+1].value
                        : ''}
                    />
                    <Row>
                        <Col md={15} lg={15}>
                        {content  && content
                            .filter(val => val.sticky === 'False')
                            .map((val, ind) => {
                                return (
                                    <div  key={ind}>
                                        <img className="bigBlogImg" src={val.uri} alt="bigBlogImg" />
                                        <div className="guide-text">
                                            <h3 className="guide-to-buy">{val.title}</h3>
                                            <div className="travel-and-date">
                                                <p className="travelBlog"> {val.field_blog_tags}</p>
                                                <p>{val.field_date}</p>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                        </Col>
                        <Col md={9} lg={9}>
                        {content.length > 0 && content
                            .filter(val => val.sticky === 'True')
                            .map((val, ind) => {
                                return (
                                    <Row key={ind}>
                                        <Col md={12} lg={12} xs={10}>
                                            <img className="smallBlogImg" src={val.uri} alt="blogs" />
                                        </Col>
                                        {/* <Col xs={1}/> */}
                                        <Col md={12} lg={12} xs={13}>
                                            <div className="smallBlogWrapper">
                                                <p className="travelBlog">#{val.field_blog_tags}</p>
                                                <h2 className="travelBlogHeader">{val.title}</h2>
                                                <p>{val.field_date}</p>
                                            </div>
                                        </Col>
                                    </Row>
                                );
                            })}
                        </Col>
                    </Row>
                </div>
            })
            : null;
        // Rendering the complete section here
        return (
            <DefaultStyle>
                <Card cardType="layout2">
                <Row type="flex" justify="center">
                    <Heading headingType="headingTwo" 
                        label={
                            node.field_latest_insights_heading ?
                            node.field_latest_insights_heading[0].value
                        : ''}
                        className="heading"
                    />
                </Row>
                <Row className="tab-data-wrapper">
                    <Col lg={24} md={24}>
                        <Tabs
                        // Added only blog section as per client request
                        tabs={this.finalTabsData.filter((val, ind) => ind===0)}
                        tabType="type3"/>
                    </Col>
                </Row>
                <p className="see-all">
                    <a href="#" target="_blank">See All</a>
                </p>
                </Card>
            </DefaultStyle>
        );
    }
}
