import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    .claim-row{
        padding: 40px;
        .claim-numbers{
            border-right: 2px solid lightgrey;
            .number{
                color: ${colors.buttonActiveTabpane};
                opacity: 0.8;
                font-size: ${fonts.fontSizeNumberHeading};
                margin-bottom: 0;
            }
            p:last-child{
                color: ${colors.lighterTextColor};
                font-size: ${fonts.fontSizeText};
                font-weight: ${fonts.fontWeightHeadingOne};
            }
        }
        .award-wrapper{
            text-align: center;
            .award-img{
                width: 149px;
            }
        }
        .claim-content{
            .title{
                color: ${colors.headingTextColor};
                font-size: ${fonts.fontSizeHeadingThree};
                font-weight: ${fonts.fontWeightHeadingOne};
            }
            .description{
                color: ${colors.headingTextColor};
                font-size: ${fonts.fontSizeText};
            }
        }
    }
}`

export default DefaultStyle;
