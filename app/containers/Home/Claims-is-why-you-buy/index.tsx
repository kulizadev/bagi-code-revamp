import React from 'react';
import Card from 'components/Card';
import DefaultStyle from './Styles';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import renderHTML from 'react-render-html';
import dottie from 'dottie';
import DrupalHeading from 'containers/Home/Common-components/Drupal-header';

export default class ClaimsIs extends React.Component<any,any>{
   public render(){
        const paragraphs = dottie.get(this.props.productData, 'paragraphs');
        const node = dottie.get(this.props.productData, 'node');
        return (
            <DefaultStyle>
                {
                    node.field_claim_header ?
                    <DrupalHeading pTagHeading={node.field_claim_header[0].value} />
                    : ''
                }
                <Row type="flex" justify="center">
                    <Col lg={22} md={22}>
                        <Card cardType="layout1">
                            <Row className="claim-row" type="flex" justify="center" align="middle">
                                <Col lg={10} md={10} className="claim-numbers">
                                    {/* Claim number benefit Section */}
                                    <Row>
                                        {   paragraphs.field_claim_numbers ?
                                            paragraphs.field_claim_numbers.map((content,id) => {
                                                return (
                                                    <Col key={id} lg={12} md={12}>
                                                        <p className="number">{content.field_benefits_heading[0].value}</p>
                                                        {renderHTML(content.field_benefits_body[0].value)}
                                                    </Col>
                                                );
                                            })
                                        :''}
                                    </Row>
                                </Col>
                                {/* Award image & Claim content */}
                                <Col lg={4} md={4} className="award-wrapper">
                                    {
                                        paragraphs.field_claim_content ?
                                        <img
                                        src={paragraphs.field_claim_content[0].field_icon[0].url}
                                        alt={paragraphs.field_claim_content[0].field_icon[0].url}
                                        className="award-img"
                                        />
                                    : ''}
                                </Col>
                                <Col lg={10} md={10} className="claim-content">
                                {
                                    paragraphs.field_claim_content ?
                                    <p className="title">
                                        {paragraphs.field_claim_content[0].field_subtitle[0].value}
                                    </p>
                                : ''}
                                {
                                    paragraphs.field_claim_content ?
                                    <p className="description">
                                        {paragraphs.field_claim_content[0].field_description[0].value}
                                    </p>
                                : ''}
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </DefaultStyle>
        );
    }
}
