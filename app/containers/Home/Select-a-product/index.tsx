import React from 'react';
import Card from 'components/Card';
import Heading from 'components/Heading';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import DefaultStyle from './Styles';
import renderHTML from 'react-render-html';
import dottie from 'dottie';
import close from 'images/Homepage-assets/cancel.png';
import ProductFlowLinks from './flowLinks';

interface stateConfig {
    showAllProducts: boolean,
    clickedProductData: any
}
export default class SelectProduct extends React.Component<any,stateConfig>{
    state = {
        showAllProducts: true,
        clickedProductData: new Array()
    }
    closeFlowLinks = () => {
        this.setState({
            showAllProducts: !this.state.showAllProducts
        })
    }
    showSpecificFlowLinks = (content) => {
        this.setState({
            showAllProducts: !this.state.showAllProducts,
            clickedProductData: content
        })
    }
    render(){
        const paragraphs = dottie.get(this.props.productData, 'paragraphs');
        const node = dottie.get(this.props.productData, 'node');
        const { showAllProducts, clickedProductData } = this.state;
        return (
            <DefaultStyle>
                <Card cardType="layout2">
                    <Row type="flex" justify="center">
                        <Heading headingType="headingTwo" 
                            label={
                                node.field_banner_title ?
                                node.field_banner_title[0].value :
                            ''}
                            className="heading"
                        />
                    </Row>
                    {/* Product Cards */}
                    <Row type="flex" justify="center" align="middle">
                        {paragraphs.field_product_details && showAllProducts ?
                            paragraphs.field_product_details.map((content,key) => {
                            return (
                                <Col lg={3} md={3} className="card-wrapper"
                                key={key}>
                                    <Card cardType="layout5"
                                    onClick={() => this.showSpecificFlowLinks(content)}>
                                        <img
                                        src={content.field_image[0].url}
                                        alt={content.field_image[0].title}
                                        className="product-img"
                                        />
                                        <p className="product-name">{content.field_image[0].title}</p>
                                    </Card>
                                </Col>
                                );
                            })
                        : null}
                        {/* Product card - clicked state with flow links */}
                        {clickedProductData && !showAllProducts ?
                            <ProductFlowLinks selectedProduct={clickedProductData}/>
                        :null}
                        {/* close icon with the flow links */}
                        {!showAllProducts ?
                            <img src={close} alt="close" className="close-img" onClick={this.closeFlowLinks}/>
                        : null}
                    </Row>
                    {/* Generate quote and background image added in img tag */}
                    <Row type="flex" justify="center">
                        <p className="generated-quote">
                            {node.field_header_content ?
                                renderHTML(node.field_header_content[0].value)
                            : ''}
                        </p>
                    </Row>
                    { node.field_banner_image ?
                        <Row>
                            <img
                            src={node.field_banner_image[0].url}
                            alt={node.field_banner_image[0].alt}
                            className="banner-img"
                            />
                        </Row>
                    :''}
                </Card>
                {/* Blue Footer section */}
                <Row type="flex" justify="center" className="stats">
                    {paragraphs.field_banner_footer_content ?
                    paragraphs.field_banner_footer_content.map(content => {
                        return (
                            <Col lg={4} md={4} className="equals" key={content.field_benefits_body[0].value}>
                                <p className="stats-key">{content.field_benefits_heading[0].value}</p>
                                {renderHTML(content.field_benefits_body[0].value)}
                            </Col>
                        );
                    })
                    : ''}
                </Row>
            </DefaultStyle>
        );
    }
}
