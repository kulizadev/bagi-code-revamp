import React from 'react';
import Card from 'components/Card';
import Col from 'antd/lib/col';
import dottie from 'dottie';
import rightArrow from 'images/rightArrowC.png';


export default class ProductFlowLinks extends React.Component<any,any>{
    render(){
        const selectedProduct = dottie.get(this.props,'selectedProduct');
        if(!selectedProduct) return null;
        return (
            <React.Fragment>
                {/* Selected product single card rendered */}
                {selectedProduct ?
                    <Col lg={3} md={3} className="card-wrapper"
                        key={selectedProduct.field_image[0].title}>
                        <Card cardType="layout5">
                            <img
                            src={selectedProduct.field_image[0].url}
                            alt={selectedProduct.field_image[0].title}
                            className="product-img"
                            />
                            <p className="product-name">{selectedProduct.field_image[0].title}</p>
                        </Card>
                    </Col>
                : null}
                {/* Selected product sub-options rendered */}
                {selectedProduct ?
                    selectedProduct.field_flow_links.map((val,key) => {
                        return (
                            <Col lg={4} md={4} className="card-wrapper"
                            key={key}>
                                <a href={val.uri}>
                                    <div className="options">
                                            <span className="title">{val.title}</span>
                                            <img src={rightArrow} alt="arrow" className="arrow-img"/>
                                    </div>
                                </a>
                            </Col>
                        );
                    })
                : null}
            </React.Fragment>
        );
    }
}