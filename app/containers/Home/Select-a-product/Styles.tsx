import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    .heading{
        margin: 40px 0;
    }
    .card-wrapper{
        padding: 0 10px;
        div:hover{
            cursor: pointer;
            border: solid 1px #113184;
            .product-img{
                transition: 0.70s;
                -webkit-transition: 0.70s;
                -moz-transition: 0.70s;
                -ms-transition: 0.70s;
                -o-transition: 0.70s;
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        .product-img{
            height: 40px;
            display: block;
            margin: 30px auto 17px;
        }
        .product-name{
            font: ${fonts.fontSizeButton} ${fonts.fontFamilyPrimary};
            font-weight: ${fonts.fontWeightHeadingOne};
            text-align: center;
        }
    }
    .options{
        border-radius: 10px;
        box-shadow: 0px 3px 7.4px 0.6px rgba(17, 49, 132, 0.18);
        border: solid 1px #c2cadf;
        padding: 20px 15px 15px;
        text-align: center;
        .title{
            font-size: ${fonts.fontSizeText};
            color: ${colors.enabledButtonBG};
            margin-right: 9px;
        }
        .arrow-img{
            width: 20px;
            padding-bottom: 5px;
        }
    }
    .options:hover{
        cursor: pointer;
        border: solid 1px #113184;
        .arrow-img{
            -webkit-transition: 0.70s;
            -moz-transition: 0.70s;
            -ms-transition: 0.70s;
            -o-transition: 0.70s;
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    .close-img{
        cursor: pointer;
        height: 20px;
    }
    .generated-quote{
        font: ${fonts.fontSizeOverlineLink} ${fonts.fontFamilyPrimary};
        font-weight: ${fonts.fontWeightPrimary};
        margin-top: 20px;
    }
    .banner-img{
        width: 100%;
        background-color: ${colors.secondaryBodyBG}
    }
    .stats{
        background-color: ${colors.homepageStatsBG};
        text-align: center;
        font-weight: ${fonts.fontWeightHeadingOne};
        .equals:not(:last-child):after{
            content: "";
            background-color: #666;
            position: absolute;
            width: 5px;
            height: 64px;
            top: 5px;
            right: 0;
            opacity: 0.37;
        }
        .stats-key{
            font-size: ${fonts.fontSizeButton};
            opacity: 0.8;
            margin: 19px 0 5px;
        }
        .p:last-child{
            font-size: ${fonts.fontSizeLink};
            opacity: 0.8;
            margin-bottom: 18px;
        }
    }
}`

export default DefaultStyle;
