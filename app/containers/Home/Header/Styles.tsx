import styled from 'styled-components';
import { colors } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    height: 88px;
    background-color: ${colors.secondaryBodyBG};
    box-shadow: 2.5px 4.3px 7px 1px rgba(81, 81, 81, 0.06);
    margin-bottom: 5px;
    // Headings and Logo Section CSS
    .left-div{
        padding-left: 40px;
        width: 100%;
        // display: flex;
        // .header-subOptions-card:nth-child(odd){
        //     border-right: 1px solid black;
        // }
        .header-subOptions-card {
            minHeight: 330px;
        }
        .logo-image{
            margin-top: 10px;
        }
        .heading-tab {
            margin-top: 22px;
            z-index: 2;
            width: 100%;
        }
        .ant-menu-horizontal {
            border-bottom: none
        }
        .ant-menu-submenu > .ant-menu {
            background-color: #fff;
            border-radius: 4px;
            //width: 65%;
            // margin-left: 15%;
        }
    }   
    .ant-menu-submenu-popup {
        position: absolute;
        z-index: 1050;
        background: #fff;
        border-radius: 4px;
        margin: 0 15%;

    }
    .subMenu-heading-container {
        margin: 0 15%;
    }
   .heading-subMenu-para {
        padding:  120px;
    }

    .header-subOptions-image {
        width: 69px;
        height: 28px;
    }

    .text-align-center {
        text-align: center;
    }

    // Right Div - Call, Buy, Renew/Claim CSS
    .right-div{
        width: 40%;
        display: inline-block;
        // float: right;
    }
}`

export default DefaultStyle;
