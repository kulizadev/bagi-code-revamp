import React from 'react';
// import { headings } from './constants';
import logoImg from 'images/bagi-logo.png';
import DefaultStyle from './Styles';
import Tabs from 'components/Tabpane';
import _get from 'lodash/get';
import { Row, Col, Menu } from 'antd';
import ButtonComponent from 'components/Button';
import CardComponent from 'components/Card';

const SubMenu = Menu.SubMenu;


interface headerProps {
	headingsRequired: boolean,
	headerData: any
}
export default class Header extends React.PureComponent<headerProps, any>{


	innerChildProp = (innerChildren) => <Row>
		{
			Array.isArray(innerChildren) && innerChildren.map(innerChildrenContent => {
				return (
					<Col md={12} style={{ padding: '12px' }}>
						<CardComponent cardType='layout2'>
							<Row type="flex" justify="center">
								<img className="header-subOptions-image" style={{ width: '69px', height: '28px' }} src={innerChildrenContent.uri} />
							</Row>
							<Row type="flex" justify="center">
								<h4 className='text-align-center'>
									<a href={innerChildrenContent.field_menu_link} target='_blank'>
										{innerChildrenContent.title}
									</a>
								</h4>
							</Row>
							<Row type="flex" justify="center">
								<p className="heading-subMenu-para">
									{innerChildrenContent.field_menu_description}
								</p>
							</Row>
							<Row type="flex" justify="center">
								<Col style={{ marginRight: '9px' }}>
									<ButtonComponent
										label='BUY NOW'
										buttonType='filled'
									/>
								</Col>
								<Col>
									<ButtonComponent
										label='EXPLORE'
										buttonType='bordered'
									/>
								</Col>
							</Row>
						</CardComponent>
					</Col>
				)
			})
		}
	</Row>

	innerChildPropForArray = (innerChildren) => Array.isArray(innerChildren) && innerChildren.length ? <Row style={{ padding: '12px' }}>
		{
			innerChildren.map(innerChildrenContent => {
				return (
					<CardComponent cardType='layout2'>
						<Row type="flex" style={{ paddingBottom: '13px' }}>
							<Col>
								<img className="header-subOptions-image" style={{ width: '33px', height: '27px' }} src={innerChildrenContent.uri} />
							</Col>
							<Col style={{paddingLeft: '10px'}}>
								<h4 style={{ marginTop: '13px' }}>
								<a href={innerChildrenContent.field_menu_link} target='_blank'>
									{innerChildrenContent.title}
								</a>
								</h4>
							</Col>
						</Row>
					</CardComponent>
				)
			})
		}
	</Row> : null;


	handleSubMenuTabsClick = (e) => {
		//e.preventDefault();
		e.stopPropagation();
	}

	finalTabsData = new Array();
	render() {
		const { headingsRequired, headerData = {} } = this.props;
		const headingArray = Object.keys(headerData);
		headingArray.map((content, index) => {
			this.finalTabsData[index] = {};
			this.finalTabsData[index]['heading'] = content;
			this.finalTabsData[index]['key'] = String(index);
			let innerTabsData = new Array();

			let innerHeadingArray = new Array();

			if (Array.isArray(headerData[content])) {
				innerTabsData[0] = {};
				innerTabsData[0]['heading'] = headerData[content][0].title;
				innerTabsData[0]['key'] = headerData[content][0].title;
				let innerChildren = headerData[content];
				innerTabsData[0]['children'] = this.innerChildPropForArray(innerChildren);
			} else {
				innerHeadingArray = Object.keys(_get(headerData, `${[content]}`, {}));
				innerHeadingArray.map((innerContent, innerIndex) => {
					innerTabsData[innerIndex] = {};
					innerTabsData[innerIndex]['heading'] = innerContent;
					innerTabsData[innerIndex]['key'] = String(innerIndex);
					let innerChildren = _get(_get(headerData, `${[content]}`), `${[innerContent]}`);
					innerTabsData[innerIndex]['children'] = this.innerChildProp(innerChildren);
				});
			}

			this.finalTabsData[index]['children'] = (<CardComponent cardType='layout2'>
				{
					Array.isArray(headerData[content]) ?
						innerTabsData[0]['children']
						: <Tabs
							tabs={innerTabsData}
							headingSubMenuTabsWidth='750px'
							tabType="type1"
						/>
				}
			</CardComponent>)
		});


		return (
			<DefaultStyle>
				<div className="left-div">
					<Row>
						<Col span={3}>
							<img src={logoImg} alt={"Bharti Axa Logo"} className={"logo-image"} />
						</Col>
						<Col span={18}>
							<div className='heading-tab'>
								{headingsRequired &&
									<Menu
										mode="horizontal"
									>
										{headingArray.map((heading, index) => (
											<SubMenu title={<span>{heading}</span>}>
												<div onClick={this.handleSubMenuTabsClick}>
													{this.finalTabsData[index]['children']}
												</div>
											</SubMenu>
										))}
									</Menu>
								}
							</div>
						</Col>
						<Col span={3}>
							<div className="right-div">

							</div>
						</Col>
					</Row>
				</div>
			</DefaultStyle>
		);
	}
}