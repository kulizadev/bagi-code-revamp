const categoryOptions = [
    {
        label: "Car",
        value: "Car"
    },
    {
        label: "Travel",
        value: "Travel"
    },
    {
        label: "Health",
        value: "Health"
    }
];

export default categoryOptions;
