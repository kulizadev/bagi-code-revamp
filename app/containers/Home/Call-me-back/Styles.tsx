import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    margin: 40px 0;
    .content-wrapper{
        padding: 32px 43px 0;
        .unable{
            font-size: ${fonts.fontSizeHeadingTwo};
            font-weight: ${fonts.fontWeightHeadingOne};
            color: ${colors.lighterTextColor};
            margin-bottom: 15px;
        }
        .call{
            font-size: ${fonts.fontSizeHeadingThree};
            color: ${colors.lighterTextColor};
            margin-bottom: 24px;
        }
        .pt15{
            padding-top: 15px;
        }
        .mr10{
            margin-right: 10px;
        }
        .mr16{
            margin-right: 16px;
        }
        .or-sec{
            margin-top: 14px;
            text-align: center;
        }
        .or-sec span{
            position: relative;
            color: ${colors.orText};
            font-size: ${fonts.fontSizeOverlineLink};
        }
        .or-sec span::before, .or-sec span::after{
            border-left: 1.5px solid ${colors.orText};
            height: 30px;
            content: "";
            position: absolute;
            left: 50%;
            bottom: 100%;
        }
        .or-sec span::after{
            top: 100%;
        }
        .or-sec span::after, .or-sec span::before{
            transform: rotate(180deg);
        }
        .call-us{
            font-size: ${fonts.fontSizeText};
            color: ${colors.headingTextColor};
            margin-bottom: 0;
        }
        .call-no{
            font-size: ${fonts.fontSizeHeadingTwo};
            color: ${colors.callNumber};
        }
    }
    .checkbox-wrapper{
        margin-left: 44px;
        padding-top: 2px;
        .checkbox-item .ant-form-item-control{
            line-height: 0;
        }
    }
    .authorize{
        font-size: ${fonts.fontSizeOverlineLink};
        color: ${colors.checkboxTextGrey};
    }
}`

export default DefaultStyle;
