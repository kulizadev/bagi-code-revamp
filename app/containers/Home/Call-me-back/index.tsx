import React from 'react';
import Card from 'components/Card';
import DefaultStyle from './Styles';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Form from 'antd/lib/form';
import message from 'antd/lib/message';
import Input from 'components/Input';
import Select from 'components/Select';
import Button from 'components/Button';
import Checkbox from 'components/Checkbox';
import GlobalConstants from 'constants/globalConstants';
import categoryOptions from './constants';
import { request } from 'configs/networklayer';
import BaseUrls from 'constants/apiConstants';
import { errorMsgs } from 'constants/staticMessages';
import regex from 'constants/regex';
import tick from 'images/Homepage-assets/tick.png';

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}
export default class CallMeBack extends React.Component<any,any>{
    state = {
        requestReceived: false
    }
    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
    }
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                // console.log('Received values of form: ', values);
                message.loading('Request Processing');
                const callMeObj = { "prospect_id" : "", "phone": values.phonenumber, "product_type": values.category, "section" : "call-back" };
                const { PythonUrl, callMeBack } = BaseUrls;
                const url = PythonUrl + callMeBack;
                request(url, {}, 'POST', callMeObj).then(response => {
                    message.success('We will contact you soon');
                    this.setState({
                        requestReceived: true
                    })
                })
                .catch(err => {
                    message.error('Some error occured');
                });
            }
        });
    };
    render(){
        const { requestReceived } = this.state;
        const { unableToDecide, scheduleCall, callUs, authorize, contactNumber, thankYou, executive } = GlobalConstants;
        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
        const phoneNumberErr = isFieldTouched('phonenumber') && getFieldError('phonenumber');
        const categoryErr = isFieldTouched('category') && getFieldError('category');
        const { phoneNumberError, productCategoryError } = errorMsgs;
        const { phoneNumberRegex } = regex;
        return (
            <DefaultStyle>
                {/* This section is hardcoded, not provided by Drupal */}
                <Row type="flex" justify="center">
                    <Col lg={22} md={22}>
                        <Card cardType="layout1">
                            <Form onSubmit={this.handleSubmit}>
                                <Row className="content-wrapper">
                                {!requestReceived ?
                                <React.Fragment>
                                    <Col lg={7} md={7}>
                                        <p className="unable">{unableToDecide}</p>
                                        <p className="call">{scheduleCall}</p>
                                    </Col>
                                    <Col lg={4} md={4} className="pt15 mr10">
                                        <Form.Item
                                        validateStatus={phoneNumberErr ? 'error' : ''}
                                        help={phoneNumberErr || ''}
                                        >
                                            {getFieldDecorator('phonenumber',{
                                                rules: [
                                                    {
                                                        required: true,
                                                        pattern: phoneNumberRegex,
                                                        message: phoneNumberError
                                                    },
                                                ],
                                            })(
                                            <Input inputType="type1"
                                            placeholder="9858697815"
                                            maxLength={10}
                                            />,
                                            )}
                                        </Form.Item>
                                    </Col>
                                    <Col lg={4} md={4} className="pt15 mr16">
                                        <Form.Item
                                        validateStatus={categoryErr ? 'error' : ''}
                                        help={categoryErr || ''}
                                        >
                                            {getFieldDecorator('category',{
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: productCategoryError,
                                                    },
                                                ],
                                            })(
                                            <Select placeholder="Category"
                                            showArrow={true}
                                            options={categoryOptions}
                                            />)}
                                        </Form.Item>
                                    </Col>
                                    <Col lg={3} md={3} className="pt15">
                                        <Form.Item>
                                            <Button buttonType="filled"
                                            label="Call me back"
                                            htmlType="submit"
                                            disabled={hasErrors(getFieldsError())}
                                            />
                                        </Form.Item>
                                    </Col>
                                </React.Fragment>
                                :
                                <React.Fragment>
                                    <Col lg={2}>
                                        <img src={tick} alt="green-tick-img"/>
                                    </Col>
                                    <Col lg={16}>
                                        <p className="unable">{thankYou}</p>
                                        <p className="call">{executive}</p>
                                    </Col>
                                </React.Fragment>
                                }
                                    <Col md={1} lg={1} className="or-sec pt15 mr16">
                                        <span>OR</span>
                                    </Col>
                                    <Col lg={4} md={4} className="pt15">
                                        <p className="call-us">{callUs}</p>
                                        <p className="call-no">{contactNumber}</p>
                                    </Col>
                                </Row>
                                {!requestReceived ?
                                <Row>
                                    <Col lg={24} md={24} className="checkbox-wrapper">
                                        <Form.Item
                                        help={''}
                                        className="checkbox-item"
                                        >
                                        {getFieldDecorator('authorize',{
                                            rules: [
                                                {
                                                    required: true,
                                                    transform: value => (value || undefined),
                                                    type: 'boolean'
                                                }
                                            ]
                                        })(
                                            <Checkbox>
                                                <span className="authorize">{authorize}</span>
                                            </Checkbox>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                                :null}
                            </Form>
                        </Card>
                    </Col>
                </Row>
            </DefaultStyle>
        );
    }
}
