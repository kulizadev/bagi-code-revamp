import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

export const DefaultStyle = styled.div`{
    .heading{
        margin: 40px 0;
        text-align: center;
        padding: 20px 0;
    }
    .ant-tabs-nav-scroll{
        margin: 0 auto;
        text-align: center;
    }
    .review-btn{
        text-align: center;
        padding: 60px 0;
    }
}`;


export const CarousalWrapper = styled.div`
    .carousel-avatar{
        position: relative;
        top: 35px;
        z-index: 1;
        img{
            width: 11em;
            border-radius: 50%;
            position: absolute;
            left: 50%;
            top:50%;
            box-shadow: 1px 1px 20px 2px;
        }
    }
    .carousel-content{
        padding: 30px;
        border-radius: 20px;
        box-shadow: 0px 1px 12.8px 2.3px rgba(0, 0, 0, 0.08);
        margin-top: 10px;
        p{
            padding: 18px 18px 18px 90px;
            text-align: justify;
            font-size: ${fonts.fontSizeButton};
            color: ${colors.headingTextColor};
            margin-bottom: 0;
        }
        .content-heading-rating{
            display: table;
            padding-left: 90px;
        }
        .rating-wrapper{
            display: flex;
            margin-bottom: 5px;
            .person-name{
                margin-right: 12px;
                padding-top: 5px;
                color: ${colors.headingTextColor};
            }
            .avg-rating{
                padding: 5px 0 0 12px;
                border-left: 1px solid;
                color: ${colors.headingTextColor};
            }
            .rated{
                padding-top: 2px;
                background: rgb(105, 151, 220);
                border-radius: 10px;
                margin-left: 2px;
                height: 30px;
                width: 55px;
                color: ${colors.buttonTextColor};
                font-size: ${fonts.fontSizeHeadingThree};
                text-align: center;
            }
        }
        .designation {
            color: ${colors.headingTextColor};
        }
        .carousel-image{
            display: inline;
            padding-left: 70px;
        }
        .carousel-image-opp{
            float: right;
            margin-top: -21px 24px 0 0;
        }
    }
`;