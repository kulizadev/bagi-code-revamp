import React from 'react';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import dottie from 'dottie';
import renderHTML from 'react-render-html';
import Carousal from 'components/Carousal';
import quoteStart from 'images/corousal.jpg';
import quoteEnd from 'images/crousal-upward.jpg';
import defaultImg from 'images/Homepage-assets/defaultDp.png';
import {CarousalWrapper} from './Styles';
 
class CustomersCarousel extends React.Component<any,any> {
    render() {
        const staticText = dottie.get(this.props, 'staticText');
        const data = dottie.get(this.props, 'data');
        if(!data) return null;
        return (
            <CarousalWrapper>
                <Carousal showArrow={true}>
                    {data ? data.map((val,index) => {
                        return (
                            <div key={index}>
                                <Row>
                                    <Col className="carousel-avatar" md={4} lg={4} sm={4} xs={4}>
                                        <img src={val.uri?val.uri:defaultImg} alt="profile-img" />
                                    </Col>
                                    <Col className="carousel-content" md={18} sm={18} xs={18} lg={18}>
                                        <img className="carousel-image" src={quoteStart} alt="quote Image"/>
                                        {renderHTML(val.comment_body)}
                                        <img className="carousel-image-opp" src={quoteEnd} alt="quote Image"/>
                                        <div className="content-heading-rating">
                                            <div  className="rating-wrapper">
                                                <div className="person-name">
                                                    <span>{val.field_customer_name} </span>
                                                </div>
                                                <div className="avg-rating">
                                                    Rated
                                                </div>
                                                <div className="rated"> {val.field_rating}</div>
                                            </div>
                                            <div className="designation">{val.field_designation}</div>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        );
                    })
                    :''}
                </Carousal>
            </CarousalWrapper>
        );
    }
}
 
export default CustomersCarousel;