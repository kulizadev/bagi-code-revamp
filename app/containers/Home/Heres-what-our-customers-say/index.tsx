import React from 'react';
import Card from 'components/Card';
import Button from 'components/Button';
import Heading from 'components/Heading';
import {DefaultStyle} from './Styles';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import dottie from 'dottie';
import Tabs from 'components/Tabpane';
//Imported network layer
import { request } from 'configs/networklayer';
import BaseUrls from 'constants/apiConstants';
import CustomersCarousel from './customers';
import _get from 'lodash/get';

interface StateConfig {
    internalApiStateData: any
}
export default class HeresWhat extends React.Component<any,StateConfig>{
    public finalTabsData = new Array();
    componentWillReceiveProps(nextProps){
        const node = dottie.get(nextProps.productData, 'node');
        const { DrupalUrl, DrupalCredential } = BaseUrls;
        const authorization = 'Basic ' + btoa(DrupalCredential.username + ':' + DrupalCredential.password);
        const apiUrlArray = new Array();
        dottie.get(node, 'field_homepage_api_links.length') ?
        node.field_homepage_api_links.map((content,key) => {
            const url = DrupalUrl + content.value;
            apiUrlArray.push(url);
        })
        :null;
        if(apiUrlArray.length == 2){
            const internalApiData = new Array();
            request(apiUrlArray[0], authorization, 'get', null).then(drupalResponse => {
                internalApiData.push(drupalResponse);
                this.setState({
                    internalApiStateData: internalApiData
                })
                request(apiUrlArray[1], authorization, 'get', null).then(drupalResponse => {
                    internalApiData.push(drupalResponse);
                    this.setState({
                        internalApiStateData: internalApiData
                    })
                });
            });
        }
    }
    makeFinalTabsData = () => {
        const { internalApiStateData } = this.state;
        if(internalApiStateData.length == 2){
            const stateSingleMap = {..._get(internalApiStateData[0],'rows',{}),
            ..._get(internalApiStateData[1],'rows',{})};
            const headingsArray = Object.keys(stateSingleMap);
            headingsArray.map((content,index) => {
                this.finalTabsData[index] = {};
                this.finalTabsData[index]["heading"] = content;
                this.finalTabsData[index]["key"] = String(index);
                this.finalTabsData[index]["children"] =
                <CustomersCarousel
                data={stateSingleMap[content]}
                // staticText={node.field_review_heading?node.field_review_heading:''}
                />;
            });
        }
    }
    render(){
        if(!this.state) return null;
        const node = dottie.get(this.props.productData, 'node');
        this.makeFinalTabsData();
        return (
            <DefaultStyle>
                <Card cardType="layout2">
                    <Heading headingType="headingTwo" 
                        label={
                            node.field_review_heading ?
                            node.field_review_heading[0].value
                        : ''}
                        className="heading"
                    />
                    <Row className="tab-data-wrapper">
                        <Col lg={24} md={24}>
                            <Tabs
                            tabs={this.finalTabsData}
                            tabType="type2"/>
                        </Col>
                    </Row>
                    <div className="review-btn">
                        <Button buttonType="filled" label="Leave a review"/>
                    </div>
                </Card>
            </DefaultStyle>
        );
    }
}
