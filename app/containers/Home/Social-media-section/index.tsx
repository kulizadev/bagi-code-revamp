import React from 'react';
import dottie from 'dottie';
import DefaultStyle from './Styles';
import GlobalConstants from 'constants/globalConstants';
import images from './image-array';

export default class SocialMedia extends React.Component<any,any>{
    render(){
        const node = dottie.get(this.props.productData, 'node');
        const { socialMedia } = GlobalConstants;
        const { imageArray, socialMediaArray } = images;
        return (
            <DefaultStyle>
                <p className="heading">{socialMedia}</p>
                <div className="social-media-wrapper">
                    {socialMediaArray.map((val,key)=> {
                        return (
                            <a href={val.url} className="icons" target="_blank" key={key}>
                                <img src={val.image} alt="social-media-icon" />
                            </a>
                        )
                    })}
                </div>
                <div className="contact-details">
                    {node.field_site_info_links ? 
                        node.field_site_info_links.map((content,key) => {
                            return (
                                <div key={key}>
                                    <img src={imageArray[key]} alt=""/>
                                    <a className="text" href={content.uri}>
                                        {content.title}
                                    </a>
                                </div>
                            );
                        })                            
                    :''}
                </div>
            </DefaultStyle>
        )
    }
}