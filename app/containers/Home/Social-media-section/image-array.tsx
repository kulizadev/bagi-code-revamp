import fb from 'images/Homepage-assets/social-media/fb.png';
import twitter from 'images/Homepage-assets/social-media/twitter.png';
import linkedin from 'images/Homepage-assets/social-media/linkedin.png';
import youtube from 'images/Homepage-assets/social-media/youtube.png';
import email from 'images/Homepage-assets/social-media/email.png';
import phone from 'images/Homepage-assets/social-media/phone.png';
import web from 'images/Homepage-assets/social-media/web.png';

const socialMediaArray = [
    {
        'image': fb,
        'url': 'https://www.facebook.com/BhartiAXAGeneralInsuranceCompany'
    },
    {
        'image': twitter,
        'url': 'https://twitter.com/BhartiAXAGI'
    },
    {
        'image': linkedin,
        'url': 'https://www.linkedin.com/company-beta/289098/'
    },
    {
        'image': youtube,
        'url': 'https://www.youtube.com/user/BhartiAXAGI'
    },
];
const imageArray = [
    email, phone, web
];
const images = {
    imageArray, socialMediaArray
}
export default images;
