import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    .heading{
        margin: 60px 0 41px;
        color: ${colors.headingTextColor};
        font-size: ${fonts.fontSizeHeadingThree};
        text-align: center;
    }
    .social-media-wrapper{
        text-align: center;
        .icons:first-child{
            margin-left: 34px;
        }
        .icons{
            width: 41px;
            margin-right: 34px;
        }
    }
    .contact-details{
        display: flex;
        justify-content: space-around;
        margin: 35px 0 66px;
        .text{
            color: ${colors.enabledButtonBG};
            font-size: ${fonts.fontSizeText};
            font-weight: ${fonts.fontWeightHeadingOne};
            margin-left: 20px;
        }
    }
}`

export default DefaultStyle;
