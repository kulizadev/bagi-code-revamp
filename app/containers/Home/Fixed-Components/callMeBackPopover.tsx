import React from 'react';
import Card from 'components/Card';
import DefaultStyle from './Styles';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import Form from 'antd/lib/form';
import message from 'antd/lib/message';
import Input from 'components/Input';
import Select from 'components/Select';
import Button from 'components/Button';
import Checkbox from 'components/Checkbox';
import GlobalConstants from 'constants/globalConstants';
import categoryOptions from 'containers/Home/Call-me-back/constants';
import { request } from 'configs/networklayer';
import BaseUrls from 'constants/apiConstants';
import { errorMsgs } from 'constants/staticMessages';
import regex from 'constants/regex';
import tick from 'images/Homepage-assets/tick.png';

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}
export default class CallMeBack extends React.Component<any,any>{
    state = {
        requestReceived: false
    }
    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
    }
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                // console.log('Received values of form: ', values);
                message.loading('Request Processing');
                const callMeObj = { "prospect_id" : "", "phone": values.phonenumber, "product_type": values.category, "section" : "call-back" };
                const { PythonUrl, callMeBack } = BaseUrls;
                const url = PythonUrl + callMeBack;
                request(url, {}, 'POST', callMeObj).then(response => {
                    message.success('We will contact you soon');
                    this.setState({
                        requestReceived: true
                    })
                })
                .catch(err => {
                    message.error('Some error occured');
                });
            }
        });
    };
    render(){
        const { requestReceived } = this.state;
        const { scheduleCall, tnc, thankYou, executive } = GlobalConstants;
        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
        const phoneNumberErr = isFieldTouched('phonenumber') && getFieldError('phonenumber');
        const categoryErr = isFieldTouched('category') && getFieldError('category');
        const { phoneNumberError, productCategoryError } = errorMsgs;
        const { phoneNumberRegex } = regex;
        return (
            <DefaultStyle>
                {/* This section is hardcoded, not provided by Drupal */}
                <Card cardType="layout1">
                    <Form onSubmit={this.handleSubmit}>
                        <Row className="content-wrapper" type="flex" justify="center">
                        {!requestReceived ?
                            <React.Fragment>
                                <Col lg={20} md={20}>
                                    <p className="call">{scheduleCall}</p>
                                </Col>
                                <Col lg={11} md={11} className="mr20">
                                    <Form.Item
                                    validateStatus={phoneNumberErr ? 'error' : ''}
                                    help={phoneNumberErr || ''}
                                    className="mb0"
                                    >
                                        {getFieldDecorator('phonenumber',{
                                            rules: [
                                                {
                                                    required: true,
                                                    pattern: phoneNumberRegex,
                                                    message: phoneNumberError
                                                },
                                            ],
                                        })(
                                        <Input inputType="type1"
                                        placeholder="9858697815"
                                        maxLength={10}
                                        />,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col lg={11} md={11}>
                                    <Form.Item
                                    validateStatus={categoryErr ? 'error' : ''}
                                    help={categoryErr || ''}
                                    className="mb0"
                                    >
                                        {getFieldDecorator('category',{
                                            rules: [
                                                {
                                                    required: true,
                                                    message: productCategoryError,
                                                },
                                            ],
                                        })(
                                        <Select placeholder="Category"
                                        showArrow={true}
                                        options={categoryOptions}
                                        />)}
                                    </Form.Item>
                                </Col>
                                <Col lg={24} md={24} className="checkbox-wrapper">
                                    <Form.Item
                                    help={''}
                                    className="checkbox-item"
                                    >
                                    {getFieldDecorator('authorize',{
                                        rules: [
                                            {
                                                required: true,
                                                transform: value => (value || undefined),
                                                type: 'boolean'
                                            }
                                        ]
                                    })(
                                        <Checkbox>
                                            <span className="authorize">{tnc}</span>
                                        </Checkbox>
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col lg={10} md={10} className="">
                                    <Form.Item className="mb0">
                                        <Button buttonType="filled"
                                        label="Call me back"
                                        htmlType="submit"
                                        disabled={hasErrors(getFieldsError())}
                                        />
                                    </Form.Item>
                                </Col>
                            </React.Fragment>
                            :
                            <React.Fragment>
                                <img src={tick} alt="green-tick-img"/>
                                <p className="thanks">{thankYou}</p>
                                <p className="executive mb0">{executive}</p>
                            </React.Fragment>
                            }
                        </Row>
                    </Form>
                </Card>
            </DefaultStyle>
        );
    }
}
