import React from 'react';
import DefaultStyle from './Styles';
// import feedback from 'images/Homepage-assets/fixed-images/feedback.png';
import call from 'images/Homepage-assets/fixed-images/call.png';
import selfHelp from 'images/Homepage-assets/fixed-images/self-help.png';
import Popover from 'components/Popover';
import CallMeBack from './callMeBackPopover';
import Form from 'antd/lib/form';

const CallMeBackForm = Form.create({ name: 'callMeBack' })(
    CallMeBack,
);
export default class FixedComponents extends React.Component{
    render(){
        return (
            <DefaultStyle>
                <div className="call-feedback">
                    {/* To be uncommented later after getting feedback requirement */}
                    {/* <a href="#">
                        <img src={feedback} alt="" className="imgs-common"/>
                        <p className="text-common text">Feedback</p>
                    </a> */}
                    <Popover placement='left' content={<CallMeBackForm />} width='371px'>
                        <img src={call} alt="" className="imgs-common call-img"/>
                        <p className="text-common text">Call</p>
                    </Popover>
                </div>
                <div className="self-help">
                    <a href="#">
                        <img src={selfHelp} alt="" className="imgs-common help-img"/>
                        <p className="text-common help">Self Help</p>
                    </a>
                </div>
            </DefaultStyle>
        )
    }
}