import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    .call-feedback{
        width: 96px;
        position: fixed;
        top: 25%;
        right: -6px;
        z-index: 9;
        border-radius: 10px;
        box-shadow: 0px 3px 7.4px 0.6px rgba(17, 49, 132, 0.18);
        border: solid 2px #c2cadf;
        background-color: #ffffff;
        text-align: center;
        cursor: pointer;
        // padding: 24px 0; To be uncommented later
        .text{
            font-size: ${fonts.fontSizeOverlineLink};
        }
        .call-img{
            padding-top: 13px;
            // border-top: 1px solid ${colors.enabledButtonBG}; To be uncommented later
        }
    }
    .text-common{
        font-weight: bold;
        color: ${colors.enabledButtonBG};
    }
    .imgs-common{
        width: 46px;
    }
    .self-help{
        position: fixed;
        right: 60px;
        bottom: 15%;
        z-index: 9;
        text-align: center;
        .help{
            font-size: ${fonts.fontSizeText};
        }
    }
    .self-help:hover .help{
        -webkit-transition: 1.50s;
        -moz-transition: 1.50s;
        -ms-transition: 1.50s;
        -o-transition: 1.50s;
        -webkit-transform: rotateY(360deg);
        -moz-transform: rotateY(360deg);
        -o-transform: rotateY(360deg);
        -ms-transform: rotateY(360deg);
        transform: rotateY(360deg);
    }
    // Call Me Back Popover CSS
    .content-wrapper{
        padding: 10px;
        .call{
            font-size: ${fonts.fontSizeButton};
            color: ${colors.lighterTextColor};
            margin-bottom: 17px;
        }
        .mr20{
            margin-right: 20px;
        }
        .mb0{
            margin-bottom: 0;
        }
        .checkbox-wrapper{
            margin-left: 5px;
            padding-top: 2px;
            .checkbox-item .ant-form-item-control{
                line-height: 0.5;
            }
        }
        .authorize{
            font-size: ${fonts.fontSizeFooterText};
            color: ${colors.checkboxTextGrey};
        }
        .thanks{
            font-size: ${fonts.fontSizeButton};
            color: ${colors.headingTextColor};
            margin: 20px 0 13px;
        }
        .executive{
            font-size: ${fonts.fontSizeText};
            color: ${colors.lighterTextColor};
            text-align: center;
        }
    }
}`

export default DefaultStyle;