import React from 'react';
import DefaultStyle from './Styles';
import renderHTML from 'react-render-html';

export default class DrupalHeading extends React.Component<any,any>{
    render(){
        const { pTagHeading } = this.props;
        return (
            <DefaultStyle>
                {/* Drupal Header render - response in P tag, wrapped in div */}
                <div className="drupalHeader">
                    { renderHTML(pTagHeading) }
                </div>
            </DefaultStyle>
        );
    }
}