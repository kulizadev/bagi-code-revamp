import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    .drupalHeader{
        p{
            color: ${colors.headingTextColor};
            font-weight: ${fonts.fontWeightPrimary};
            font: ${fonts.fontSizeText} ${fonts.fontFamilyPrimary};
            text-align: center;
        }
        p:first-child{
            font-size: ${fonts.fontSizeHeadingTwo};
            line-height: ${fonts.lineHeightH2};
            margin: 40px 0 10px;
        }
    }
}`

export default DefaultStyle;
