import React from 'react';
import Card from 'components/Card';
import DefaultStyle from './Styles';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import dottie from 'dottie';
import DrupalHeading from 'containers/Home/Common-components/Drupal-header';

export default class Awards extends React.Component<any,any>{
    render(){
        const paragraphs = dottie.get(this.props.productData, 'paragraphs');
        const node = dottie.get(this.props.productData, 'node');
        return (
            <DefaultStyle>
                {
                    node.field_awards_heading ?
                    <DrupalHeading pTagHeading={node.field_awards_heading[0].value} />
                    : ''
                }
                <Row type="flex" justify="center">
                    <Col lg={22} md={22}>
                        <Card cardType="layout1">
                            <Row type="flex" justify="center">
                                {   paragraphs.field_awards ?
                                    paragraphs.field_awards.map((content,id) => {
                                    return (
                                        <Col lg={6} md={6} key={id} offset={1}>
                                            <div className="img-wrapper">
                                                <img className="award-img" src={content.field_icon[0].url} alt={content.field_icon[0].alt}/>
                                            </div>
                                            <p className="award-subtitle">{content.field_subtitle[0].value}</p>
                                            <p className="award-description">{content.field_description[0].value}</p>
                                        </Col>
                                    );
                                }): ''}
                            </Row>
                            <p className="see-all">
                                <a href="#" target="_blank">See All</a>
                            </p>
                        </Card>
                    </Col>
                </Row>
            </DefaultStyle>
        );
    }
}
