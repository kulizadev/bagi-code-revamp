import styled from 'styled-components';
import { fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    .img-wrapper{
        text-align: center;
        margin-top: 25px;
        .award-img{
            width: 80px;
        }
    }
    .award-subtitle{
        font-size: ${fonts.fontSizeButton};
        font-weight: ${fonts.fontWeightHeadingOne};
        margin-bottom: 14px;
    }
    .award-description{
        font-size: ${fonts.fontSizeOverlineLink};
        margin-bottom: 29px;
    }
    .see-all{
        text-align: center;
        padding-bottom: 19px;
        margin-left: 4%;
        font-size: ${fonts.fontSizeOverlineLink};
    }
}`

export default DefaultStyle;
