import React from 'react';
import Form from 'antd/lib/form';
//Imported each Section
import Header from "./Header";
import SelectProduct from './Select-a-product';
import AwarenessAbout from './Awareness-about-bharti-axa';
import ClaimsIs from './Claims-is-why-you-buy';
import HeresWhat from './Heres-what-our-customers-say';
import Awards from './Awards-and-recognitions';
import Interested from './We-got-you-interested';
import CallMeBack from './Call-me-back';
import Peep from './Peep-into-our-latest-insights';
import SocialMedia from './Social-media-section';
import Footer from './IRDAI-and-footer';
import FixedComponents from './Fixed-Components';
//Imported network layer
import { request } from 'configs/networklayer';
import BaseUrls from 'constants/apiConstants';

const CallMeBackForm = Form.create({ name: 'callMeBack' })(
    CallMeBack,
);
export default class Home extends React.Component{
    state = {
        isLoading: true,
        headerResponse: {},
        sectionsResponse: {
            paragraphs:{},
            node: {}
        }
    };
    componentDidMount(){
        const { DrupalUrl, HomepageUrl, MainHeaderUrl, DrupalCredential } = BaseUrls;
        let url = DrupalUrl + MainHeaderUrl;    //Header API
        const authorization = 'Basic ' + btoa(DrupalCredential.username + ':' + DrupalCredential.password);
        request(url, authorization, 'get', null).then(drupalResponse => {
            this.setState({
                isLoading: false,
                headerResponse: drupalResponse.rows
            })
        });
        url = DrupalUrl + HomepageUrl;  //Homepage Sections API
        request(url, authorization, 'get', null).then(drupalResponse => {
            this.setState({
                isLoading: false,
                sectionsResponse: drupalResponse
            })
        });
    }
    render(){
        const { headerResponse, sectionsResponse, isLoading } = this.state;
        if(!sectionsResponse || !headerResponse) return null;
        return (
            <React.Fragment>
                <Header headingsRequired={true} headerData={headerResponse}/>
                <FixedComponents />
                <SelectProduct productData={sectionsResponse}/>
                <AwarenessAbout productData={sectionsResponse}/>
                <ClaimsIs productData={sectionsResponse}/>
                <HeresWhat productData={sectionsResponse}/>
                <Awards productData={sectionsResponse}/>
                <Interested productData={sectionsResponse}/>
                <CallMeBackForm />
                <Peep productData={sectionsResponse}/>
                <SocialMedia productData={sectionsResponse}/>
                <Footer />
            </React.Fragment>
        );
    }
}
