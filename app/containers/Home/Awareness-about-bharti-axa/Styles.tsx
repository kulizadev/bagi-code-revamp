import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    .heading{
        margin: 40px 0;
    }
    .about{
        background-color: ${colors.secondaryBodyBG};
        border-radius: 3rem;
        div:last-child{
            border-radius: 3rem;
        }
        .bharti-wrapper{
            padding: 52px 46px 0 66px;
            min-height: 700px;
            .bharti-img{
                width: 160px;
            }
            .bharti-description{
                margin-bottom: 0;
            }
        }
        .axa-wrapper{
            padding: 39px 45px 0;
            .axa-img{
                width: 67px;
            }
        }
    }
    .common-title{
        color: ${colors.lighterTextColor};
        font-size: ${fonts.fontSizeButton};
        font-weight: ${fonts.fontWeightHeadingOne};
        margin-top: 15px;
    }
    .common-description{
        color: ${colors.lighterTextColor};
        font-size: ${fonts.fontSizeText};
        margin-top: 50px;
    }
}`

export default DefaultStyle;
