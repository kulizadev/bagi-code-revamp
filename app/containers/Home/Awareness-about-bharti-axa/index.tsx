import React from 'react';
import Card from 'components/Card';
import Heading from 'components/Heading';
import DefaultStyle from './Styles';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import backgroundImage from 'images/global_images/ellipse-grey1.png';
import dottie from 'dottie';
import classes from './classNames';
import Typography from 'antd/lib/typography';

export default class AwarenessAbout extends React.Component<any,any>{
    render(){
        const { Paragraph } = Typography;
        const paragraphs = dottie.get(this.props.productData, 'paragraphs');
        const node = dottie.get(this.props.productData, 'node');
        const { bharti, axa } = classes;
        return (
            <DefaultStyle>
                <Row type="flex" justify="center">
                    <Heading headingType="headingTwo" 
                        label={
                            node.field_about_heading ?
                            node.field_about_heading[0].value
                        : ''}
                        className="heading"
                    />
                </Row>
                <Row type="flex" justify="center">
                    <Col lg={22} md={22}>
                        <Row type="flex" justify="center" className="about">
                            { paragraphs.field_about_content ?
                                paragraphs.field_about_content.map((content, key) => {
                                    // Awareness About Bharti AXA
                                    let contentClass = key == 0 ? bharti : axa;
                                    return (
                                        <Col lg={12} md={12} key={key}>
                                            <Card cardType={contentClass.cardType} backgroundImg={backgroundImage}>
                                                <div className={contentClass.wrapper}>
                                                    <img src={content.field_icon[0].url}
                                                    alt={content.field_icon[0].alt}
                                                    className={contentClass.img}
                                                    />
                                                    <p className={contentClass.title}>{content.field_subtitle[0].value}</p>
                                                    <Paragraph
                                                    className={contentClass.desc}
                                                    ellipsis={{ rows: 16, expandable: true }}
                                                    >
                                                        {content.field_description[0].value}
                                                    </Paragraph>
                                                </div>
                                            </Card>
                                        </Col>
                                    )
                                })
                            :''}
                        </Row>
                    </Col>
                </Row>
            </DefaultStyle>
        );
    }
}
