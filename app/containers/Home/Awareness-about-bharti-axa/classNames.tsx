const classes = {
    bharti: {
        wrapper: "bharti-wrapper",
        img: "bharti-img",
        title: "bharti-title common-title",
        desc: "bharti-description common-description",
        cardType: "layout3"
    },
    axa: {
        wrapper: "axa-wrapper",
        img: "axa-img",
        title: "common-title",
        desc: "common-description",
        cardType: "layout2"
    }
};
export default classes;
