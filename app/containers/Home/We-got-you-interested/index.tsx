import React from 'react';
import Card from 'components/Card';
import Heading from 'components/Heading';
import Button from 'components/Button'
import DefaultStyle from './Styles';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import dottie from 'dottie';
export default class Interested extends React.Component<any,any>{
    handleClick = (uri) => {
        document.location.href = uri;
    }
    render(){
        const paragraphs = dottie.get(this.props.productData, 'paragraphs');
        const node = dottie.get(this.props.productData, 'node');
        return (
            <DefaultStyle>
                <Row type="flex" justify="center">
                    <Heading headingType="headingTwo" 
                        label={
                            node.field_product_card_ ?
                            node.field_product_card_[0].value
                            : ''
                        }
                        className="heading"
                    />
                </Row>
                <Row type="flex" justify="center">
                    { paragraphs.field_product_card_details ?
                        // First Map to render all 5 product cards
                        paragraphs.field_product_card_details.map((content, key) => {
                            return (
                                <Col lg={4} md={4} key={key} className="card-gap">
                                    <Card cardType="layout5">
                                        <div className="card-head-wrapper">
                                            <img src={content.field_product_card_image[0].url}
                                            alt={content.field_product_card_image[0].alt}
                                            className="product-img"
                                            />
                                            <p className="product-title">{content.field_product_card_image[0].title}</p>
                                        </div>
                                        <div className="product-details-wrapper">
                                        { content.field_product_details ?
                                            // Second map to render bullet-points description
                                            content.field_product_details.map((content,key) => {
                                                return (
                                                    <Row key={key} className="details">
                                                        <Col lg={4} md={4}>
                                                            <img src={content.url}
                                                            alt={content.alt}
                                                            className="details-icon"
                                                            />
                                                        </Col>
                                                        <Col lg={20} md= {20}>
                                                            <span className="details-text">{content.title}</span>
                                                        </Col>
                                                    </Row>
                                                );
                                            })
                                        : ''}
                                        </div>
                                        {/* View More Button for all cards */}
                                        <div className="view-more">
                                            <Button buttonType={"bordered"}
                                            label={content.field_product_link[0].title}
                                            onClick={() => this.handleClick(content.field_product_link[0].uri)}
                                            />
                                        </div>
                                    </Card>
                                </Col>
                            );
                        })
                    : ''}
                </Row>
            </DefaultStyle>
        );
    }
}
