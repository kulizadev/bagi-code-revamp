import styled from 'styled-components';
import { colors, fonts } from 'configs/styleVars';

const DefaultStyle = styled.div`{
    .heading{
        margin: 40px 0 10px;
    }
    .card-gap{
        padding: 0 10px;
    }
    .card-head-wrapper{
        text-align: center;
        border-bottom: 1px solid lightgrey;
        .product-img{
            height: 50px;
            margin: 23px 0 29px;
        }
        .product-title{
            font-size: ${fonts.fontSizeHeadingThree};
            font-weight: ${fonts.fontWeightHeadingOne};
            color: ${colors.headingTextColor};
        }
    }
    .product-details-wrapper{
        min-height: 135px;
        .details{
            padding: 12px 12px 0;
            .details-icon{
                width: 18px;
            }
            .details-text{
                vertical-align: middle;
                font-size: ${fonts.fontSizeOverlineLink};
                color: ${colors.lighterTextColor};
            }
        }
    }
    .view-more{
        margin: 29px 0 33px;
        text-align: center;
    }
}`

export default DefaultStyle;
