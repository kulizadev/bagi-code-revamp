import { createGlobalStyle } from 'styled-components';
import { fonts, colors } from 'configs/styleVars';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
    font-size: ${fonts.webFontSize};
    background: ${colors.primaryBodyBG}
  }

  body {
    font-family: ${fonts.fontFamilyPrimary};
  }

  body.fontLoaded {
    font-family: ${fonts.fontFamilyPrimary};
  }

  #app {
    height: 100%;
  }
  .ant-popover-inner-content{
    box-shadow: -0.1px 2px 4px 0 rgba(0, 0, 1, 0.33);
    border: solid 2px #b5bed8;
    border-radius: 13px;
}
.ant-popover-inner{
  border-radius: 13px;
}
.ant-popover-arrow{
    width: 12.485281px;
    height: 12.485281px;
    background: #ffffff;
}
.ant-popover-placement-top > .ant-popover-content > .ant-popover-arrow{
  border-right: solid 2px #b5bed8;
  border-bottom: solid 2px #b5bed8;
} 
.ant-popover-placement-topLeft > .ant-popover-content > .ant-popover-arrow{
  border-right: solid 2px #b5bed8;
  border-bottom: solid 2px #b5bed8;
} 
li.ant-select-dropdown-menu-item i.anticon.anticon-check.ant-select-selected-icon{
  padding: 3px;
  border: 1px solid #b5b5b5;
  background: white;
  color: #b5b5b5;
  border-radius: 50%;
}
li.ant-select-dropdown-menu-item-selected i.anticon.anticon-check.ant-select-selected-icon{
  padding: 3px;
  border: 1px solid #83c275;
  background: #83c275;
  color: white;
  border-radius: 50%;
}
.ant-select-dropdown-menu::-webkit-scrollbar-thumb {
  background-color: darkgrey;
  outline: 1px solid slategrey;
  border-radius: 10px;
}
.ant-select-dropdown-menu::-webkit-scrollbar {
  width: 0.5em;
  padding: 10px;
}
.ant-select-dropdown-menu{
  border-radius: 7px;
  box-shadow: 0px 2px 4px 1.1px rgba(17, 49, 132, 0.2);
  border: solid 1px #829ce1;
  background-color: #ffffff
}
.ant-form-item-label > label{
  color: rgba(0,0,0,0.68);
  font-size: 13px !important;
  font-weight: bold;
}
.ant-form-item-label > label::after{
  content: '';
}
.has-error div.ant-form-explain, .error-field{
    color: white;
    position: relative;
    background-color: red;
    border-radius: 12px;
    display: table;
    padding: 2px 15px;
    min-height: 25px;
    z-index: 3;
    margin: 2px 0px;
    font-size: 11px;
    font-weight: bold;
    top: 10px;
    ::before{
      content: "";
      position: absolute;
      top: -20px;
      left: 20px;
      border-width: 10px;
      border-style: solid;
      border-color: transparent transparent red transparent;
    }
}
  p,
  label {
    font-family: ${fonts.fontFamilyPrimary};
    line-height: 1.5em;
  }
  @media (max-width: 760px){
    font-size: ${fonts.mobileFontSize};
  }
`;

export default GlobalStyle;

