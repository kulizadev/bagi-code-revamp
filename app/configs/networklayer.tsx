
export const request = (url, authorization, sendingMethod, dataObject) => {
  let options = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': '',
    },
    // credentials: 'same-origin',
    method: '',
    body: '',
  };

  if (authorization) {
    const token = authorization;
    options.headers.Authorization = token;
  }

  if (sendingMethod) {
    options.method = sendingMethod;
  }

  if (dataObject) {
    options.body = JSON.stringify(dataObject);
  }else{
    delete options.body;
  }
   return fetch(url, options)
    .then(response => response.json())
    .then(json => json)
    .catch((error) => {
        return error;
    })
}
