// Color Variables as per the latest Styleguide
export const globalColors = {
    white: '#fff',
    paleGrey: '#f0f2f6',  //pale-grey
    primaryBlackColor: '#333',
    secondaryBlackColor: '#666',
    disabledButtonColor: `#aeaeae`,   //button-disable grey color
    activeButtonColor: `#113184`,     // button-enabled blue color
    errorColor: '#ec1428',
    paleGreyTwo: '#e1e6f0',
    greyThree: '#e7eaf2',
    inputFieldTextColor:'rgba(119, 119, 119, 0.7)',
    linkColor: '#0066cc',//cerulean
    primaryBorderColor: '#cccccc',
    brownishGrey: '#5d5d5d',
    warmGrey: '#888',
    blackShaded7: 'rgba(51, 51, 51, 0.7)',
    blackShaded5: 'rgba(51, 51, 51, 0.5)',
    greyishBrown: '#494949',
    perrywinkle: '#829ce1',
    braedcrumbColor: '#777',
    greyblue: '#8898c1',
    darkskyblue: '#4878ea',
    sliderdot: '#8f9299',
    pleasingBlue: '#a0d9ef',
    checkboxText: '#9a9a9a',
    checkBoxChecked: '#8BC34A',
    callNumberBlueColor: '#3e589b',
    orGrey: '#cbc4c2',
    checkGrey: '#9a9a9a',
    footerBlue: '#415a9d',
    labelColor: 'rgba(0,0,0,0.68)',
    palegraythree: '#ebebeb',
    loader: '#01529c',
    black30: 'rgba(0, 0, 0, 0.3)'
};

export const colors = {
    /** Color Schema */

    //Background Colors
    primaryBodyBG: globalColors.paleGrey,
    secondaryBodyBG: globalColors.white,
    enabledButtonBG: globalColors.activeButtonColor,
    disabledButtonBG: globalColors.disabledButtonColor,
    errorBG: globalColors.errorColor,
    buttonActiveTabpane: globalColors.greyblue,
    toggle:globalColors.paleGreyTwo,
    homepageStatsBG: globalColors.pleasingBlue,
    irdaiFooterBG: globalColors.greyThree,
    footerLightBG: globalColors.footerBlue,

    // Text Colors
    headingTextColor: globalColors.primaryBlackColor,
    lighterTextColor: globalColors.secondaryBlackColor,
    buttonTextColor: globalColors.white,
    errorTextColor: globalColors.errorColor,
    inputText: globalColors.inputFieldTextColor,
    linkText: globalColors.linkColor,
    inputFieldClr : globalColors.inputFieldTextColor,
    breadCrumbLink: globalColors.braedcrumbColor,
    callNumber: globalColors.callNumberBlueColor,
    orText: globalColors.orGrey,
    checkboxTextGrey: globalColors.checkGrey,
    palegraythreeColor: globalColors.palegraythree,
    blackThrityColor: globalColors.black30,

    /** Border Schema */
    primaryBorderColor: globalColors.secondaryBlackColor,

    /** slider Color */
    slider:globalColors.darkskyblue,

    /** Carousal Color */
    carousalDots: globalColors.sliderdot,

    /** Cheakboc Color */
    checkboxTextColor: globalColors.checkboxText,
    checkBoxCheckedColor: globalColors.checkBoxChecked,

    /** loader */
    loaderColor: globalColors.loader,
};

// Font Variable:
export const fonts = {
    // Font Family
    fontFamilyPrimary: 'Montserrat',
    // Font Weight
    fontWeightHeadingOne: '600',
    fontWeightPrimary: '500',
    // Font Size
    webFontSize: '10px',
    mobileFontSize: '8px',
    
    fontSizeHeadingOne: '3.6rem',
    fontSizeHeadingTwo: '2.2rem',
    fontSizeHeadingThree: '1.8rem',
    fontSizeButton: '1.6rem',
    fontSizeText: '1.4rem',
    fontSizeLink: '1.3rem',
    fontSizeOverlineLink: '1.2rem',
    fontSizeFooterText: '1.1rem',
    fontSizeNumberHeading: '3rem',

    // Line Height Variables
    lineHeightH1: '4.5rem',
    lineHeightH2: '3.29rem',
    lineHeightTextbox: '3.29rem',

    fontColorInputBox: globalColors.brownishGrey,
};

export const sizes = {
    paddingXL: '7rem',
    paddingL: '5rem',
    paddingM: '3rem',
    paddingS: '1rem',
    paddingXS: '0.5rem',

    marginXL: '5rem',
    marginL: '3rem',
    marginM: '2rem',
    marginS: '1rem',
    marginXS: '0.2rem',

    borderRadiusXL: '5rem',
    borderRadiusL: '3rem',
    borderRadiusM: '2rem',
    borderRadiusS: '1rem',
    borderRadiusXS: '0.2rem',

    formFieldMinHeight: '4.9rem',


};
export const borderBoxStyle = {
    boxShadow: '0px 2px 0.8px 0.2px rgba(0, 0, 0, 0.16)',
    border: `solid 1px ${globalColors.primaryBorderColor}`
}
const vars = {
    colors,
    fonts,
    sizes,
    borderBoxStyle,
};

export default vars;
