import { takeLatest, put, all } from 'redux-saga/effects';
import BaseUrls from 'constants/apiConstants';


const request = (constants) => {
    return { type: constants.INIT }
};

const receive = (constants, data, resolve) => {
    resolve ? resolve(data): ''
    return { type: constants.SUCCESS, payload: {data} }
};

const error = (constants, err,reject) => {
    reject ? reject(err): ''
    return { type: constants.ERROR, error: err }
};


function* fetchApiData(action) {
    const { constants, sendingMethod, postBody, resolve, reject } = action.payload;
    const authorization = 'Basic ' + btoa(BaseUrls.DrupalCredential.username + ':' + BaseUrls.DrupalCredential.password);
    let options = {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': '',
        },
        // credentials: 'same-origin',
        method: 'GET',
        body: '',
      };
    
      if (authorization) {
        options.headers.Authorization = authorization;
      }
    
      if (sendingMethod) {
        options.method = sendingMethod;
      }
      if (postBody) {
        options.body = JSON.stringify(postBody);
      }else{
        delete options.body;
      }
    
    try {
        yield put(request(constants));
        const response = yield fetch(constants.URL,options);
        const data = yield response.json();
        yield put(receive(constants,data, resolve));
    } catch (err) {
        yield put(error(constants, err, reject));
    }
}

export function* getApiData() {
    yield takeLatest('GET', fetchApiData);
}

export default function* rootSaga() {
    yield all([getApiData()])
}
